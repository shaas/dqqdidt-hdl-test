# -*- coding: utf-8 -*-
"""
Created on Tue Aug  6 12:33:00 2019
Description: Generation of VHDL package from Excell sheet
    
@author: anskocze
"""

import sys
import reg_map_pkg as rmp

if len(sys.argv) > 1:
    config_block_select = int(sys.argv[1])
else:
    print('Usage: python gen_reg_map_pkg index_to_data_sheet_in_Excel_file')
    exit()

if rmp.generate(config_block_select, '.', '.'):
    print('Done OK')
else:
    print('Failed')
