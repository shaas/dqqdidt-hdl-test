library ieee;
use ieee.std_logic_1164.all;
USE IEEE.numeric_std.ALL;
USE IEEE.fixed_pkg.all;
use IEEE.fixed_float_types.all;

LIBRARY UQDSLib;
use UQDSLib.UQDSLib.ALL;
use UQDSLib.reg_map_pkg.all;
use UQDSLib.config_block_lib.all;
use UQDSLib.util.all;


---------------------------------------------------------------------------------------------------------
-- Company: CERN
--
-- File: config_block.vhd
-- File history:
--      0.1: 14.03.2019: first attempt
--      1.0: 18.06.2019: use for 11T protection in SM18
--  	2.0: 19.07.2019: use for FAIR
--		2.1: 24.09.2019: Changed position of logic bits to be compatible with non-modular FAIR
--	<Revision number>: <Date>: <Comments>
--
-- Description: 
--
--!@brief Custom configuration block number 3 to work with UQDS for FAIR 
--	
-- INPUT CHANNELS : 15
-- Ch0: U1_A
-- Ch1: U2_A
-- Ch2: Ulead1_A
-- Ch3: Ulead2_A
-- Ch4: Current_A
-- Ch5: U1_B
-- Ch6: U2_B
-- Ch7: Ulead1_B
-- Ch8: Ulead2_B
-- Ch9: Current_B
-- Ch10: U1_C
-- Ch11: U2_C
-- Ch12: Ulead1_C
-- Ch13: Ulead2_C
-- Ch14: Current_C
--
-- OUTPUT CHANNELS : 24
-- Ch0: U1_A
-- Ch1: U2_A
-- Ch2: Ulead1_A
-- Ch3: Ulead2_A
-- Ch4: Current_A
-- Ch5: U1_B
-- Ch6: U2_B
-- Ch7: Ulead1_B
-- Ch8: Ulead2_B
-- Ch9: Current_B
-- Ch10: U1_C
-- Ch11: U2_C
-- Ch12: Ulead1_C
-- Ch13: Ulead2_C
-- Ch14: Current_C
-- Ch15: Logics

-- Ch16: Diff_U1U2_A
-- Ch17: Diff_U1U2_B
-- Ch18: Diff_U1U2_C
-- Ch19: DiDt_A
-- Ch20: DiDt_B
-- Ch21: DiDt_C
-- Ch22: Channel_Valid
-- CH23: Trigger Lines

--
-- LOGICS
-- L1(2bits): Sync/Latch
-- L2(2bits): U1-U2
-- L3(2bits): U_lead2
-- L4(2bits): U_lead1
-- Above times 3 for 3 magnets
-- Rest -> didt_logic(1) & broken_wire_logic(6 bits) & trigger_postmortem
-- Targeted device: M2GL150
-- Author: Daniel Blasco, Jens Steckert, Surbhi Mundra
--
----------------------------------------------------------------------------------------------------------
entity config_block is
	port(
		CLK                      : in  std_logic;
		rst                      : in  std_logic;
		--Data from ADC, scaled & dready pulses
		ChannelsScaledData       : in  channelsArray(CHANNELS_IN_MAX - 1 downto 0);
		ChannelsScaledDataRdy    : in  std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
		
		--Flags to show if ADC gives constant values
		ADC_Health			 	: in  std_logic_vector(REGISTER_SIZE - 1 downto 0);
		--ADC_Health_Mask		 	: in  std_logic_vector(REGISTER_SIZE - 1 downto 0);
		--Flags gnerated by scaler to indicate that signal left linear range of UQDS channel
		ADC_overRange			: in  std_logic_vector(REGISTER_SIZE - 1 downto 0);
		--ADC_overRange_Mask		: in  std_logic_vector(REGISTER_SIZE - 1 downto 0);
		Channel_Mask			: in  std_logic_vector(REGISTER_SIZE - 1 downto 0);
			
		--Registers configuring this block (127 registers in total)		
		ConfigBlockReg           : in  customRegArray;
		
		--Signals from top level command interpreter to initiate auto offset correction
		AutozeroStart            : in  std_logic_vector(31 downto 0);
		
		--Configuration of Readout (mux settings)
		ReadoutConfig            : in  std_logic_vector(REGISTER_SIZE - 1 downto 0);
		
		AdcStartConv             : in  std_logic;
		
		--Mask to select Post mortem trigger from local trigger sources
		PMBufferMask             : in  std_logic_vector(REGISTER_SIZE - 1 downto 0);
		
		--Sync signals from top level (actual action is defined on top)
		SyncSignals              : in  std_logic_vector(5 downto 0);
		
		--Data to PM buffer & associated dready signals
		BufferMUXData            : out outputChannelsArray;
		BufferMUXDataRdy         : out std_logic;
		
		--Mask to mark non-decimable PM buffer channels (non-analogue values)
		BufferNoDecimateChannels : out std_logic_vector(CHANNELS_OUT_MAX - 1 downto 0);
		
		--Data to normal register space
		RegMUXData               : out regChannelsArray;
		
		--Data to Fast Readout module & associated dready
		FReadoutMUXData          : out outputChannelsArray;
		FReadoutMUXDataRdy       : out std_logic;
		
		--Trigger lines generated locally, mapped on TOP to pysical lines
		TriggerLines             : out std_logic_vector(TRIGGER_LINES_MAX - 1 downto 0);
		
		--PM trigger signal generated locally
		TriggerPM                : out std_logic;
		
		--ID of this config block
		ConfigBlockNum           : out std_logic_vector(REGISTER_SIZE - 1 downto 0)
	);
end config_block;

architecture config_block_arch of config_block is
type integerArray is array (CHANNELS_GENERICS_NUM - 1 downto 0) of integer range 0 to 15;
type intMavgDepthArray is array (CHANNELS_GENERICS_NUM -1 downto 0) of integer range 0 to log2c(filter_mavg_ram_depth);

function init_integerArray (init_val : in integer; size: in integer) return integerArray is
	variable initarr : integerArray;
	begin
		for i in size-1 downto 0 loop
			initarr(i) := init_val;
		end loop;
		return initarr;
end function;
function init_intMavgDepthArray (init_val : in integer; size: in integer) return intMavgDepthArray is
	variable initarr : intMavgDepthArray;
	begin
		for i in size-1 downto 0 loop
			initarr(i) := init_val;
		end loop;
		return initarr;
end function;
	--========================= Signal declaration ============================

	-- Channel configuration signals
	type configArray is array (CHANNELS_GENERICS_NUM - 1 downto 0) of std_logic_vector(7 downto 0);
	
	signal channel_filter_cfg        : configArray;
	signal channel_daq_cfg           : configArray;
	
	signal channel_mean_depth_int    : intMavgDepthArray := init_intMavgDepthArray(2, CHANNELS_GENERICS_NUM);
	signal channel_median_depth_int  : integerArray := init_integerArray(2, CHANNELS_GENERICS_NUM);
	signal channel_median_middle_int : integerArray := init_integerArray(1, CHANNELS_GENERICS_NUM);
	signal channel_mean_inv_depth    : regDataArray(CHANNELS_GENERICS_NUM - 1 downto 0);
	
	type gainArray is array (CHANNELS_GENERICS_NUM - 1 downto 0) of std_logic_vector(15 downto 0);
	signal channel_corrector_gain    : gainArray;
	signal channel_corrector_offset  : channelsArray(CHANNELS_GENERICS_NUM - 1 downto 0);

	-- End of pipeline data
	signal scaled_data             : outputChannelsArray := (others=>(others=>'0'));
	signal filtered_data_secondary : outputChannelsArray := (others=>(others=>'0'));
	signal filtered_data           : outputChannelsArray := (others=>(others=>'0'));
	signal filtered_data_rdy       : std_logic_vector(CHANNELS_IN - 1 downto 0);
	signal filtered_data_sec_rdy   : std_logic_vector(CHANNELS_IN - 1 downto 0);
	--signal didt_data_filtered : regDataArray(2 downto 0);
	signal didt_data          : regDataArray(NUM_LOGIC-1 downto 0);
	signal didt_data_rdy      : std_logic_vector(NUM_LOGIC-1 downto 0);
	
	-- Logic signals
	signal logic_status, quench_status : std_logic_vector(31 downto 0):= (others=> '0');
	signal broken_wire_logic  : std_logic_vector(23 downto 0):= (others=> '0');
	signal didt_logic         : std_logic_vector(11 downto 0):= (others=> '0');																					
	
	signal Channel_Valid	: std_logic_vector(15 downto 0);	
	signal Channel_Valid_sum :std_logic_vector(2 downto 0):=(others=>'0');
	
	type QDLogicConfigArray is array (7 downto 0) of std_logic_vector(31 downto 0);
	signal broken_wire_th : QDLogicConfigArray;
	signal trigger			: std_logic_vector(TRIGGER_LINES_MAX - 1 downto 0);
	--=========================================================================
	constant generate_median : boolean := TRUE;


begin

	Channel_Valid <= ADC_Health(15 downto 0) OR ADC_Health(31 downto 16) OR ADC_overRange(15 downto 0) OR ADC_overRange(31 downto 16);
	-- Config block identifier (each different config block for different FW should have a different identifier)
	ConfigBlockNum <= std_logic_vector(to_unsigned(3, 32));
	TriggerLines <=  trigger;
	
	--========================== Register mapping =============================

	--	Assign Generic Config block registers to its signals
	RegisterMapConfigBlock(
			ConfigBlockReg,  	--IN
			ChannelGenericA, 	--OUT
			ChannelGenericB, 	--OUT
			ChannelGenericC, 	--OUT
			ChannelGenericD, 	--OUT
			SimplThresh,  	--OUT
			LogicConf  	--OUT
		);

	--=========================================================================

	--================= Channel generic for input channels ====================

	data_processing : for i in 0 to CHANNELS_IN - 1 generate
		channel_data_processing : channel_generic
			generic map(
				generate_median => generate_median,
				d_width        => 32,
				b_width        => 32,   -- Not used
				mavg_ram_depth => 1024, -- Max mavg depth
				cor_c_width    => 16
			)
				-- port map
			port map(
				-- Inputs
				clk             => CLK,
				rst             => rst,
				sampleclock     => ChannelsScaledDataRdy(i),
				Data_in         => ChannelsScaledData(i),
				autozero_start  => AutozeroStart(i),
				mux_data        => channel_filter_cfg(i), -- Config [autozero, manual, mean, median, decimation,decimation,decimation,inverter]
				mux_daq         => channel_daq_cfg(i)(1 downto 0), -- DAQ
				mavg_depth		 => channel_mean_depth_int(i),
				real_invdiv     => channel_mean_inv_depth(i),
				median_depth    => channel_median_depth_int(i),
				median_middle   => channel_median_middle_int(i),
				cor_gain        => channel_corrector_gain(i), -- Manual correction
				cor_offset      => channel_corrector_offset(i), -- Manual correction
				-- Outputs
				cor_offset_auto => open, -- Not used
				Dmux_data_out   => filtered_data(i)(31 downto 0), 				-- Output to regspace
				Bmux_data_out   => filtered_data_secondary(i)(31 downto 0), 	-- Output to buffers
				d_ready_out     => filtered_data_rdy(i), -- Output rdy
				buf_ready_out	=> filtered_data_sec_rdy(i),--filtered_data_sec_rdy,
				debug           => open
			);

	end generate;
	
	--============================ Logic blocks ===============================

	didt_check : for i in 0 to NUM_LOGIC-1 generate
		current_diff : differentiator_synram
			generic map(
				arr_depth => 128,       --65536,
				d_width   => 32
			)
			port map(
				clk                => CLK,
				rst                => rst,
				din                => filtered_data(5*i + 4),
				dready             => filtered_data_rdy(5*i + 4),
				dt                 => x"96",
				normalizing_shifts => x"06",
				dout               => didt_data(i),--scaled_data(19+i)
				done               => didt_data_rdy(i)--scaled_data_rdy(19+i)
			);			
			scaled_data(19+i) <= didt_data(i);
		
		--Channel generic for didt based vtap protection	
		channel_data_processing : channel_generic
			generic map(
				d_width        => 32,
				b_width        => 32,   -- Not used
				--				median_depth   => channel_median_depth_int(i),    -- Number of previous samples taken for median
				--				median_middle  => channel_median_middle_int(i),    -- To be the median should be median_depth/2
				mavg_ram_depth => 1024, -- Max mavg depth
				cor_c_width    => 16
			)
				-- port map
			port map(
				-- Inputs
				clk             => CLK,
				rst             => rst,
				sampleclock     => didt_data_rdy(i),
				Data_in         => didt_data(i),
				autozero_start  => '0',
				mux_data        => channel_filter_cfg(15), -- Config [autozero, manual, mean, median, decimation,decimation,decimation,inverter]
				mux_daq         => channel_daq_cfg(15)(1 downto 0), -- DAQ
				mavg_depth_bits => channel_mean_depth_int(15),
				real_div        => channel_mean_depth_int(15),
				real_invdiv     => channel_mean_inv_depth(15),
				median_depth    => channel_median_depth_int(15),
				median_middle   => channel_median_middle_int(15),
				cor_gain        => channel_corrector_gain(15), -- Manual correction
				cor_offset      => channel_corrector_offset(15), -- Manual correction
				-- Outputs
				cor_offset_auto => open, -- Not used
				Dmux_data_out   => filtered_data(19+i), -- Output
				Bmux_data_out   => filtered_data_secondary(19+i), -- Not used
				d_ready_out     => open,
				buf_ready_out	=> open,--
				debug           => open
			);
			
		didt_comparator : qlogic_comp_simple
			generic map(d_width => 32)
			port map(clk   => CLK, rst => rst,
			         U_in  => filtered_data(19+i),
			         T_in  => SimplThresh(6),
			         disc  => (others => '0'),
			         L_out => didt_logic(4*i + 3 downto 4*i)
			        );

		channel_comparator1 : inverted_qlogic_comp_simple
			generic map(d_width => 32)
			port map(clk   => CLK, rst => rst,
			         U_in  => filtered_data(5*i),
			         T_in  => broken_wire_th(i),
			         disc  => LogicConf(7)(15 downto 0),
			         L_out => broken_wire_logic(8*i + 3 downto 8*i)
			        );

		channel_comparator2 : inverted_qlogic_comp_simple
			generic map(d_width => 32)
			port map(clk   => CLK, rst => rst,
			         U_in  => filtered_data(5*i + 1),
			         T_in  => broken_wire_th(i),
			         disc  => LogicConf(7)(15 downto 0),
			         L_out => broken_wire_logic(8*i + 7 downto 8*i + 4)
			        );
		
		broken_wire_th(i) <= SimplThresh(7) when didt_logic(4*i + 1) = '1' else (others => '0');

	end generate;		
	
	detection : for i in 0 to NUM_LOGIC - 1 generate
		mag_twolead_logic : detection_block_mag_twolead
			generic map(d_width => 32)
			port map(
				clk         => CLK,
				rst         => rst,
				magA        => filtered_data(5*i + 0),
				magB        => filtered_data(5*i + 1),
				leadA       => filtered_data(5*i + 2),
				leadB       => filtered_data(5*i + 3),
				thres_mag   => SimplThresh(2*i),
				disc_mag    => LogicConf(2*i)(15 downto 0),
				thres_leads => SimplThresh(2*i + 1),
				disc_leads  => LogicConf(2*i + 1)(15 downto 0),
				magABsum    => filtered_data(16+i),
				logic_out   => quench_status(8*i + 7 downto 8*i)
			);
		filtered_data_secondary(16+i)	<=filtered_data(16+i);
		scaled_data(16+i)				<=filtered_data(16+i);
	end generate;
		
	type_matching_scaled_data : for i in 0 to CHANNELS_IN - 1 generate
		scaled_data(i) <= ChannelsScaledData(i);
	end generate;

	data_processing_signals : for i in 0 to CHANNELS_GENERICS_NUM - 1 generate

		channel_filter_cfg(i)          <= ChannelGenericA(i)(7 downto 0); -- MUX control
		channel_daq_cfg(i)(7 downto 0) <= "00000" & ChannelGenericA(i)(10 downto 8); -- MUX control Buffer
		channel_mean_depth_int(i)      <= to_integer(unsigned(ChannelGenericA(i)(31 downto 16))); -- Moving average filter length
		
		channel_median_depth_int(i)    <= to_integer(unsigned(ChannelGenericB(i)(7 downto 0))) when generate_median = TRUE else 2; -- Median filter depth
		channel_median_middle_int(i)   <= to_integer(unsigned('0' & ChannelGenericB(i)(7 downto 1))) when generate_median = TRUE else 1; -- Median filter middle (depth divided by 2)
		channel_corrector_gain(i)      <= ChannelGenericB(i)(31 downto 16); -- Corrector gain
		
		channel_corrector_offset(i)    <= ChannelGenericC(i); -- Corrector offset
		channel_mean_inv_depth(i)      <= ChannelGenericD(i); -- Inverted multiplication for mavg filter

	end generate;
	--=========================================================================

	--======================== Output multiplexers ============================
	
	-- We have three main data paths:
	-- scaled_data: 			raw data from channels after scaler
	-- filtered_data:			from channel generic main data output (input to logic) & derived values
	-- filtered_data_secondary: from channel generic buffer data output & derived values
		
	--Multiplex data going to registers	
	
	generate_mux_data1 : for i in 0 to CHANNELS_OUT - 1 generate
		RegMUXData(i)      <= filtered_data_secondary(i) when (ReadoutConfig(1 downto 0) = "01")
			else scaled_data(i) when (ReadoutConfig(1 downto 0) = "10")
			else filtered_data(i);
	end generate;
	
	--Multiplex data going to buffers (on top level)
	BufferMUXData       <= 		filtered_data_secondary 	when 	(ReadoutConfig(1 downto 0) = "01") --use data from channel buffer mux
						else 	scaled_data 				when 	(ReadoutConfig(1 downto 0) = "10") --use raw data
						else 	filtered_data;															  --use mux chain data
	
	--dready for post mortem buffers		
	BufferMUXDataRdy 	<= 		filtered_data_sec_rdy(0) 			when 	(ReadoutConfig(1 downto 0) = "01") --use dready from buffer port
						else 	ChannelsScaledDataRdy(0) 	when	(ReadoutConfig(1 downto 0) = "10") --use dready from raw data
						else 	filtered_data_rdy(0);													--use dready from end of mux chain
			
	--Multiplex data going to fast readout (on top level)		
			
	type_matching_freadout : for i in 0 to CHANNELS_OUT - 1 generate
	FReadoutMUXData(i) 	<= 		filtered_data_secondary(i) 	when 	(ReadoutConfig(9 downto 8) = "01")
						else scaled_data(i) 				when (ReadoutConfig(9 downto 8) = "10")
						else filtered_data(i);
	end generate;
	
	FReadoutMUXDataRdy <= filtered_data_sec_rdy(0) 					when (ReadoutConfig(9 downto 8) = "01")
						else ChannelsScaledDataRdy(0) 		when (ReadoutConfig(9 downto 8) = "10")
						else filtered_data_rdy(0);
 
	--============================ Logic match ================================
	--logic_status(11 downto 0): 3x 4 bits from QD logic
	--ADD adc status bits
	logics: for i in 0 to NUM_LOGIC - 1 generate
		logic_status(8*i+7 downto 8*i+2)	<=	quench_status(8*i+7 downto 8*i+2);
		logic_status(8*i)	<= broken_wire_logic(8*i);
		logic_status(8*i+1)	<= broken_wire_logic(8*i+4);
	end generate;
	--add sync signals
	logic_status(24)              <= didt_logic(0) OR didt_logic(1) OR didt_logic(2);
	logic_status(30 downto 25)	<= SyncSignals;
	logic_status(31)			<= TriggerPM;
	
	--============================ Output match ===============================
	--Propagate actual threshold and actual discriminator to data channels
	filtered_data(15)(31 downto 0)           <= logic_status(31 downto 0);
	filtered_data(22) <= x"0000" & Channel_Valid;
	filtered_data(23)(TRIGGER_LINES_MAX-1 downto 0) <= trigger;
	
	filtered_data_secondary(15)(31 downto 0) <= logic_status(31 downto 0);
	filtered_data_secondary(22) <= x"0000" & Channel_Valid;
	filtered_data_secondary(23)(TRIGGER_LINES_MAX-1 downto 0) <= trigger;
	
	scaled_data(15)(31 downto 0)             <= logic_status(31 downto 0);
	scaled_data(22) <= x"0000" & Channel_Valid;
	scaled_data(23)(TRIGGER_LINES_MAX-1 downto 0) <= trigger;

	--============================ Triggers ===============================
	
	--Control trigger HW lines
	--Triggers (17 downto 2): Active voltage output lines to trigger HDS etc
	--Triggers (1 downto 0):  Current loop ('0' = opens or '1' = closes)
	triggers : for i in 0 to NUM_LOGIC - 1 generate
		trigger(i) <= not quench_status(i*8) and not((broken_wire_logic(i*8) or broken_wire_logic(i*8+4)) and PMBufferMask(6)) and not Channel_Valid_sum(i);
	end generate;
	
	--Marks non-decimatable buffer channels (logic flags etc)  (in our case ch8 are logic signals)

	BufferNoDecimateChannels(31 downto 22) <= (others => '1');
	BufferNoDecimateChannels(21 downto 16) <= (others => '0');	
	BufferNoDecimateChannels(15 downto 15) <= (others => '1');
	BufferNoDecimateChannels(14 downto 0) <= (others => '0');
	
	--Evaluate the channel over range flags: (active if channel exceeds 22.5V, checked in scaler)
	overrange : for i in 0 to NUM_LOGIC - 1 generate
	Channel_Valid_sum(i) <= (Channel_Valid(i*5+4) AND Channel_Mask(i*5+4)) OR
						(Channel_Valid(i*5+3) AND Channel_Mask(i*5+3)) OR
						(Channel_Valid(i*5+2) AND Channel_Mask(i*5+2)) OR
						(Channel_Valid(i*5+1) AND Channel_Mask(i*5+1)) OR
						(Channel_Valid(i*5) AND Channel_Mask(i*5));
	end generate;
	
	--Combine QD logic signals to trigger signals, we're high-active
	--								sym				asym ap1			asym ap2
		
	--Condition to trigger post mortem:
	TriggerPM                 <=  	(broken_wire_logic(0)  and PMBufferMask(6)) or --broken wire logic block A(U2)
									(broken_wire_logic(4)  and PMBufferMask(6)) or --broken wire logic block A(U1)
									(broken_wire_logic(8)  and PMBufferMask(6)) or --broken wire logic block B(U2)
									(broken_wire_logic(12) and PMBufferMask(6)) or --broken wire logic block B(U1)
									(broken_wire_logic(16) and PMBufferMask(6)) or --broken wire logic block C(U2)
									(broken_wire_logic(20) and PMBufferMask(6)) or --broken wire logic block C(U1)
									(quench_status(0) 	   and PMBufferMask(0)) or --latch block A
									(quench_status(1) 	   and PMBufferMask(1)) or --sync block A
									(quench_status(8) 	   and PMBufferMask(2)) or --latch block B
									(quench_status(9) 	   and PMBufferMask(3)) or --sync block B
									(quench_status(16) 	   and PMBufferMask(4)) or --latch block C
									(quench_status(17) 	   and PMBufferMask(5)) or --sync block C
									
									(Channel_Valid_sum(0)  and PMBufferMask(0)) or -- overrange in Block A
									(Channel_Valid_sum(1)  and PMBufferMask(2)) or -- overrange in Block B
									(Channel_Valid_sum(2)  and PMBufferMask(4));   -- overrange in Block C								
									
	--=========================================================================

end;