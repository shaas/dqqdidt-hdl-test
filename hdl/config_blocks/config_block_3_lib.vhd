--------------------------------------------------------------------------------
-- Company: CERN
--
-- File: config_block_lib.vhd
-- File history:
--      <Revision number>	: <Date>		: <Comments>
--		  V 0.1 			:	09.08.2019 	: first implementation for config block 2
--
--
-- Description: 
-- Library for config block 2 dependent constants previously located in UQDSLib
--
-- 
--
-- Targeted device: originally (<Family::IGLOO2> <Die::M2GL150> <Package::1156 FBGA>)
-- 
--	
-- Author: Jens Steckert 
-- 
--
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
USE IEEE.numeric_std.ALL;
USE IEEE.fixed_pkg.all;



package config_block_Lib is

	
	constant CHANNELS_IN           : integer := 15;
	constant CHANNELS_OUT          : integer := 24;
	constant CHANNELS_OUT_FR          : integer := 16; 
	constant CHANNELS_GENERICS_NUM : integer := 16; --# config block dependent
	

	constant IDepSettingsSteps 		: integer := 4; --length of current dependent settings table
	
	--======================================================
	--buffer settings
	constant CHANNELS_PER_RAM      		: integer range 1 to 15  := 6;
	constant BUFFER_DECIMATION_BITS  	: integer range 0 to 63  := 8;
	constant SRAM_ADDR_WIDTH    		: integer range 2 to 25  := 20; --number of 16-bit words in SRAM chip
	constant NR_CHANNELS_BUFFER 		: integer range 1 to 15  := 9;
	constant NUM_LOGIC              	: integer := 3; --# config block dependent
	constant SRAM_DEPTH         : integer                := 2**SRAM_ADDR_WIDTH;
	constant SRAM_MAX           : integer                := SRAM_DEPTH - (SRAM_DEPTH mod NR_CHANNELS_BUFFER);
	constant SRAM_POST_TRIGGER  : integer                := SRAM_MAX/2 - (SRAM_MAX/2 mod NR_CHANNELS_BUFFER);
	
	constant median_max_depth 	 : integer range 1 to 127 := 10;
	constant cor_c_width        : integer                := 16;
	constant filter_mavg_ram_depth : integer range 0 to 4095 := 1023;
	
end config_block_Lib;
