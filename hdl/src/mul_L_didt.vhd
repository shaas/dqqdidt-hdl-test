library ieee ;
use ieee.std_logic_1164.all;
USE IEEE.numeric_std.ALL;
USE IEEE.fixed_pkg.all;

LIBRARY UQDSLib;
use UQDSLib.UQDSLib.ALL;

---------------------------------------------------------------------------------------------------------
-- Company: CERN
--
-- File: add_didt.vhd
-- File history:
--      0.1: 26.10.2011: first attempt, TESTED
--		0.2: 25.06.2013: special treatment for undulator L
--		0.3: 26.06.2013: flexible width (24 or 24 bit) --> works fine
--		1.0: 17.08.2018: adapted for uQDS, using fixed_pkg
--      
--  
--	<Revision number>: <Date>: <Comments>
--
-- Description: 
--
--!@brief multiplies didt with L values and cuts the signal to the proper width
--	
--	
--
--
-- Targeted device: Actel ProAsic3 A3PE1500 PQ208
-- Author: Jens Steckert
--
----------------------------------------------------------------------------------------------------------
entity mul_L_didt is 
generic(d_width : integer range 15 to 32 := 32;
		d_width_L : integer range 7 to 32 := 16
);
port (
	clk, rst 		: in std_logic;
	didt	 		: in std_logic_vector(d_width -1 downto 0); 		--s(15,16)
	L				: in std_logic_vector(d_width_L -1 downto 0);					--u(2,14)
	start_mult		: in std_logic;
	dout 			: out std_logic_vector(d_width -1 downto 0) 		--s(7,24)
);
end mul_L_didt;


architecture mul_L_didt_arch of mul_L_didt is



--internal inductance signal, one bit wider to force to signed positive
signal L_i : std_logic_vector(d_width_L downto 0);

signal result : std_logic_vector(d_width + 17 -2 downto 0);


--create fixed point signed vector with proper length
signal mul_result_fx : sfixed(17 downto -30);

--final result vector (cropped)
signal result_crop :sfixed(7 downto -24); --std uQDS voltage format16

component multS is
generic (W1, W2: natural; omode: std_logic);
port(clk, rst, start	:in std_logic;
	multiplier	:in std_logic_vector(W1-1 downto 0);
	multiplicand	:in std_logic_vector(W2-1 downto 0);
	done		:out std_logic;
	product		:out std_logic_vector(W1+W2-2 downto 0));
end component;

begin

--convert u(2,14) to s(2,14)
L_i <= '0' & L;



--------------------------------------------------------------------------
--multiplier

-- s(15,16) x s(2,14) = s(15 + 2, 16 + 14) = s(17,30)
-- since L * di/dt gives a voltage, we should keep treatment as voltage uQDS standard voltage format is s(7,24)
-- comma is located at 47 - integral part (17) = 30 
-- integral part of length 7 is then (30 + 7) -1 
-- ==> crop_int <= (product'left -17 + 6) downto (product'left -17)
-- ==> crop_frac <= product'left -18 downto (29 - 24) 

-- ==> crop <= product(product'left -17 + 6 downto product'left - 18 - 24)



mul_seq: multS
generic map(W1 => d_width, W2 => 17, omode => '1')
port map (clk => clk, rst => rst, 
			start => start_mult, 
			multiplier => didt,
			multiplicand => L_i,
			done => open,
			product => result
			);

mul_result_fx <= to_sfixed(result,mul_result_fx);

--fixed_pkg supported resize --> saves a lot of brainfuck
result_crop <= resize(mul_result_fx, result_crop); 	--resize using defaults the code below throws an error but defaults 
													--should just work fine
													
--result_crop <= resize(	arg => mul_result_fx,		--number to be resized
--						size_res => result_crop, 	--resize to crop vector length
--						overflow_style => TRUE,		--saturate number
--						round_style => FALSE		--don't round, just cut
--						);

--to convert fixed point back to std_logic_vector use "to_slv" conversion function
dout <= to_slv(result_crop);

end;		