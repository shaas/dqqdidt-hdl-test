LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
use ieee.numeric_std.all;

---------------------------------------------------------------------------------------------------------
-- Company: CERN
--
-- File: median.vhd
-- File history:
--      0.1: 08/03/2011: 	initial version, 
--		0.2: 08/08/2011:	fixed width version, simulated, works
--		0.3: 08/09/2011: 	parametrized by generics, simulated, works
--		0.4: 10/23/2011:	parametrized data width from 16 to 24 bit
--		0.5: 11/07/2011: 	enhanced data width to 26 bit (unfortunately data path is now 25..)
--		0.6: 14/02/2018:    dblascos: changed d_width range up to 32 bits
--     
--	<Revision number>: <Date>: <Comments>
--
-- Description: 
-- 				Implements a median filter supporting partially sorted arrays
--				The algorithm takes a new sample and adds it to an already sorted array
--				Array comes pre-filled out of reset !
--	
-- Function: 	
--				
--
-- ToDo:		
--				
--
--
-- Targeted device: Actel ProAsic3 A3PE1500 PQ208
-- Author: Jens Steckert
--
----------------------------------------------------------------------------------------------------------
entity median_filter is
	generic(
		d_width : integer range 15 to 32 := 24;
		max_depth   : integer range 2 to 126 := 6
		--middle  : integer range 1 to 62  := 3
	);
	port(
		clk          : in  std_logic;
		rst          : in  std_logic;
		sample_clock : in  std_logic;
		val_in       : in  std_logic_vector(d_width-1 downto 0);
		out_valid    : out std_logic;
		median_out   : out std_logic_vector(d_width-1 downto 0);
		depth : in integer range 0 to max_depth-1 := 6;
		middle : in integer range 0 to max_depth/2  := 3
	);
end median_filter;

architecture a_median of median_filter is

	type state_type is (idle, newval, check_and_copy, copy_over, del_old, insert,
	                    swap, get_median);

	signal state : state_type;

	--resource estimation for dwidth 22, and depth 40 (middle 20) --> 10ms spike rejection
	--val_arr0 --> 40x22 = 880, TMR 2640 registers + voters
	--val_arr0 --> 40x22 = 880, TMR 2640 registers + voters

	type arr_dx16 is array (0 to max_depth) of signed(d_width-1 downto 0);
	signal val_arr0 : arr_dx16;
	signal val_arr1 : arr_dx16;

	type arr_dx3 is array (0 to max_depth) of integer range 0 to max_depth;
	signal age_arr0 : arr_dx3;
	signal age_arr1 : arr_dx3;

	--avoid using ram blocks for the median filter arrays (already short in ram on a3pe1500)

	attribute syn_ramstyle : string;
--	attribute syn_ramstyle of val_arr0 : signal is "registers";
--	attribute syn_ramstyle of val_arr1 : signal is "registers";
--	attribute syn_ramstyle of age_arr0 : signal is "registers";
--	attribute syn_ramstyle of age_arr1 : signal is "registers";
	attribute syn_ramstyle of val_arr0 : signal is "no_rw_check";
	attribute syn_ramstyle of val_arr1 : signal is "no_rw_check";
	attribute syn_ramstyle of age_arr0 : signal is "no_rw_check";
	attribute syn_ramstyle of age_arr1 : signal is "no_rw_check";

	signal cnt0          : integer range 0 to max_depth + 1;
	signal cnt1          : integer range 0 to max_depth + 1;
	signal new_value     : signed(d_width-1 downto 0);
	signal flag_inserted : std_logic;

	signal sample_pulse     : std_logic;
	signal sample_clock_old : std_logic;

begin

	new_value <= signed(val_in);

	--pulser creates one clock wide puls from rising edge of sample clock

	pulser : process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				sample_pulse     <= '0';
				sample_clock_old <= '0';
			else
				sample_clock_old <= sample_clock;
				if sample_clock_old < sample_clock then
					--rising edge
					sample_pulse <= '1';
				else
					sample_pulse <= '0';
				end if;
			end if;
		end if;
	end process;

	fsm : process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				--bring age array in a defined state
				for i in depth downto 0 loop
					val_arr0(i) <= to_signed(0, d_width);
				end loop;
				--pre-fill array with sorted default values
				for i in depth downto 0 loop
					age_arr0(i) <= i;
				end loop;
				--reset  val arr 1 
				for i in depth downto 0 loop
					val_arr1(i) <= (others => '0');
				end loop;

				cnt0          <= 0;
				cnt1          <= 0;
				out_valid     <= '0';
				flag_inserted <= '0';
				median_out    <= (others => '0');
				state         <= idle;
			else

				case state is

					when idle =>
						out_valid <= '0';
						if sample_pulse = '1' then
							state <= newval;
						else
							state <= idle;
						end if;

					when newval =>
						cnt0          <= 0; --reset array counter
						cnt1          <= 0;
						flag_inserted <= '0';
						out_valid     <= '0';
						state         <= check_and_copy;

					when check_and_copy =>
						--walk over array, 
						if cnt0 <= depth then
							--check age
							if new_value > val_arr0(cnt0) then --if new value is larger 
								if age_arr0(cnt0) < depth then --if current value in array is younger than 6 
									state <= copy_over; --copy old value in new array (same position)
								else    --current value is 6 samples old (needs to be deleted)
									state <= del_old;
								end if;

							else        --new value is larger than old value
								if flag_inserted = '0' then --if new value hasn't been inserted yet
									state <= insert;
								else    --value has been already inserted
									if age_arr0(cnt0) < depth then --if current value in array is younger than 6 
										state <= copy_over; --copy old value in new array (same position)
									else --current value is 6 samples old (needs to be deleted)
										state <= del_old;
									end if;
								end if;
							end if;
						else            --end of array;
							if flag_inserted = '1' then
								cnt0  <= 0;
								cnt1  <= 0;
								state <= swap;
							else
								--we're at the end of the array and so far nothing had been inserted
								--that means the new value is larger than all existing, so put as last
								--value
								state <= insert;
							end if;

						end if;
					when copy_over =>
						val_arr1(cnt1) <= val_arr0(cnt0);
						--increment the age of the current element and copy over to new array
						age_arr1(cnt1) <= age_arr0(cnt0) + 1;
						cnt0           <= cnt0 + 1; --increment counter origin
						cnt1           <= cnt1 + 1; --increment counter destination
						state          <= check_and_copy;

					when del_old =>
						cnt0  <= cnt0 + 1; --increment origin counter, keep destination counter
						state <= check_and_copy;

					when insert =>
						val_arr1(cnt1) <= new_value;
						age_arr1(cnt1) <= 0;
						flag_inserted  <= '1';
						cnt1           <= cnt1 + 1;
						state          <= check_and_copy;

					when swap =>
						--copy array1 to array 0
						if cnt0 <= depth then
							val_arr0(cnt0) <= val_arr1(cnt0);
							age_arr0(cnt0) <= age_arr1(cnt0);
							cnt0           <= cnt0 + 1;
							state          <= swap;
						else
							state <= get_median;
						end if;
					when get_median =>
						median_out <= std_logic_vector(val_arr0(middle));
						out_valid  <= '1';
						state      <= idle;
					when others =>
						state <= idle;

				end case;
			end if;
		end if;
	end process;

end;
