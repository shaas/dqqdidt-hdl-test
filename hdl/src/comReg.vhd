--------------------------------------------------------------------------------
--! Company: CERN
--!
--! @File: ComReg.vhd
-- File history:
--      v0.1	12/04/2017	first version for uQDS 2.0
--		v0.2 	10/10/2018 added register mask 
--      
--! @brief basic command interface register 
-- Description: 
--				
--
-- Targeted device: M2GL150
--! @Author: Jens Steckert
--
--------------------------------------------------------------------------------
library IEEE;
LIBRARY UQDSLib;

use IEEE.std_logic_1164.all;
use UQDSLib.UQDSLib.ALL;
use ieee.numeric_std.all;


entity ComReg is
generic(	
		dwidth 					: integer range 8 to 64 := 32
			);
port (clk, rst					: in std_logic;
		--reset default value 
		resetDefault			: in std_logic_vector(dwidth -1 downto 0);
		--register mask to limit value propagation
		registerMask			: in std_logic_vector(dwidth -1 downto 0);
		--register input in normal op
		din						: in std_logic_vector(dwidth -1 downto 0);
		--write enable enables writing to the registers (only relevant for the writable registers)
		wEN						: in std_logic;
		ReadOnly				: in std_logic;
		--freezes register update (global signal for all registers)
		freeze					: in std_logic;
		dout					: out std_logic_vector(dwidth -1 downto 0)
		);
end entity;

architecture rtl of ComReg is

signal REG : std_logic_vector(dwidth -1 downto 0);

begin

	reg_control: process(clk, rst)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				REG <= resetDefault;
			else
				--if register is write enabled OR ReadOnly Reg is updated with input
				--if freeze is active, register keeps content
				if freeze = '0' AND (wEN = '1' OR ReadOnly = '1') then
					REG <= din;
				else
					REG <= REG;
				end if;
			end if;
		end if;
	end process;

	
dout <= REG AND registerMask;

end;