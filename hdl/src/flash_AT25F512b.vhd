LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
use ieee.numeric_std.all;
---------------------------------------------------------------------------------------------------------
-- Company: CERN
--
-- File: flash_at25F512b.vhd
-- File history:
--      0.1: 10/07/2010: 	initial version, works
--      0.2: 10/08/2010:	added cs wait states 
--		0.2: 11/09/2011:	modified for two byte reading 
--		0.3: 04/30/2012:	added support for AT25DF021 --> 2Mbit (24-bit addressing)
--		1.0: 01/23/2018:	added support for 4-byte writes and read --> MSB first (lowest adr)
--	<Revision number>: <Date>: <Comments>
--
-- Description: 
--! @brief				interface to atmel at25F512b flash memory	
--	
-- Function: 	
--				provides read and write routines to the flash
--
-- ToDo:		write it !
--				
--
--
-- Targeted device: Actel ProAsic3 A3PE1500 PQ208
-- Author: Jens Steckert
--
----------------------------------------------------------------------------------------------------------
entity flash_at25f512b is
port (
    clk : in std_logic;
    rst : in std_logic;
	sck : out std_logic;
	so 	: in std_logic;
	si	: out std_logic;
	cs	: out std_logic;
	wp	: out std_logic;
	hold : out std_logic;
    cmd	: in std_logic_vector(3 downto 0);
	busy : out std_logic;
	pstatus : out std_logic_vector(7 downto 0);
	paddress : in std_logic_vector(23 downto 0);
	pwdata	: in std_logic_vector( 7 downto 0);
	pwdata2 : in std_logic_vector(7 downto 0);
	pwdata3	: in std_logic_vector(7 downto 0);
	pwdata4	: in std_logic_vector(7 downto 0);
	prdata2 : out std_logic_vector(15 downto 0);
	prdata2_valid : out std_logic;
	prdata	: out std_logic_vector(7 downto 0);
    prdata4 : out std_logic_vector(31 downto 0) 
);
end flash_at25f512b;


architecture flash_at25f512b_arch of flash_at25f512b is


signal write_multi_bytes	:std_logic_vector(1 downto 0);
signal read_multi_bytes	:std_logic_vector(1 downto 0);
signal cmd_vector	:std_logic_vector(63 downto 0);
signal address		:std_logic_vector(23 downto 0);

signal opcode		:std_logic_vector(7 downto 0);
signal clock_count	:integer range -1 to 62;
signal readdata		:std_logic_vector(31 downto 0);

signal status		:std_logic_vector(7 downto 0);
signal write_enable :std_logic;
signal waitcount	:integer range 0 to 15;
signal waiting_time	:integer range 0 to 15;
type state_type is (idle, do_read, do_read2, read_data, read_data2, read_data4,
					clk_lo_r, clk_hi_r, clk_lo_r2, clk_hi_r2, clk_hi_r4, clk_lo_r4,  wait_r,
					do_erase, erase_data, clk_lo_e, clk_hi_e, wait_e, 
					do_write, write_data, clk_lo_w, clk_hi_w, wait_w,
					do_read_stat, read_stat, clk_hi_s, clk_lo_s, wait_s,
					do_wp, write_protect, clk_hi_wp, clk_lo_wp, wait_wp,
					do_wstat, write_stat, clk_hi_wst, clk_lo_wst, wait_wst,
					do_swp, sector_protect, clk_hi_swp, clk_lo_swp, wait_swp);
					
signal state : state_type;

begin

hold <= '1'; --don't use that feature, always high
waiting_time <= 3;

fsm: process(clk)
begin
if (clk'event and clk= '1') then 
	if rst = '1' then
		write_enable <= '1';
		opcode <= (others => '0');
		status <= (others => '0');
		clock_count <= 0;
		write_enable <= '0';
		cmd_vector <= (others => '0');
		address <= (others => '0');
		readdata <=(others => '0');
		state <= idle;
		waitcount <= 3;
		busy <= '1';
		si <= '0';
		cs <= '1';
		sck <= '0';
		pstatus <= (others => '0');
		prdata <= (others => '0');
		write_multi_bytes <= "00";
		read_multi_bytes  <= "00";
		prdata2 <= (others => '0');
		prdata2_valid <= '0';
	else
		case state is
			when idle =>
				cs <= '1';
				wp <= '1'; --pull wp high to enable write access;
				waitcount <= waiting_time;
				
				prdata2_valid <= '0';
				if status(0) = '1' then --check if device is busy, poll status until not busy
					busy <= '1';
					state <= do_read_stat;
				else
					--device not busy, accept commands
					case cmd is
						when "0000" =>
							busy <= '0';
							
							state <= idle;
						when "0001" => --read from flash 
							opcode <= x"03"; --slow <33Mhz read
							address <= "000000" & paddress(17 downto 0);
							busy <= '1';
							read_multi_bytes <= "01";
							state <= do_read;
						when "0010" =>	--write byte to array
							opcode <= x"02";
							address <= "000000" & paddress(17 downto 0);
							busy <= '1';
							write_multi_bytes <= "01";
							state <= do_write;
						when "1110" => --write two bytes to array
							opcode <= x"02";
							address <= "000000" & paddress(17 downto 0);
							busy <= '1';
							write_multi_bytes <= "10";	
							state <= do_write;
						when "1111" =>  --write four bytes to array
							opcode <= x"02";
							address <= "000000" & paddress(17 downto 0);
							busy <= '1';
							write_multi_bytes <= "11";	
							state <= do_write;
						when "0011" => --block erase
							opcode <= x"20"; --4k page erase
							address <= "000000" & paddress(17 downto 0);
							busy <= '1';
							state <= do_erase;
						
						when "0100" => --read status from flash
							busy <= '1';
							state <= do_read_stat;
						when "0101" =>	--disable writing 
							write_enable <= '0';
							busy <= '1';
							state <= do_wp;
						when "0110" => --enable writing
							write_enable <= '1';
							busy <= '1';
							state <= do_wp;
						when "0111" => --read two bytes
							opcode <= x"03"; --slow <33MHz read
							address <= "000000" & paddress(17 downto 0);
							busy <= '1';
							read_multi_bytes <= "10";
							state <= do_read2; --read 2 bytes
						when "1100" => --read four bytes
							opcode <= x"03"; --slow <33MHz read
							address <= "000000" & paddress(17 downto 0);
							busy <= '1';
							read_multi_bytes <= "11";
							state <= do_read2; --read 2 bytes	
							
						------------------------------------------------------------
						--advanced commands for 2 Mbit flash AT25DF021
						when "1000" => --protect sector
							opcode <= x"36";	
							address <= "000000" & paddress(17 downto 0);
							busy <= '1';
							state <= do_swp;
							
						when "1001" => -- unprotect sector
							opcode <= x"39";
							address <= "000000" & paddress(17 downto 0);
							busy <= '1';
							
							state <= do_swp;
						when "1010" => --write to status register
							
							opcode <= x"01";
							busy <= '1';
							state <= do_wstat;
							
						when others => 
							busy <= '0';
							state <= idle;
					end case;
				end if;
			------------------------------------read data-------------------------------------------	
			when do_read =>
				cs <= '0'; --set cs low to wake up device
				sck <= '0'; --set clock low
				clock_count <= 39;
				cmd_vector <= x"000000" & opcode & address & x"00"; --assemble command vector (opcode + adress)
				state <= read_data;
			when read_data =>
				if clock_count >= 0 then
					clock_count <= clock_count -1;
					
					if clock_count < 8 then
						readdata(clock_count) <= so;
					else
						si <= cmd_vector(clock_count);
					end if;
					
					state <= clk_hi_r;
				else
					prdata <= readdata(7 downto 0);
					waitcount <= waiting_time;
					state <= wait_r;
					
				end if;
			when clk_hi_r =>
				sck <= '1';
				state <= clk_lo_r;
			when clk_lo_r =>
				sck <= '0';
				state <= read_data;
			
			------------------------------------read data (two/ four byte)-------------------------------------------	
			
			when do_read2 =>
				--perform a 2-byte read
				cs <= '0'; --set cs low to wake up device
				sck <= '0'; --set clock low
				case read_multi_bytes is
					when "10" =>
						--read two bytes
						cmd_vector <= x"0000" & opcode & address & x"0000"; --assemble command vector (opcode + adress + space holder for data)
						clock_count <= 47;
						state <= read_data2;
					when "11" => 
						--read four bytes
						cmd_vector <= opcode & address & x"00000000"; --assemble command vector (opcode + adress + space holder for data)
						clock_count <= 63;
						state <= read_data4;
					when others => null;
				end case;
				
				
			when read_data2 =>
				if clock_count >= 0 then
					clock_count <= clock_count -1;
					if clock_count < 16 then
						readdata(clock_count) <= so;
					else
						si <= cmd_vector(clock_count);
					end if;
		
					state <= clk_hi_r2;
				else
					waitcount <= waiting_time;
					prdata2 <= readdata(15 downto 0);
					state <= wait_r;
				end if;
			when clk_hi_r2 =>
				sck <= '1';
				state <= clk_lo_r2;
			when clk_lo_r2 =>
				sck <= '0';
				state <= read_data2;
					
			when read_data4 =>
				--byte 0 (lowest adr in flash) of the four byte vector readdata will be at postition 31 downto 24 !!!
				if clock_count >= 0 then
					clock_count <= clock_count -1;
					if clock_count < 32 then
						readdata(clock_count) <= so;
					else
						si <= cmd_vector(clock_count);
					end if;
		
					state <= clk_hi_r4;
				else
					waitcount <= waiting_time;
					prdata4 <= readdata;
					state <= wait_r;
				end if;
				
			when clk_hi_r4 =>
				sck <= '1';
				state <= clk_lo_r4;
			when clk_lo_r4 =>
				sck <= '0';
				state <= read_data4;
				
			when wait_r =>
				cs <= '1';
				if waitcount >0 then
					waitcount <= waitcount -1;
					state <= wait_r;
				else
					prdata2_valid <= '1';
					state <= idle;
				end if;
			------------------------------------erase------------------------------------------	
			when do_erase =>
				cs <= '0';
				sck <= '0';
				clock_count <= 31;
				cmd_vector <= x"00000000" & opcode &  address;
				state <= erase_data;
			when erase_data =>
				if clock_count >= 0 then
					clock_count <= clock_count -1;
					si <= cmd_vector(clock_count); --clock out data
					state <= clk_hi_e;
				else
					cs <= '1';
					waitcount <= waiting_time;
					state <= wait_e;
				end if;
			when clk_hi_e => --set clock high
				sck <= '1';
				state <= clk_lo_e;
			when clk_lo_e => --set clock low
				sck <= '0';
				state <= erase_data;
			when wait_e =>	--wait 4 clocks with cs_high to meet the min 50ns tcsh of the device
				if waitcount >0 then
					cs <= '1';
					waitcount <= waitcount -1;
					state <= wait_e;
				else
					state <= do_read_stat;
				end if;
			------------------------------------read status--------------------------------------	
			when do_read_stat =>								--set chip select low
				cs <= '0';
				sck <= '0';
				clock_count <= 15;
				cmd_vector <= x"000000000000" & x"05" & x"00";
				state <= read_stat;
			when read_stat =>									--if clock_count > 8 put data on the bus
				if clock_count >= 0 then
					clock_count <= clock_count -1;
					if clock_count < 8 then
						status(clock_count) <= so;
					else
						si <= cmd_vector(clock_count);
					end if;
					state <= clk_hi_s;
				else
					pstatus <= status;
					waitcount <= waiting_time;
					state <= wait_s;
				end if;
			when clk_hi_s =>									--clock high
				sck <= '1';
				state <= clk_lo_s;
			when clk_lo_s =>									--clock low
				sck <= '0';
				state <= read_stat;
			when wait_s =>										--put chip select high, end of transmission
				cs <= '1';
				if waitcount >0 then
					waitcount <= waitcount -1;
					state <= wait_s;
				else
					state <= idle;
				end if;
			----------------------------------write protect/enable--------------------------------
			when do_wp =>
				cs <= '0';
				sck <= '0';
				clock_count <= 7;
				if write_enable = '1' then
					cmd_vector <= x"00000000000000" & x"06"; --enables writing
				else
					cmd_vector <= x"00000000000000" & x"04"; --disables writing
				end if;
				state <= write_protect;
			when write_protect =>
				if clock_count >= 0 then
					clock_count <= clock_count -1;
					si <= cmd_vector(clock_count);
					state <= clk_hi_wp;
				else
					waitcount <= waiting_time;
					state <= wait_wp;
				end if;
			when clk_hi_wp =>
				sck <= '1';
				state <= clk_lo_wp;
			when clk_lo_wp =>
				sck <= '0';
				state <= write_protect;
			when wait_wp =>
				cs <= '1';
				if waitcount >0 then
					waitcount <= waitcount -1;
					state <= wait_wp;
				else
					state <= idle;
				end if;
			----------------------------------sector protect/unprotect
			when do_swp =>
				cs <= '0';
				sck <= '0';
				clock_count <= 31;
				cmd_vector <= x"00000000" & opcode & address; --pad cmd_vector to 64 bit
				state <= sector_protect;
			when sector_protect =>
				if clock_count >= 0 then
					clock_count <= clock_count -1;
					si <= cmd_vector(clock_count);
					state <= clk_hi_swp;
				else
					waitcount <= waiting_time;
					state <= wait_swp;
				end if;
			when clk_hi_swp =>
				sck <= '1';
				state <= clk_lo_swp;
			when clk_lo_swp =>
				sck <= '0';
				state <= sector_protect;
			when wait_swp =>
				cs <= '1';
				if waitcount >0 then
					waitcount <= waitcount -1;
					state <= wait_swp;
				else
					state <= idle;
				end if;
			--------------------------------------write status register--------------------------
			when do_wstat =>
				cs <= '0';
				sck <= '0';
				clock_count <= 15;
				cmd_vector <= x"000000000000" & opcode & pwdata; --pad cmd_vector to 64 bit
				state <= write_stat;
			when write_stat =>
				if clock_count >= 0 then
					clock_count <= clock_count -1;
					si <= cmd_vector(clock_count);
					state <= clk_hi_wst;
				else
					waitcount <= waiting_time;
					state <= wait_wst;
				end if;
			when clk_hi_wst =>
				sck <= '1';
				state <= clk_lo_wst;
			when clk_lo_wst =>
				sck <= '0';
				state <= write_stat;
			when wait_wst =>
				cs <= '1';
				if waitcount >0 then
					waitcount <= waitcount -1;
					state <= wait_wst;
				else
					state <= idle;
				end if;
			
			----------------------------------write data------------------------------------------
			when do_write =>
				cs <= '0';
				sck <= '0';
				case write_multi_bytes is
					when "01" => 
						--write one byte;
						clock_count <= 39;
						cmd_vector <= x"000000" & opcode & address & pwdata; --assemble command vector (opcode + adress)
					when "10" => 
						--write two bytes
						clock_count <= 47;
						cmd_vector <= x"0000" & opcode & address & pwdata2 & pwdata; --assemble command vector (opcode + adress)
					when "11" => 
						--write four bytes
						clock_count <= 63;
						--write to flash MSB first (lowest adr)
						cmd_vector <= opcode & address & pwdata4 & pwdata3 & pwdata2 & pwdata; --assemble command vector (opcode + adress)
										
					when others => null;
				end case;
				
				
				state <= write_data;
			when write_data =>
				if clock_count >= 0 then
					clock_count <= clock_count -1;
					si <= cmd_vector(clock_count);
					state <= clk_hi_w;
				else
					cs <= '1';
					waitcount <= waiting_time;
					state <= wait_w; --read status and return to idle afterwards
				end if;
			when clk_hi_w =>
				sck <= '1';
				state <= clk_lo_w;
			when clk_lo_w =>
				sck <= '0';
				state <= write_data;
			when wait_w =>
				cs <= '1';
				if waitcount >0 then
					waitcount <= waitcount -1;
					state <= wait_w;
				else
					state <= do_read_stat;
				end if;
			when others =>
				state <= idle;
		end case;
	end if;end if;
end process;

end;
