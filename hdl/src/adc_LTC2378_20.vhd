--------------------------------------------------------------------------------
-- Company: CERN
--
-- File: adc_LTC2378_20.vhd
-- File history:
--      <Revision number>: <Designer>: <Date>: <Comments>
-- 		v1.1 (dblascos) ??/??/????	added watchdog when waiting for busy signal
--		v1.2 (as)		01/08/2019	correction to send 20 sck pulses to ADC
--
-- Description: interface to ADC LTC2378-20
-- state READOUT_1 is used to preserv doubling of MSB possible due to delayes of introduced by digital isolator ISO7842 on sdo line
-- 16ns is introduced by digital isolator on sck line 
-- and again 16ns is introduced by digital isolator on sdo line -> total 32 ns
-- state WAIT_2 is used adjust sample rate: when 2 -> 740.7407 kHZ, when 1 -> 754.717 kHz, when 0 -> 769.2308 kHz (at system clock 40MHz, 25ns)
-- the range for cnt_read is extended to -1 in order to correct aquisition of LSB
-- 
-- Targeted device: <Family::IGLOO2> <Die::M2GL150> <Package::1152 FBGA>
-- Author: Jelena Spasic
-- Modified by: Surbhi Mundra
-- Comment: Simplified the state diagram
--------------------------------------------------------------------------------
library IEEE;

USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

entity adc_LTC2378_20 is
	port(
		-- FPGA side
		clk        : in  std_logic;
		reset      : in  std_logic;
		start_conv : in  std_logic;
		data_rdy   : out std_logic;
		data_out   : out std_logic_vector(19 downto 0);
		adc_constant: out std_logic;
		adc_stuck : out std_logic;
		-- ADC side
		sdi        : in  std_logic;
		sck        : out std_logic;
		busy       : in  std_logic;
		conv       : out std_logic
	);
end adc_LTC2378_20;

architecture arch of adc_LTC2378_20 is

	constant N_READ   : integer := 20;
	--constant N_START  : integer := 0;
	--constant N_WAIT_1 : integer := 0;
	--constant N_WAIT_2 : integer := 1;		--7.54717 MHz

	constant WATCHDOG_MAX : integer := 1000; -- 25ms at 40MHz
	constant max_samples_to_check : integer := 20; -- ~1ms at Global Heart Beat @195

	type state_type is (IDLE, CONVERSION, WAIT0, READOUT
);		--WAIT_1, 

	signal state  : state_type;
	signal data_buffer    : std_logic_vector(19 downto 0);
	signal cnt_read       : integer range -1 to 31;
    signal sck_enable     : std_logic:='0';
	
	signal watchdog : integer range 0 to WATCHDOG_MAX - 1;
	signal prev_data : std_logic_vector(19 downto 0);
	signal counter   : integer range 0 to max_samples_to_check;
	signal drdy,busy_sync 	 : std_logic;

begin


	watchdog_counter : process(clk,reset)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				adc_stuck <= '0';
				watchdog   <= WATCHDOG_MAX - 1;
			else
                busy_sync <= busy;
                if watchdog > 0 then
                    if busy_sync = busy then
                        watchdog <= watchdog - 1;
                    else
                        watchdog   <= WATCHDOG_MAX - 1;
                    end if;
                else
                    adc_stuck <= '1';
                    watchdog   <= WATCHDOG_MAX - 1;
                end if;
            end if;
		end if;
	end process;

	malfunction_detector : process(clk,reset)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				counter     <= 0;
				drdy    <= '0';
				prev_data   <= (others => '0');
				adc_constant <= '0';
			else
				drdy <= data_rdy;
				if (data_rdy > drdy) then

					prev_data <= data_out;
					if (data_out = prev_data) then
						if (counter < max_samples_to_check) then
							counter     <= counter + 1;
							--adc_constant <= '0';
						else
							adc_constant <= '1';
						end if;

					else
						counter <= 0;
						--adc_constant <= '0';
					end if;

				end if;
			end if;
		end if;
	end process;
	
    sck <= sck_enable and clk; 
	
	fsm: process(clk, reset) begin
    if rising_edge(clk) then
        if reset='1' then
            data_rdy <='0';
            data_buffer <= (others=>'0');
            conv <='0';
            sck_enable <= '0';
            state <= IDLE;
            data_out <= (others=>'0');
        else
            case state is
			when IDLE =>
				if start_conv = '1' then
					state <= CONVERSION;
                    conv <='1';
				else
					state <= IDLE;
				end if;
                data_rdy <='0';
                sck_enable <= '0';
                data_buffer <= (others=>'0');
			when CONVERSION =>
                conv <= '0';
                if busy = '1' then
					state <= CONVERSION;
                    if watchdog = 0 then
                        state <= IDLE;
                    end if;
				else
					state <= WAIT0;
				end if;
			when WAIT0 =>
				state <= READOUT;
                cnt_read    <= N_READ;
			when READOUT =>
                sck_enable <= '1';
				if cnt_read < 0 then
					state <= IDLE;
                    data_rdy <='1';
                    data_out <= data_buffer;
                    sck_enable <= '0';
				else
					state <= READOUT;
                    cnt_read    <= cnt_read - 1;
                    data_buffer <= data_buffer(18 downto 0) & sdi;
				end if;
            end case;
        end if;
    end if;
	end process;

end arch;
