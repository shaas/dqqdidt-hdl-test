library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library UQDSlib;
use UQDSlib.UQDSLib.ALL;
use UQDSLib.config_block_lib.all;
--------------------------------------------------------------------------------
-- Company: CERN
--
-- File: decimating_buffer.vhd
-- File history:
--      v0.1    16/05/2017	first version
--      v0.2     8/08/2017	useless and equivalent states eliminated; when decimation_bits=4, SPI frame (between two consecutive CS rising edge) must be at least 39 us ! 
--		v1.0	10/10/2019	Corrected several bugs, version for 32 bit -wide input data		
-- Description: 
--
--   Implements a rolling buffer. Once triggered, will continue recording for the  
--	 specified amount of words. Then stops. 
--	 Samples are arranged in packets with nb_channels samples (one per channel).
--	 Read out can be performed in normal or in 
--	 decimated mode. Normal mode returns all samples. 
--	 Decimated mode returns every 2 to power decimation_bits samples.
--	
--	 If channel is marked as "logic channel" a non-averaging decimation method is used
--	 To avoid glitches, post_trigger_samples should be < words_max/2

--	Command vector encoding:
--		(0) --> read_pulse_decimated
--		(1) --> read_pulse_all_samples
--		(2) --> restart_reading (set read pointer to start position)
--		(3)	--> rearm_command
--		(4..7) reserved
--
-- Targeted device: ProASIC3e A3PE1500 PQG208
-- Author: Jens Steckert (CERN), Andrzej Skoczen (AGH-UST)
--
--------------------------------------------------------------------------------

entity decimating_buffer is
	generic(nb_channels          : integer range 1 to 32      := 7;
	        address_width        : integer range 2 to 22      := 17;
	        --decimation 				: integer range 0 to 31 := 4; --number of samples to be added up (must be 2^n)
	        decimation_bits      : integer range 0 to 6       := 2; --number of bits (n) the sum vector needs to be xtended
	        words_max            : integer range 3 to 1048576 := 1048574; --number of words saved in memory
	        post_trigger_samples : integer range 3 to 1048576 := 524228); --number of samples recorded after trigger
	port(
		clk, rst       : in  std_logic;
		--cypress ram interface signals
		cy_adr_in      : out std_logic_vector(address_width - 1 downto 0);
		cy_rdata       : in  std_logic_vector(adc_data_width - 1 downto 0);
		cy_wdata       : out std_logic_vector(adc_data_width - 1 downto 0);
		cy_r_done      : in  std_logic;
		cy_w_done      : in  std_logic;
		cy_cmd         : out std_logic_vector(1 downto 0);
		--Input data
		input_array    : in  channelsArray(nb_channels - 1 downto 0);
		logic_channels : in std_logic_vector(nb_channels - 1 downto 0);
		--Command pulses 						Sensitive state
		dready         : in  std_logic; --IDLE
		buffer_trigger : in  std_logic; --IDLE

		--read_pulse_all		:in std_logic;		--READY_TO_READ
		--read_pulse_dec		:in std_logic;		--READY_TO_READ

		--rearm_pulse 		:in std_logic;		--READT_TO_READ
		cmd_vector     : in  std_logic_vector(7 downto 0);
		output_array   : out channelsArray(nb_channels - 1 downto 0);
		--output_ready 		:out std_logic;
		status         : out std_logic_vector(7 downto 0);
		data_r_debug   : out std_logic_vector(adc_data_width - 1 downto 0);
		data_w_debug   : out std_logic_vector(adc_data_width - 1 downto 0);
		debug_vector   : out std_logic_vector(7 downto 0)
	);
end decimating_buffer;

architecture RTL of decimating_buffer is
	type state_type is (IDLE,           --x01
	                    REC,            --x02
	                    REC_W0,         --x03
	                    REC_W1,         --x04
	                    REC_INC_SAMPLE, --x05
	                    REC_INC_WORD,   --x06
	                    REC_CHECK_PTRIG, --x07
	                    READY_TO_READ,  --x08
	                    READM,          --x09
	                    READM_W,        --x0B
						READM_W_LOW16,
						READM_W_HIGH16,
						READM_ASSIGN_VAL,
	                    READM_INC_SAMPLE, --x0C
	                    READM_INC_WORD, --x0D						
	                    READM_CHECK_END, --x0E
	                    READ_DEC_PROPAGATE, --x15
	                    REARM           --x16
	                   );

	type dec_vect_array is array (natural range <>) of std_logic_vector((32 - 1) + decimation_bits downto 0);
	signal state                                    : state_type;
	signal word_adr, stop_position                  : integer range 0 to sram_depth - 1; --one word contains several samples
	signal sample_count                             : integer range 0 to nb_channels; --one sample is one ADC sample 16bit
	signal dec_count                                : integer range 0 to 63;
	signal post_trigger_sample_count                : integer range 0 to sram_depth - 1; --post trigger counter
	signal decimation_buffer                        : dec_vect_array(nb_channels - 1 downto 0);
	signal buffer_triggered, buffer_triggered_pulse : std_logic;
	signal dec, buffer_trigger_old                  : std_logic;
	signal sample_buffer_Low, sample_buffer_high	: std_logic_vector(15 downto 0);
	signal input_array_reg : channelsArray(nb_channels - 1 downto 0);

	signal packet_second_half : std_logic;

	constant dec_n_samples : natural := 2**decimation_bits;

	function sl2int(x : std_logic) return integer is
	begin
		if x = '1' then
			return 1;
		else
			return 0;
		end if;
	end;

begin

	--status

	--status(0) '1' when recording, '0' when buffer is not recording
	--status(1) '1' when buffer is read decimated , '0' when buffer has not been started to read or is already 
	--status(2) '1' when buffer is read , '0' when buffer has not been started to read or is already 
	--status(3) '1' when buffer has been read completely, '0' while reading or when not t
	--status(4) '1' when buffer records after trigger, 
	--status(7) '1' when one packet of data are available on buffer output, '0' Command BUFFER_READ was sent

	data_r_debug <= cy_rdata;
	data_w_debug <= cy_wdata;

	process(clk, rst) is
	begin
		if rising_edge(clk) then
			if rst = '1' or state = REARM then
				buffer_triggered       <= '0';
				buffer_trigger_old     <= '0';
				buffer_triggered_pulse <= '0';
			else
				buffer_trigger_old <= buffer_trigger;
				--detect rising edge of buffer trigger signal
				if buffer_trigger_old < buffer_trigger then
					buffer_triggered_pulse <= '1';
					buffer_triggered       <= '1';
				else
					buffer_triggered_pulse <= '0';
				end if;
			end if;
		end if;
	end process;

	process(clk, rst) is
		variable decimation_packet : std_logic_vector(31 downto 0);
	begin
		if rising_edge(clk) then
			if rst = '1' then
				state                     <= IDLE;
				--output_ready <= '0';
				post_trigger_sample_count <= post_trigger_samples; --0;
				word_adr                  <= 0;
				sample_count              <= 0;
				dec_count                 <= 0;
				cy_adr_in                 <= (others => '0');
				cy_wdata                  <= (others => '0');
				cy_cmd                    <= (others => '0');

				status        <= (others => '0');
				stop_position <= 0;
				debug_vector  <= x"00";

				input_array_reg    <= (others => (others => '0'));
				packet_second_half <= '0';
			else

				case state is
					when IDLE =>
						debug_vector <= x"01";
						if buffer_triggered = '1' then
							status(4) <= '1';
						end if;
						if dready = '1' then
							--new sample arrived
							state     <= REC;
							status(0) <= '1';

							input_array_reg <= input_array;

						else
							state <= IDLE;
						end if;

					when REC =>
						debug_vector <= x"02";
						cy_adr_in    <= std_logic_vector(to_unsigned(word_adr + 2*sample_count + sl2int(packet_second_half), cy_adr_in'length)); --set address
						if (packet_second_half = '0') then
							cy_wdata <= input_array_reg(sample_count)(15 downto 0); --set data
						else
							cy_wdata <= input_array_reg(sample_count)(31 downto 16); --set data
						end if;
						cy_cmd       <= "10"; --write to memory
						status(2)    <= '0';
						status(7)    <= '0';
						state        <= REC_W0;

					when REC_W0 =>
						debug_vector <= x"03";
						state        <= REC_W1;

					when REC_W1 =>
						debug_vector <= x"04";
						cy_cmd       <= "00"; --set mem idle
						if cy_w_done = '1' then
							state <= REC_INC_SAMPLE;
						else
							state <= REC_W1;
						end if;

					when REC_INC_SAMPLE => --increment sample counter of one word (nb_channels of samples)
						debug_vector <= x"05";
						if packet_second_half = '0' then
							packet_second_half <= '1';
							state              <= REC;
						elsif sample_count < (nb_channels - 1) then
							packet_second_half <= '0';
							sample_count       <= sample_count + 1;
							state              <= REC;
						else
							packet_second_half <= '0';
							sample_count       <= 0;
							state              <= REC_INC_WORD;
						end if;

					when REC_INC_WORD =>
						debug_vector <= x"06";
						if word_adr + 2*sample_count < words_max - 2*nb_channels then
							word_adr <= word_adr + 2*nb_channels;
						else
							word_adr     <= 0; --end of memory reached set back to 0
							sample_count <= 0;
						end if;
						state        <= REC_CHECK_PTRIG;

					when REC_CHECK_PTRIG =>
						debug_vector <= x"07";
						if buffer_triggered = '1' then
							--buffer had been triggered, decrease post-trigger counter and
							if post_trigger_sample_count >= 2*nb_channels then
								post_trigger_sample_count <= post_trigger_sample_count - 2*nb_channels; --1;
								state                     <= IDLE;
							else
								--save the end position of the buffer and proceed to ready to read state
								stop_position <= word_adr;
								status(4)     <= '0';
								state         <= READY_TO_READ;
							end if;
						else
							--no trigger, just continue recording
							state <= IDLE;
						end if;
					when READY_TO_READ =>
						debug_vector <= x"08";
						status(0)    <= '0';
						sample_count <= 0;
						dec          <= '0';
						if cmd_vector(0) = '1' then

							dec_count         <= 0;
							--zero decimation buffer
							decimation_buffer <= (others => (others => '0'));
							dec               <= '1';
							state             <= READM; --_DEC;

						elsif cmd_vector(1) = '1' and status(3) = '0' then --suspend next reading
							--sample_count <= 0;
							state <= READM; --_ALL;

						elsif cmd_vector(2) = '1' then
							word_adr           <= stop_position; -- nb_channels;
							packet_second_half <= '0';
							state              <= READY_TO_READ;
							case status(3 downto 0) is
								when "1010" => state <= READM;
									dec   <= '1';
								when "1100" => state <= READM;
									dec   <= '0';
								when others => null;
							end case;
						--changed logic here: buffer can be always reset (doesn't have to be fully read)
						elsif cmd_vector(3) = '1' then -- and status(3) = '1' then	--to be sure that buffer is already read		

							state <= REARM;

						else

							state <= READY_TO_READ;
						end if;
					--===================================COMMON FOR FULL AND DECIMATED READ===========================================					
					when READM =>
						--setup read of lower 16 bit
						debug_vector       <= x"09";
						status(3 downto 1) <= '0' & not dec & dec;
						cy_adr_in          <= std_logic_vector(to_unsigned(word_adr + 2*sample_count , cy_adr_in'length));
						cy_cmd             <= "01"; --read ram
						status(7)          <= '0';
						
						state              <= READM_W_LOW16;
						

					when READM_W_LOW16 =>     --wait for requested lower 16bit sample and request reading the upper 16 bit
						debug_vector       <= x"0A";
						cy_cmd       <= "00"; --set ram back to idle
						
						if cy_r_done = '1' then
							--buffer lower 16 bit
							sample_buffer_Low <= cy_rdata;
								
							--increment ram address, and give command to read higher 16 bit
							status(3 downto 1) <= '0' & not dec & dec;
							cy_adr_in          <= std_logic_vector(to_unsigned(word_adr + 2*sample_count + 1, cy_adr_in'length));--add one for higher 16 bit
							cy_cmd             <= "01"; --read ram
							status(7)          <= '0';
								
							state <= READM_W_HIGH16;
								
						else
							state <= READM_W_LOW16;
						end if;
						
					when READM_W_HIGH16 =>	--wait for the upper 16 bit
						debug_vector       <= x"0B";
						cy_cmd       <= "00"; --set ram back to idle
						
						if cy_r_done = '1' then
							--buffer higher 16 bit
							sample_buffer_high <= cy_rdata;
								
								
							state <= READM_ASSIGN_VAL;
								
						else
							--wait for sram
							state <= READM_W_HIGH16;
						end if;
					

					when READM_ASSIGN_VAL =>
						debug_vector       <= x"0C";
						--Assign the value from SRAM to output ports (undecimated read) or decimation_buffer (decimated read)
						
						--check decimated read
						if dec = '1' then
							--if channel is a logic channel
							if logic_channels(sample_count) = '1' then
								--just update lower 32 bits of decimation buffer
								decimation_buffer(sample_count)(31 downto 0) <= sample_buffer_high & sample_buffer_Low;
							else
							-- analogue channel add new values to decimation buffer
								decimation_buffer(sample_count) <= std_logic_vector(signed(decimation_buffer(sample_count)) + signed(sample_buffer_high & sample_buffer_Low));
							end if;
						
						--undecimated read
						else
							output_array(sample_count) <= sample_buffer_high & sample_buffer_Low;
						end if;
						
						state <= READM_INC_SAMPLE;
					
					

					when READM_INC_SAMPLE =>
						debug_vector       <= x"0D";
						
						
						--If lower 16 bit are selected (packet_second_half = 0) generate new ram address
						if sample_count < (nb_channels -1) then --or packet_second_half = '0' then --IMPORTANT check against nb_channels (why ??? 25.10.2019)
							sample_count <= sample_count + 1;
							
							
							state              <= READM;
							status(7)          <= '0';
						else
							--if all samples of one word (nb_channels) are read increment word
							if dec = '1' then
								sample_count <= 0;
							end if;
							state <= READM_INC_WORD;
						end if;

					when READM_INC_WORD =>
						debug_vector <= x"0E";
						if (word_adr + 2*nb_channels) < words_max then
							word_adr <= word_adr + 2*nb_channels;
						else
							word_adr <= 0;
						end if;
						if dec = '1' then
							status(7) <= '0';
						else
							status(7) <= '1';
						end if;

						state <= READM_CHECK_END;

					when READM_CHECK_END =>
						debug_vector <= x"0F";
						if word_adr = stop_position then
							--put some signal to show that buffer is finished
							status(3) <= '1';
						else
							status(3) <= '0';
						end if;
						if dec = '1' then
							state <= READ_DEC_PROPAGATE;
						else
							state <= READY_TO_READ;
						end if;
					--=======================================DECIMATED READ==============================	
					when READ_DEC_PROPAGATE =>
						debug_vector <= x"10";
						if dec_count < (dec_n_samples - 1) then
							dec_count <= dec_count + 1;
							state     <= READM; --_DEC;
						else
							--samples accumulated
							dec_count                     <= 0;
							for I in 0 to (nb_channels - 1) loop
								output_array(I) <= decimation_buffer(I)((32 - 1) + decimation_bits downto decimation_bits);
								if logic_channels(I) = '1' then
									--logic channels take last value read from buffer (not decimated) since it contains logic flags
									output_array(I) <= decimation_buffer(I)((32 - 1) downto 0);
								end if;
							end loop;
							
							status(7)                     <= '1';
							state                         <= READY_TO_READ;
						end if;

					when REARM =>
						debug_vector              <= x"11";
						--Re-Arm buffer after re-arm command
						post_trigger_sample_count <= post_trigger_samples; --0;
						--buffer_triggered <= '0';
						dec_count                 <= 0;
						word_adr                  <= 0;
						sample_count              <= 0;
						stop_position             <= 0;
						packet_second_half        <= '0';
						status(3)                 <= '0';
						status(7)                 <= '0';
						state                     <= IDLE;
					when others =>
						state <= IDLE;
				end case;
			end if;
		end if;
	end process;

end architecture RTL;