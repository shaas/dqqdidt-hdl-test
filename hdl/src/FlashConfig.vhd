library ieee;
LIBRARY UQDSLib;
--------------------------------------------------------------------------------
-- Company: CERN
--
-- File: Flash_Config.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--		  	v1 			:	18.01.2018 : first version
--			v1.1		:	23.01.2018 : implemented auto startup and fixed 
--										 byte-order bugs --> works
--
-- Description: 
-- entity writes the contents of a register file to the flash memory and loads the 
-- data from flash. Controlled by "cmd"
--
-- CMD: x0000: IDLE 
-- CMD: x0001: WRITE register file content to flash memory block <flash_block>
-- CMD: x0010: READ register file content from <flash block>
-- CMD: x0011: Write start-up condition, start from block 1 - 7, 
--			   block 0 disables auto configure after reset
--
-- Targeted device: originally (<Family::IGLOO2> <Die::M2GL060> <Package::484 FBGA>)
--
-- Author: Jens Steckert
-- 
--
--------------------------------------------------------------------------------
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use UQDSLib.UQDSLib.ALL;

entity FlashConfig is
	port(
		clk               : in  std_logic;
		rst               : in  std_logic;
		--command of the flash config entity, drives the main fsm
		cmd               : in  std_logic_vector(3 downto 0);
		flash_block       : in  std_logic_vector(3 downto 0);
		flash_data_w      : in  std_logic_vector(REGISTER_SIZE - 1 downto 0);
		flash_data_r      : out std_logic_vector(REGISTER_SIZE - 1 downto 0);
		--write enable for com reg (recall from flash)
		reg_WEN           : out std_logic_vector(2**REGISTER_ADR_WIDTH - 1 downto 0);
		--multplexer signal to divert data to control comRegIn by the flash
		mux_flash         : out std_logic;
		--com reg address currently read or written
		reg_adr           : out std_logic_vector(REGISTER_ADR_WIDTH - 1 downto 0);
		--low level flash memory signals routed to the top level ports
		Flash_AT25_SO     : in  std_logic;
		Flash_AT25_inputs : out AT_flash_outputs;
		debug             : out std_logic_vector(7 downto 0)
	);
end entity FlashConfig;

architecture RTL of FlashConfig is

	type state_type is (IDLE,
	                    STARTUP,        --first state after reset
	                    SU_READ_BYTE_ZERO, --read first byte of flash array
	                    SU_FLASH_READ,
	                    SU_WAIT_FLASH_READ,
	                    SU_EVAL,
	                    WRITE_FLASH,
	                    WF_UNLOCK,
	                    WF_PREP_WAIT_UNLOCK,
	                    WF_WAIT_UNLOCK,
	                    WF_PREPARE,
	                    WF_ERASE,
	                    WF_WAIT_ERASE,
	                    WF_PREP_WRITE,
	                    WF_CHECK_WRITE,
	                    WF_DO_WRITE,
	                    WF_WAIT_WRITE,
	                    WS_UNLOCK,
	                    WS_PREP_WAIT_UNLOCK,
	                    WS_WAIT_UNLOCK,
	                    WS_PREPARE,
	                    WS_WRITE,
	                    WS_WAIT_WRITE,
	                    READ_FLASH,
	                    RF_CALC_F_ADR,
	                    RF_CHECK_FLASH_READ,
	                    RF_FLASH_READ,
	                    RF_WAIT_FLASH_READ,
	                    RF_INCREMENT,
	                    WRITE_STARTUP
	                   );
	signal state : state_type;

	signal flash_inputs         : flash_inputs;
	signal flash_outputs        : flash_outputs;
	signal reg_adr_count        : integer range 0 to 255;
	--real address of the byte in the flash memory
	signal flash_address        : std_logic_vector(23 downto 0);
	signal byte_zero            : std_logic_vector(7 downto 0);
	signal flash_offset         : std_logic_vector(23 downto 0);
	signal watchdog_timer		: integer range 0 to integer'high;
	signal slv_reg_adr_count	: std_logic_vector(23 downto 0);

	constant flash_block_length : std_logic_vector(23 downto 0) := x"001000";

begin
	slv_reg_adr_count <= x"0000" & std_logic_vector(to_unsigned(reg_adr_count, 8));

	--instantiate the flash wrapper which talks to the low level flash interface which does
	--the low level communication with the flash memory via SPI

	main_fsm : process(clk, rst) is
	begin
		if rising_edge(clk) then
			if rst = '1' then
				state                    <= STARTUP;
				reg_adr_count            <= 0;
				flash_address            <= (others => '0');
				byte_zero                <= (others => '0');
				flash_inputs.cmd         <= (others => '0');
				flash_inputs.address     <= (others => '0');
				flash_inputs.wdata       <= (others => '0');
				flash_inputs.transparent <= '0';
				reg_adr                  <= (others => '0');
				reg_WEN                  <= (others => '0');
				mux_flash                <= '0';
				watchdog_timer           <= 0;
			else

				case state is
					when IDLE =>
						case cmd is
							when "0000" =>
								--idle command
								mux_flash <= '0';
								state     <= IDLE;
							when "0001" => --write to flash
								mux_flash    <= '1';
								flash_offset <= x"00" & flash_block(3 downto 0) & x"000";
								state        <= WRITE_FLASH;
							when "0010" => --read from flash
								mux_flash    <= '1';
								flash_offset <= x"00" & flash_block(3 downto 0) & x"000";
								state        <= READ_FLASH;
							when "0011" => --write startup config to flash
								mux_flash <= '1';
								state     <= WRITE_STARTUP;
							when others =>
								state <= IDLE;
						end case;
						--set register file write enable to 0 if nothing is done
						reg_WEN <= (others => '0');
						--set flash mux to com interface if no read/write action is performed

						flash_address <= (others => '0');
						reg_adr_count <= 0;
					--==========================STARTUP============================================
					when STARTUP =>
						flash_inputs.address <= x"000000"; --address zero
						flash_inputs.cmd     <= "0001"; --read one byte
						state                <= SU_FLASH_READ;

					when SU_FLASH_READ =>
						flash_inputs.cmd <= "0000";
						state            <= SU_WAIT_FLASH_READ;

					when SU_WAIT_FLASH_READ =>
						if flash_outputs.busy = '0' then

							--put flash data to byte zero register and evaluate
							byte_zero <= flash_outputs.rdata;

							state <= SU_EVAL;
						else
							state <= SU_WAIT_FLASH_READ;
						end if;
					when SU_EVAL =>
						case byte_zero is

							when x"01" =>
								--valid flash block, set flash address offset accordingly
								flash_offset <= x"00" & byte_zero(3 downto 0) & x"000";
								state        <= READ_FLASH;
							when x"02" =>
								--valid flash block, set flash address offset accordingly
								flash_offset <= x"00" & byte_zero(3 downto 0) & x"000";
								state        <= READ_FLASH;
							when x"03" =>
								--valid flash block, set flash address offset accordingly
								flash_offset <= x"00" & byte_zero(3 downto 0) & x"000";
								state        <= READ_FLASH;
							when x"04" =>
								--valid flash block, set flash address offset accordingly
								flash_offset <= x"00" & byte_zero(3 downto 0) & x"000";
								state        <= READ_FLASH;
							when x"05" =>
								--valid flash block, set flash address offset accordingly
								flash_offset <= x"00" & byte_zero(3 downto 0) & x"000";
								state        <= READ_FLASH;
							when x"06" =>
								--valid flash block, set flash address offset accordingly
								flash_offset <= ux"00" & byte_zero(3 downto 0) & x"000";
								state        <= READ_FLASH;
							when x"07" =>
								--valid flash block, set flash address offset accordingly
								flash_offset <= x"00" & byte_zero(3 downto 0) & x"000";
								state        <= READ_FLASH;

							when others =>
								--invalid startup codes, set standard block address and go to idle 
								flash_offset <= flash_block_length;
								state        <= IDLE;
						end case;

					--==========================WRITE===============================================
					when WRITE_FLASH =>
						--write current register file content to flash block specified
						state <= WF_UNLOCK;
					when WF_UNLOCK =>
						--uses wrapper function to write config register
						flash_inputs.transparent <= '0';
						flash_inputs.wdata       <= x"00"; --write 00 to config register for global
						flash_inputs.cmd         <= "1010"; -- unprotect
						state                    <= WF_PREP_WAIT_UNLOCK;
					when WF_PREP_WAIT_UNLOCK =>
						flash_inputs.cmd <= "0000";
						state            <= WF_WAIT_UNLOCK;
					when WF_WAIT_UNLOCK =>
						if flash_outputs.busy = '0' then
							state <= WF_PREPARE;
						else
							state <= WF_WAIT_UNLOCK;
						end if;

					when WF_PREPARE =>
						flash_inputs.cmd         <= "0011"; --erase flash
						flash_inputs.address     <= flash_offset;
						flash_inputs.transparent <= '0';
						state                    <= WF_ERASE;

					when WF_ERASE =>
						flash_inputs.cmd <= "0000";
						state            <= WF_WAIT_ERASE;

					when WF_WAIT_ERASE =>
						if flash_outputs.busy = '0' then
							state <= WF_PREP_WRITE;
						else
							state <= WF_WAIT_ERASE;
						end if;
					when WF_PREP_WRITE =>
						--set register address to be read from
						reg_adr       <= std_logic_vector(to_unsigned(reg_adr_count, 8));
						--set address of flash memory
						--flash_address <= x"0000" & std_logic_vector(to_unsigned(reg_adr_count, 8));
						--calculate flash address from register address (multiply by 4 realized by a double left shift)
						flash_address <= std_logic_vector(unsigned(flash_offset) + shift_left(unsigned(slv_reg_adr_count), 2));
						state         <= WF_CHECK_WRITE;
					when WF_CHECK_WRITE =>
						if reg_adr_count < (2**REGISTER_ADR_WIDTH) then
							--set flash write data, address and execute write command
							flash_inputs.address <= flash_address;
							flash_inputs.wdata   <= flash_data_w(7 downto 0); --for the test just write one byte, the lowest
							flash_inputs.wdata2  <= flash_data_w(15 downto 8); --for the test just write one byte, the lowest
							flash_inputs.wdata3  <= flash_data_w(23 downto 16); --for the test just write one byte, the lowest
							flash_inputs.wdata4  <= flash_data_w(31 downto 24); --for the test just write one byte, the lowest
							flash_inputs.cmd     <= "1111"; --write command
							state                <= WF_DO_WRITE;
						else
							state <= IDLE;
						end if;
					when WF_DO_WRITE =>
						flash_inputs.cmd <= "0000";
						state            <= WF_WAIT_WRITE;
					when WF_WAIT_WRITE =>
						if flash_outputs.busy = '0' then
							reg_adr_count <= reg_adr_count + 1;
							state         <= WF_PREP_WRITE;
						else
							state <= WF_WAIT_WRITE;
						end if;
					--===================================WRITE STARTUP========================================================
					when WRITE_STARTUP =>
						state <= WS_UNLOCK;
					when WS_UNLOCK =>
						--uses wrapper function to write config register
						flash_inputs.transparent <= '0';
						flash_inputs.wdata       <= x"00"; --write 00 to config register for global
						flash_inputs.cmd         <= "1010"; -- unprotect
						state                    <= WS_PREP_WAIT_UNLOCK;
					when WS_PREP_WAIT_UNLOCK =>
						flash_inputs.cmd <= "0000";
						state            <= WS_WAIT_UNLOCK;
					when WS_WAIT_UNLOCK =>
						if flash_outputs.busy = '0' then
							state <= WS_PREPARE;
						else
							state <= WS_WAIT_UNLOCK;
						end if;

					when WS_PREPARE =>
						flash_inputs.cmd         <= "0100"; --erase flash & write one byte
						flash_inputs.address     <= (others => '0'); --address zero
						flash_inputs.wdata       <= "0000" & flash_block; --save startup block after next reset
						flash_inputs.transparent <= '0';
						state                    <= WS_WRITE;
					when WS_WRITE =>
						flash_inputs.cmd <= "0000";
						state            <= WS_WAIT_WRITE;

					when WS_WAIT_WRITE =>
						if flash_outputs.busy = '0' then
							state <= IDLE;
						else
							state <= WS_WAIT_WRITE;
						end if;

					--==================================READ branch===========================================================	
					when READ_FLASH =>
						mux_flash     <= '1';
						reg_adr_count <= 0;
						state         <= RF_CHECK_FLASH_READ;
					when RF_CALC_F_ADR =>
						flash_address <= std_logic_vector(unsigned(flash_offset) + shift_left(unsigned(slv_reg_adr_count), 2));
						state         <= RF_CHECK_FLASH_READ;
					when RF_CHECK_FLASH_READ =>
						if reg_adr_count < (2**REGISTER_ADR_WIDTH) then
							flash_inputs.address <= flash_address;
							flash_inputs.cmd     <= "1100"; --read four bytes 
							state                <= RF_FLASH_READ;
						else
							state <= IDLE;
						end if;

					when RF_FLASH_READ =>
						flash_inputs.cmd <= "0000";
						state            <= RF_WAIT_FLASH_READ;

					when RF_WAIT_FLASH_READ =>
						if flash_outputs.busy = '0' then

							--put flash data to register output and write enable
							flash_data_r           <= flash_outputs.rdata4;
							reg_WEN(reg_adr_count) <= '1';
							state                  <= RF_INCREMENT;
						else
							state <= RF_WAIT_FLASH_READ;
						end if;

					when RF_INCREMENT =>
						reg_adr_count          <= reg_adr_count + 1;
						--set register write enable back to zero
						reg_WEN(reg_adr_count) <= '0';
						state                  <= RF_CALC_F_ADR;

					when others =>
						state <= IDLE;

				end case;

				--watchdog counter in case flash does not respond
				if (state /= IDLE) then
					watchdog_timer <= watchdog_timer + 1;
					if (watchdog_timer > 80000000) then -- 40MHZ clock, 2 seconds
						state <= IDLE;
					end if;
				else
					watchdog_timer <= 0;
				end if;
			end if;
		end if;
	end process main_fsm;

	fla : flash_wrapper
		port map(
			clk           => clk, rst => rst,
			pso           => Flash_AT25_SO,
			pAT_flash_out => Flash_AT25_inputs,
			pinputs       => flash_inputs,
			poutputs      => flash_outputs
		);

end architecture RTL;
