library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

LIBRARY UQDSLib;
use UQDSLib.UQDSLib.ALL;

---------------------------------------------------------------------------------------------------------
--! Company: CERN
--
--! @File: detection_block_mag_twolead.vhd
-- File history:
--		unknown
--		v0.1	03/10/2017	first version for uQDS
 
--!@brief 				Implements one standard quench detection logic (one virtual bridge & two current leads) 
-- Description: 	
--			
--	logic_out(7) 	--> logic lead A sync quench
--	logic_out(6) 	--> logic lead A latch quench
--	logic_out(5) 	--> logic lead B sync quench
--	logic_out(4) 	--> logic lead B latch quench
--	logic_out(3) 	--> logic magnet sync quench
--	logic_out(2) 	--> logic magnet latch quench
--	logic_out(1)   	--> '0' RESERVED
--	logic_out(0)	--> leadA quench or leadB quench or magnet quench
--
-- Targeted device: Actel ProAsic3 A3PE1500 PQ208
--! @Author: Jens Steckert (CERN), Andrzej Skoczen (AGH-UST)
--
----------------------------------------------------------------------------------------------------------

entity detection_block_mag_twolead is
	generic(d_width : integer range 15 to 33 := 32);
	port(
		clk : in std_logic;
		rst : in std_logic;
		
		magA 			: in std_logic_vector(d_width-1 downto 0);
		magB 			: in std_logic_vector(d_width-1 downto 0);
		leadA 			: in std_logic_vector(d_width-1 downto 0);
		leadB 			: in std_logic_vector(d_width-1 downto 0);
		thres_mag		: in std_logic_vector(d_width-1 downto 0);
		disc_mag		: in std_logic_vector(15 downto 0);
		thres_leads		: in std_logic_vector(d_width-1 downto 0);
		disc_leads		: in std_logic_vector(15 downto 0);
		magABsum		: out std_logic_vector(d_width-1 downto 0);
		logic_out		: out std_logic_vector(7 downto 0)
		
	);
end entity detection_block_mag_twolead;

architecture RTL of detection_block_mag_twolead is
	
	
	
component qlogic_comp_simple is
	generic(d_width : integer range 15 to 33 := 32);
	port (
	    clk : 		in std_logic;
	    rst : 		in std_logic;
	    U_in : 		in std_logic_vector(d_width-1 downto 0);
		T_in : 		in std_logic_vector(d_width -1 downto 0);
		disc	: 	in std_logic_vector(15 downto 0);
		L_out : 	out std_logic_vector(3 downto 0)
	);
end component;

component qlogic_bridge is
	generic(d_width : natural := 32);
	port(
		clk 		: in std_logic;
		rst 		: in std_logic;
		qthreshold	: in std_logic_vector(d_width-1 downto 0);
		time_disc	: in std_logic_vector(15 downto 0);
		inA			: in std_logic_vector(d_width-1 downto 0);
		inB			: in std_logic_vector(d_width-1 downto 0);
		outAplusB 	: out std_logic_vector(d_width-1 downto 0);
		log_out 	: out std_logic_vector(3 downto 0)
	);
end component;
	
	
	
signal logic_leadA	: std_logic_vector(3 downto 0);
signal logic_leadB : std_logic_vector(3 downto 0);
signal logic_magnet : std_logic_vector(3 downto 0);	

	
begin


--detection leadA

leadA_detection: qlogic_comp_simple
	generic map(d_width =>d_width)
	port map(clk => clk, rst => rst,
			U_in => leadA,
			T_in => thres_leads,
			disc => disc_leads,
			L_out => logic_leadA
	);

--detection magnet
magnet_detection: qlogic_bridge
	generic map(d_width => d_width)
	port map(clk => clk, rst=> rst,
			qthreshold => thres_mag,
			time_disc => disc_mag,
			inA => magA,
			inB => magB,
			outAplusB => magABsum,
			log_out => logic_magnet
	);

--detection leadB
leadB_detection: qlogic_comp_simple
	generic map(d_width =>d_width)
	port map(clk => clk, rst => rst,
			U_in => leadB,
			T_in => thres_leads,
			disc => disc_leads,
			L_out => logic_leadB
	);	

--combine the outputs of the logic modules logic_out(0) is leadA trigger or leadB trigger or magnet trigger
logic_out <= logic_leadA(1 downto 0) & logic_leadB(1 downto 0) &  logic_magnet(1 downto 0) & 
				(logic_leadA(1) or logic_leadB(1) or logic_magnet(1)) & (logic_leadA(0) or logic_leadB(0) or logic_magnet(0)); 







end architecture RTL;
