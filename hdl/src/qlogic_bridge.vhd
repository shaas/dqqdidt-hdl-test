library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

LIBRARY UQDSLib;
use UQDSLib.UQDSLib.ALL;

--------------------------------------------------------------------------------
-- Company: CERN
--
-- File: qlogic_bridge.vhd
-- File history:
--      0.1: 13.04.2018: first attempt, untested
--		
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- implements an bridge like quench detection logic
--	input A + input B <= threshold
--	inputs assumed to be 2s complement signed integers
--
-- ToDo:
--		test !
--
-- Targeted device: IGLOO2
-- Author: Jens Steckert
--
--------------------------------------------------------------------------------
entity qlogic_bridge is
generic(d_width : natural := 32);
	port(
		clk 		: in std_logic;
		rst 		: in std_logic;
		qthreshold	: in std_logic_vector(d_width-1 downto 0);
		time_disc	: in std_logic_vector(15 downto 0);
		inA			: in std_logic_vector(d_width-1 downto 0);
		inB			: in std_logic_vector(d_width-1 downto 0);
		outAplusB 	: out std_logic_vector(d_width-1 downto 0);
		log_out 	: out std_logic_vector(3 downto 0)
	);
end entity qlogic_bridge;

architecture RTL of qlogic_bridge is
	
	
component qlogic_comp_simple is
	generic(d_width : integer range 15 to 33 := 32);
	port (
	    clk : 		in std_logic;
	    rst : 		in std_logic;
	    U_in : 		in std_logic_vector(d_width-1 downto 0);
		T_in : 		in std_logic_vector(d_width -1 downto 0);
		disc	: 	in std_logic_vector(15 downto 0);
		L_out : 	out std_logic_vector(3 downto 0)
	);
end component;	
	
	
	
signal sumAB 		: signed(d_width downto 0);  --one bit longer
signal sumAB_clip	: std_logic_vector(d_width -1 downto 0); --standard length, clipped

constant pos_clip : std_logic_vector(d_width -2 downto 0) := (others =>'1');
constant neg_clip : std_logic_vector(d_width -2 downto 0) := (others =>'0');
	
begin


--add U1 and U2 to create UQS1 this is perfomed on base of the decimated and filtered data
--done in a process to relax timing
adder: process(clk)
begin
if rising_edge(clk) then
	if rst = '1' then
		sumAB <= (others => '0');
	else
		--sign-extend U1, U2 and add them 
		sumAB <= signed( inA(inA'left)  & std_logic_vector(inA)) + signed( inB(inB'left) & std_logic_vector(inB));
	end if;
end if;
end process;

--now clip result of addition (to avoid wrap-arounds)
with to_bitvector(std_logic_vector((sumAB((sumAB'length -1) downto (sumAB'length -2)))),'0') select 
				--positive, non-clipping number, just take value
	sumAB_clip <=	std_logic_vector(sumAB(sumAB'length -2 downto 0))		when "00",
    			--positive, larger than x7fff, clip to pos full scale
					"0" & pos_clip 												when "01",
    			--negative, larger than x8000, clip to neg full scale
					"1" & neg_clip												when "10",
				--negative, non-clipping number, just take value	
					std_logic_vector(sumAB(sumAB'length -2 downto 0))	 	when "11";


--put sum to output port
outAplusB <= sumAB_clip;

--=======================================QD logic============================================================

--quench logic for sum
quench_logic : qlogic_comp_simple 
	generic map(d_width => d_width)
	port map (clk => clk, rst => rst,
		U_in => sumAB_clip, 
		T_in => qthreshold, 
		disc => time_disc,
		L_out => log_out);

--===========================================================================================================


end architecture RTL;
