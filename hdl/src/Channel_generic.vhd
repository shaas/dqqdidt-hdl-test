---------------------------------------------------------------------------------------------------------
-- Company: CERN
--
-- File: channel_generic.vhd
-- File history:
--      Created:	11.01.17		: generic signal processing pipeline 
--		 	
--      1.1 27.01.2017 modified by Josef Kopal for Altera carrier (fix for ufixed type)
--      1.2 23-9-2017 modified by Daniel Blasco: added some data_ready flags and added autozero to output
--		1.3 09-07-2019: Used fixed types during arithmetic operations incl. resisze 
-- Description: 
-- 				combines mux structure and functional blocks of the usual signal processing pipeline
-- Function: 	
--				Covers the functionality of the usual pipeline in one entity. This should help structuring the code 
--				in a better way. 
--
-- Targeted device: Microsemi Igloo2 (doesn't really matter, generic VHDL)
-- Author: Jens Steckert 
--
----------------------------------------------------------------------------------------------------------

library ieee;

use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;
use ieee.fixed_pkg.all;
use IEEE.fixed_float_types.all;

LIBRARY UQDSLib;
use UQDSLib.UQDSLib.ALL;
use UQDSLib.config_block_lib.all;
use work.util.all;

entity channel_generic is
	generic(
		generate_median	   	: boolean := TRUE;
		d_width        		: integer range 3 to 32   := 16; --data width
		b_width        		: integer range 3 to 32   := 16; --buffer data width
		mavg_ram_depth 		: integer range 3 to 1024 := 1024; --moving average ram depth (max)
		cor_c_width    		: integer range 8 to 24   := 16 --corrector correction factor width
	);
	port(clk             : in  std_logic;
	     rst             : in  std_logic;
	     sampleclock     : in  std_logic;
	     Data_in         : in  std_logic_vector(d_width - 1 downto 0);
	     autozero_start  : in  std_logic;
	     --Multiplexer
	     mux_data        : in  std_logic_vector(7 downto 0);
	     mux_daq         : in  std_logic_vector(2 downto 0);
	     --functional block parameters
	     mavg_depth 	: in  integer range 0 to mavg_ram_depth-1;
	     real_invdiv     : in  std_logic_vector(d_width - 1 downto 0);
	     median_depth    : in  integer range 0 to 126 := 2;
	     median_middle   : in  integer range 0 to 62  := 1;
	     cor_gain        : in  std_logic_vector(cor_c_width - 1 downto 0);
	     cor_offset      : in  std_logic_vector(d_width - 1 downto 0);
	     --outputs
	     cor_offset_auto : out std_logic_vector(d_width - 1 downto 0);
	     Dmux_data_out   : out std_logic_vector(d_width - 1 downto 0);
	     d_ready_out     : out std_logic;
	     Bmux_data_out   : out std_logic_vector(b_width - 1 downto 0);
	     buf_ready_out	 : out std_logic;
	     debug           : out std_logic_vector(1 downto 0));
end channel_generic;

architecture channel_generic_arch of channel_generic is

--	signal d_adc_out      : std_logic_vector(d_width - 1 downto 0);
	signal d_inv_out      : std_logic_vector(d_width - 1 downto 0);
	signal d_f_median_out : std_logic_vector(d_width - 1 downto 0);
--	signal d_dec0002_out  : std_logic_vector(d_width - 1 downto 0);
	signal d_dec0004_out  : std_logic_vector(d_width - 1 downto 0);
	signal d_dec0008_out  : std_logic_vector(d_width - 1 downto 0);
	signal d_dec0016_out  : std_logic_vector(d_width - 1 downto 0);
	signal d_dec0032_out  : std_logic_vector(d_width - 1 downto 0);
	signal d_dec0064_out  : std_logic_vector(d_width - 1 downto 0);
	signal d_dec0256_out  : std_logic_vector(d_width - 1 downto 0);
	signal d_f0_out       : std_logic_vector(d_width - 1 downto 0);
	signal d_cor_out      : std_logic_vector(d_width - 1 downto 0);
	signal d_inv_mux      : std_logic_vector(d_width - 1 downto 0);
	signal d_dec_mux      : std_logic_vector(d_width - 1 downto 0);
	signal d_f_median_mux : std_logic_vector(d_width - 1 downto 0);
	signal d_f0_mux       : std_logic_vector(d_width - 1 downto 0);
	signal d_cor_mux      : std_logic_vector(d_width - 1 downto 0);

	signal daq_mux : std_logic_vector(d_width - 1 downto 0);

	signal d_dready_adc        : std_logic;
	signal d_dready_dec0004    : std_logic;
	signal d_dready_dec0008    : std_logic;
	signal d_dready_dec0016    : std_logic;
	signal d_dready_dec0032    : std_logic;
	signal d_dready_dec0064    : std_logic;
	signal d_dready_dec0256    : std_logic;
	signal d_dready_mux        : std_logic;
	signal d_dready_median_in  : std_logic;
	signal d_dready_median_out : std_logic;
	signal d_dready_mean_in    : std_logic;
	signal d_dready_mean_out   : std_logic;
	signal buf_ready		   : std_logic;	
	
	signal d_offset      : std_logic_vector(d_width - 1 downto 0);
	signal d_offset_man  : std_logic_vector(d_width - 1 downto 0);
	signal d_offset_auto : std_logic_vector(d_width - 1 downto 0);

	signal autozero_done : std_logic := '0';
	signal standard_fxp : sf_voltage;
	signal sf_d_cor_mux : sf_voltage;
	signal sf_d_offset_auto : sf_voltage;
	signal fsp, fsn : std_logic_vector(d_width -1 downto 0);
	
begin
	debug <= "00";

	fsp <= (fsp'left =>'0',others=>'1');
	fsn <= (fsn'left =>'1',others=>'0');
	
	multiplexer_data : process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				d_inv_mux          <= (others => '0');
				d_dec_mux          <= (others => '0');
				d_f0_mux           <= (others => '0');
				d_cor_mux          <= (others => '0');
				d_dready_mux       <= '0';
				d_dready_median_in <= '0';
				--d_dready_median_out <= '0';
				d_dready_mean_in   <= '0';
			--d_dready_mean_out <= '0';
			--autozero_done      <= '0';
			--d_offset_auto      <= (others => '0');
			else
				case mux_data(0) is
					when '0' =>
						d_inv_mux <= Data_in;
					when '1' =>
						d_inv_mux <= d_inv_out;
					when others =>
				end case;
				case mux_data(3 downto 1) is
					when "000" =>
						d_dec_mux          <= d_inv_mux;
						d_dready_median_in <= d_dready_adc;
					when "001" =>
						d_dec_mux          <= d_dec0004_out;
						d_dready_median_in <= d_dready_dec0004;
					when "010" =>
						d_dec_mux          <= d_dec0008_out;
						d_dready_median_in <= d_dready_dec0008;
					when "011" =>
						d_dec_mux          <= d_dec0016_out;
						d_dready_median_in <= d_dready_dec0016;
					when "100" =>
						d_dec_mux          <= d_dec0032_out;
						d_dready_median_in <= d_dready_dec0032;
					when "101" =>
						d_dec_mux          <= d_dec0064_out;
						d_dready_median_in <= d_dready_dec0064;
					when "110" =>
						d_dec_mux          <= d_dec0256_out;
						d_dready_median_in <= d_dready_dec0256;
					when others =>
						d_dec_mux          <= d_inv_mux;
						d_dready_median_in <= d_dready_adc;
				end case;
				case mux_data(4) is		--Median Filter
					when '0' =>
						d_f_median_mux   <= d_dec_mux;
						d_dready_mean_in <= d_dready_median_in;
					when '1' =>
						d_f_median_mux   <= d_f_median_out;
						d_dready_mean_in <= d_dready_median_out;
					when others =>
				end case;
				case mux_data(5) is		--Moving Average filter
					when '0' =>
						d_f0_mux     <= d_f_median_mux;
						d_dready_mux <= d_dready_mean_in;
					when '1' =>
						d_f0_mux     <= d_f0_out;
						d_dready_mux <= d_dready_mean_out;
					when others =>
				end case;
				case mux_data(6) is		--Corrector (gain/offset manual setting)
					when '0' =>
						d_cor_mux <= d_f0_mux;
					when '1' =>
						d_cor_mux <= d_cor_out;
					when others =>
				end case;

				case mux_data(7) is		--Offset compensation (automatic)
					when '0' =>
						d_offset <= d_cor_mux;
					when '1' =>
						d_offset <= d_offset_man;
					when others =>
				end case;
			end if;
		end if;
	end process;

	daq_multiplexer : process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				daq_mux <= (others => '0');
			else
				case mux_daq(2 downto 0) is
					when "000" =>	--raw data
						daq_mux <= Data_in;
						buf_ready <= d_dready_adc;
					when "001" =>	--inverter
						daq_mux <= d_inv_mux;
						buf_ready <= d_dready_adc;
					when "010" =>	--decimator
						daq_mux <= d_dec_mux;
						buf_ready <= d_dready_median_in;
					when "011" =>	--median filter
						daq_mux <= d_f_median_mux;
						buf_ready <= d_dready_mean_in;	
					when "100" =>	--Moving average filter
						daq_mux <= d_f0_mux;
						buf_ready <= d_dready_mux;
					when "101" =>	--corrector
						daq_mux <= d_cor_mux;
						buf_ready <= d_dready_mux;
					when "110" =>	--auto offset
						daq_mux <= d_offset;
						buf_ready <= d_dready_mux;
					when others =>
						daq_mux <= d_offset;
						buf_ready <= d_dready_mux;
				end case;
			end if;
		end if;
	end process;

	--create outputs
	cor_offset_auto <= d_offset_auto;
	Dmux_data_out   <= d_offset;
	Bmux_data_out   <= daq_mux;
	d_ready_out     <= d_dready_mux;
	Buf_ready_out   <= buf_ready;
	--Functional blocks

	d_dready_adc <= sampleclock;
	--d_offset_man <= cor_offset;

	--inverter
	inv : process(rst,clk)
    begin
    if rst = '1' then
        d_inv_out <= (others => '0');
    elsif rising_edge(clk) then
        if (Data_in = fsp) then
            d_inv_out <= fsn;
        elsif (Data_in = fsn) then
            d_inv_out <= fsp;
        else
            d_inv_out <= std_logic_vector(not(signed(Data_in)) + 1);
        end if;
    end if;
	end process;

	--decimation
	ddec0004 :  decimateX
		generic map(d_width => d_width, X => 4, omode => true)
		port map(clk         => clk, reset => rst,
		         dready      => d_dready_adc,
		         data_in     => d_inv_mux,
		         slow_dready => d_dready_dec0004,
		         slow_data   => d_dec0004_out
		        );
	
	ddec0008 :  decimateX
		generic map(d_width => d_width, X => 8, omode => true)
		port map(clk         => clk, reset => rst,
		         dready      => d_dready_adc,
		         data_in     => d_inv_mux,
		         slow_dready => d_dready_dec0008,
		         slow_data   => d_dec0008_out
		        );

	ddec0016 :  decimateX
		generic map(d_width => d_width, X => 16, omode => true)
		port map(clk         => clk, reset => rst,
		         dready      => d_dready_adc,
		         data_in     => d_inv_mux,
		         slow_dready => d_dready_dec0016,
		         slow_data   => d_dec0016_out
		        );

	ddec0032 :  decimateX
		generic map(d_width => d_width, X => 32, omode => true)
		port map(clk         => clk, reset => rst,
		         dready      => d_dready_adc,
		         data_in     => d_inv_mux,
		         slow_dready => d_dready_dec0032,
		         slow_data   => d_dec0032_out
		        );
		        
	ddec0064 :  decimateX
		generic map(d_width => d_width, X => 64, omode => true)
		port map(clk         => clk, reset => rst,
		         dready      => d_dready_adc,
		         data_in     => d_inv_mux,
		         slow_dready => d_dready_dec0064,
		         slow_data   => d_dec0064_out
		        );

	ddec0256 :  decimateX
		generic map(d_width => d_width, X => 256, omode => true)
		port map(clk         => clk, reset => rst,
		         dready      => d_dready_adc,
		         data_in     => d_inv_mux,
		         slow_dready => d_dready_dec0256,
		         slow_data   => d_dec0256_out
		        );

	--median filter
gen_median: if generate_median = TRUE generate
	median_d :  median_filter
		generic map(d_width => d_width, max_depth => median_max_depth) --, middle => median_middle)
		port map(
			clk         => clk, 
			rst 		=> rst, 
			sample_clock=> d_dready_median_in,
			val_in      => d_dec_mux, 
			out_valid 	=> d_dready_median_out, 
			median_out 	=> d_f_median_out, 
			depth 		=> median_depth, 
			middle 		=> median_middle);
			
end generate;
gen_no_median: if generate_median = FALSE generate
	d_f_median_out <= d_dec_mux;
	d_dready_median_out <= d_dready_median_in;
end generate;

	--moving average filter
	mean_d : meanram_fsm_fast
		generic map(d_width => d_width, ram_depth => mavg_ram_depth, realmul => false)
		port map(
			clk         	=> clk, 
			sampleclock 	=> d_dready_mean_in, 
			rst 			=> rst, 
			filter_depth 	=> mavg_depth,
		    Data_in     	=> d_f_median_mux,  
		    real_invdiv 	=> real_invdiv, 
		    Qout 			=> d_f0_out, 
		    dready_out 		=> d_dready_mean_out);

	cor_d :  corr_fixed
		generic map(d_width => d_width,
		            o_width => d_width,
		            c_width => cor_c_width,
		            outreg  => TRUE)
		port map(
			clk		=> clk, 
			rst		=> rst, 
			din		=> d_f0_mux, 
			cfactor	=> cor_gain,
	        offset  => cor_offset, 
	        dout 	=> d_cor_out);

	autozero_d :  autozero
		generic map(d_width => d_width)
		port map(
			clk		=> clk, 
			rst 	=> rst, 
			start 	=> autozero_start, 
			done 	=> autozero_done,
			din    	=> d_cor_mux, 
			dready 	=> d_dready_mux, 
			offcor 	=> d_offset_auto);
	
	--convert to sfixed
	sf_d_cor_mux <= to_sfixed(d_cor_mux, standard_fxp);
	sf_d_offset_auto <= to_sfixed(d_offset_auto, standard_fxp);
	
	autozero_offset_add : process(clk)
	begin
		if (rising_edge(clk)) then
			if rst = '1' then
				d_offset_man <= (others => '0');
			else
				--if the input signal is already saturated, just pass it without applying offset
				if (d_cor_mux = fsp OR d_cor_mux = fsn) then
					d_offset_man <= d_cor_mux;
				else
					if (autozero_done = '1') then
						--add as sfixed types and resize (takes care about overflow and rounding
						
						d_offset_man <= to_slv(resize(arg => (sf_d_cor_mux + sf_d_offset_auto), size_res => standard_fxp, 
												overflow_style => fixed_saturate, round_style => fixed_truncate
						));

					else
						d_offset_man <= d_cor_mux;
					end if;
				end if;
			end if;
		end if;
	end process;

end;