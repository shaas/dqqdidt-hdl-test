LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
use ieee.numeric_std.all;
--------------------------------------------------------------------------------
-- Company: CERN
--
-- File: autozero.vhd
-- File history:
--      0.1: 22.06.2009: first attempt
--      0.2: 21.07.2009: corrected nasty bug in shrinking 17-bit corval
--						to 8bit signed
--		0.3: 09/20/2010: adapted to nIPQ, fixed correction to -1LSB bug
--		0.4: 10/25/2011: 24-bit wide version for nDG
--      0.5: 01/13/2011: 25 bit wide version for nDG (one channel only)
--		0.6: 02/01/2012: increased points for autozero calculation
--		0.7: 05/30/2013: reduced points for autozero calc --> slower ADC
--		0.8: 06/26/2013: variable input data width
--		0.9: 14/02/2018: extended to 32 bits width

--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- multiplies the incoming value with a correction factor 
--
-- ToDo:
--		test !
--
-- Targeted device: ProAsic3 A3PE1500 PQ208
-- Author: Jens Steckert
--
--------------------------------------------------------------------------------

entity autozero is
	generic(d_width : integer range 15 to 32);
	port(
		clk    : in  std_logic;
		rst    : in  std_logic;
		start  : in  std_logic;
		done   : out std_logic;
		din    : in  std_logic_vector(d_width - 1 downto 0);
		dready : in  std_logic;
		offcor : out std_logic_vector(d_width - 1 downto 0)
	);
end autozero;

--! @brief Autozero will calculate offset correction values for the two incoming channels
--! @details takes 4k samples and calculates the mean value. From this the zero level is subtracted

architecture autozero_arch of autozero is

--	signal corout : signed(15 downto 0);

--	signal dreadyold   : std_logic_vector(1 downto 0);
	signal cmdold      : std_logic;
--	signal dreadypulse : std_logic_vector(1 downto 0);

	--type state_type is (idle, prepare, mux, sample, calcmean, calcoffset, increment, convertoffset );
	type state_type is (idle, prepare, sample, calcmean, calcoffset, convertoffset);

	signal state            : state_type;
	signal sum              : signed(d_width + 11 downto 0);
	signal mean             : signed(d_width - 1 downto 0);
	signal corval           : signed(d_width + 3 downto 0);
	signal sampcount        : integer range 0 to 4096;--65535;
--	signal divshift         : integer range 2 to 12;
	signal startpulse       : std_logic;
	signal chcount          : integer range 0 to 1;
--	signal dreadyi          : std_logic_vector(1 downto 0);
	signal clip_result      : signed(d_width - 1 downto 0);
	signal sample_pulse     : std_logic;
	signal sample_clock_old : std_logic;
	signal sample_clock     : std_logic;

begin

	sample_clock <= dready;

	startpulser : process(clk)
	begin
		if (clk'event and clk = '1') then
			if rst = '1' then
				startpulse <= '0';
				cmdold     <= '0';
			else
				cmdold <= start;
				if start > cmdold then
					startpulse <= '1';
				else
					startpulse <= '0';
				end if;
			end if;
		end if;
	end process;

	pulser : process(clk)
	begin
		if rising_edge(clk) then
			if rst = '1' then
				sample_pulse     <= '0';
				sample_clock_old <= '0';
			else
				sample_clock_old <= sample_clock;
				if sample_clock_old < sample_clock then
					--rising edge
					sample_pulse <= '1';
				else
					sample_pulse <= '0';
				end if;
			end if;
		end if;
	end process;

	fsm : process(clk)
	begin
		if (clk'event and clk = '1') then
			if rst = '1' then
				--blah
				chcount     <= 0;
				sampcount   <= 0;
				sum         <= (others => '0');
				mean        <= (others => '0');
				corval      <= (others => '0');
				clip_result <= (others => '0');
				state       <= idle;
				done        <= '0';
			else
				case state is
					when idle =>
						if startpulse = '1' then
							state <= prepare;
						else
							state <= idle;
						end if;

					when prepare =>
						--sampcount <= 65535;
						sampcount <= 4096; --better for slow sampling signal
						done      <= '0';
						sum       <= (others => '0');
						state     <= sample;

					when sample =>
						if sample_pulse = '1' then
							if sampcount > 0 then
								sum       <= sum + signed(din);
								sampcount <= sampcount - 1;
								state     <= sample;
							else
								--sum <= sum + din; --	last sample
								state <= calcmean;
							end if;
						else
							state <= sample;
						end if;

					when calcmean =>
						--mean(24 downto 0) <= signed(sum(40 downto 16));
						--for 4096 values
						--mean(24 downto 0) <= signed(sum(36 downto 12));
						mean(d_width - 1 downto 0) <= signed(sum(d_width + 11 downto 12));
						state                      <= calcoffset;
					when calcoffset =>
						--calculate correction value which has to be added to achieve zero
						corval <= 0 - resize(mean, corval'length);
						state  <= convertoffset;

					when convertoffset =>
						if corval(d_width + 3) = '1' then --negative number
							--if corval(23 downto 15) = "111111111" then --within negative range
							if corval(d_width + 2 downto d_width - 1) = "111111111" then --within negative range
								clip_result <= corval(d_width - 1 downto 0);
							else        --result is out of range, clip at negative full scale
								clip_result <= x"80000000";
							end if;
						else            --number is positive
							if corval(d_width + 2 downto d_width - 1) = "000000000" then --within positive range
								clip_result <= corval(d_width - 1 downto 0);
							else
								clip_result <= x"7fffffff"; --exceeds max pos range, clip !
							end if;
						end if;
						done  <= '1';
						state <= idle;
				end case;
			end if;
		end if;
	end process;

	offcor <= std_logic_vector(clip_result);

end autozero_arch;

