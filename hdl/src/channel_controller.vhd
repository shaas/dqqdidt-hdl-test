--------------------------------------------------------------------------------
-- Company: CERN
--
-- File: channel_controller.vhd
-- File history:
--      1.0: ??: <Comments>
--		1.1: 07/05/2019: Added slow clock input, FSM runs on fast clock, checked, works -Jens
-- Description: 
--
-- channel controller - receives PGA settings and test signal through parallel port, 
-- and sends them to a shift register through serial port
--
-- Targeted device: <Family::IGLOO2> <Die::M2GL150> <Package::1152 FBGA>
-- Author: Jelena Spasic
--
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library UQDSlib;
use UQDSlib.UQDSLib.ALL;

entity channel_controller is
port (
	clk			: in std_logic;
	slowclock	: in std_logic;
	reset		: in std_logic;
	data_in		: in std_logic_vector(7 downto 0);
	enable		: in std_logic;
	data_out	: out std_logic;
	clk_out		: out std_logic;
	strobe		: out std_logic
);
end channel_controller;

architecture arch of channel_controller is
   
	type states is (SENDING, START, IDLE, DONE);

	signal current_state	: states;
	signal next_state		: states;
	signal data_buffer		: std_logic_vector(7 downto 0);
	signal bit_cnt			: integer range 0 to 7;
	signal bit_cnt_load		: std_logic;
	signal busy				: std_logic;
	signal slowpulse		: std_logic; 
	signal slowclock_reg, slowclock_reg1		: std_logic;
begin

	Slowpulsegen: process(clk)
	begin
		if rising_edge(clk) then
			if reset = '1' then
				slowpulse <= '0';
				slowclock_reg <= '0';
			else
				--creates one clock wide pulse at pos edge of slow clock
				--FSM will only change outputs at slowpulse = '1'
				if slowclock > slowclock_reg then
					slowpulse <= '1';
				else
					slowpulse <= '0';
				end if;
				slowclock_reg <= slowclock;
			end if;
		end if;
	end process;
	



	FSM_change_state: process(clk)
	begin
		if rising_edge(clk) then
			if reset='1' then
				current_state	<= IDLE;
			else
				if slowpulse = '1' then
					current_state	<= next_state;
				else
					current_state <= current_state;
				end if;
			end if;
		end if;
	end process;	
	
	prepare_data: process (clk)
	begin
		if rising_edge(clk) then
			if reset='1' then
				data_buffer <= (others => '0');
			else
				if current_state=START then
					data_buffer <= data_in;
				end if;
			end if;
		end if;
	end process;
	
	
	main_FSM:   process(all)
	begin
		case current_state is			
			when IDLE=>	
				if enable='1' then 
					next_state <= START;
				else 
					next_state<=IDLE;
				end if;

			when START=>	 
				next_state <= SENDING;
				
			when SENDING =>
				if bit_cnt=0 then		
					next_state <= DONE;
				else
					next_state <= SENDING;
				end if;
	
			when DONE => 
				next_state <= IDLE;
		end case;
	end process;

	FSM_signals: process(all)
	begin 
		case current_state is
			when IDLE=>
				bit_cnt_load <='0';
				data_out <= '0';
				--clk_out <='0';	
				strobe <= '0';
				busy <= '0';

			when START=>
				bit_cnt_load <='1';
				data_out <= '0';
				--clk_out <='0';
				strobe <= '0';
				busy <= '0';
				
			when SENDING=>		
				bit_cnt_load <='0';
				data_out <= data_buffer(bit_cnt);
				--clk_out <= clk;
				strobe <= '0';
				busy <= '1';

			when DONE=>	
				bit_cnt_load <='0';
				data_out <= '0';
				--clk_out <='0';
				--strobe <= '1';
				strobe <= not(clk);
				busy <= '0';		
		end case;
	end process;
	
	bit_counter: process(clk)
	begin
		if rising_edge(clk) then
			if reset='1' then
				bit_cnt <= 7;	
			else 
				if bit_cnt_load='1' then 
					bit_cnt <= 7;	
				elsif current_state = SENDING then
					if bit_cnt > 0 and slowpulse = '1' then
						bit_cnt <= bit_cnt - 1;
					end if;
				end if;
			end if;
		end if;
	end process;
	--only if slowclock is 
	clk_out <= busy and not(slowclock);
	--clk_out <= not(clk) and busy;
	
end arch;
