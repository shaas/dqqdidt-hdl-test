library ieee;
use ieee.std_logic_1164.all;
USE IEEE.numeric_std.ALL;
--USE IEEE.fixed_pkg.all;

LIBRARY UQDSLib;
use UQDSLib.UQDSLib.ALL;

---------------------------------------------------------------------------------------------------------
-- Company: CERN
--
-- File: channel_malfunction_alerter.vhd
-- File history:
--      0.1: 10.10.2018: first attempt
--      
--  
--	<Revision number>: <Date>: <Comments>
--
-- Description: 
--
--!@brief keeps track of the last values measured by the adc and if they are all the same rises a flag
--	
--
--
-- Targeted device: IGLOO150
-- Author: Daniel Blasco
--
----------------------------------------------------------------------------------------------------------
entity channel_malfunction_alerter is
	generic(d_width     : integer range 15 to 32 := 32;
	        max_samples : integer range 8 to 255 := 16
	       );
	port(
		clk, rst    : in  std_logic;
		data_in     : in  std_logic_vector(d_width - 1 downto 0);
		data_rdy    : in  std_logic;
		malfunction : out std_logic
	);
end channel_malfunction_alerter;

architecture channel_malfunction_alerter_arch of channel_malfunction_alerter is

	signal counter   : integer range 0 to max_samples;
	signal prev_data : std_logic_vector(d_width - 1 downto 0);
	signal buf_drdy  : std_logic;

begin

	process(clk)
	begin
		if rising_edge(clk) then

			if rst = '1' then
				counter     <= 0;
				buf_drdy    <= '0';
				prev_data   <= (others => '0');
				malfunction <= '0';

			else

				buf_drdy <= data_rdy;
				if (data_rdy > buf_drdy) then

					prev_data <= data_in;
					if (data_in = prev_data) then

						if (counter < max_samples) then
							counter     <= counter + 1;
							malfunction <= '0';
						else
							malfunction <= '1';
						end if;

					else
						counter     <= 0;
						malfunction <= '0';
					end if;

				end if;

			end if;
		end if;
	end process;
end;
