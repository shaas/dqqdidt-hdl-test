LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
USE IEEE.fixed_pkg.all;
use IEEE.fixed_float_types.all;
--library RADlib;
--use RADlib.util.all;

use work.util.all;
use work.UQDSLib.all;
--------------------------------------------------------------------------------
-- Company: CERN
--
-- File: meanram_fsm_fast
-- File history:
--      v1.0    11/16/2011       Works well
--		v1.1	01/31/2012		regsum register too small for depths >5, fixed
--		v2.0	01/31/2012		read loop is accelerated by factor of two 
--		v4.0	08/29/2012		fully new implementation ram is instantiated by synplify
--      v4.1    14/02/2018      dblascos: changed d_width range up to 32 bits
--		v5.0	04.02.2019		generic allows for real multiplier
--		v6.0    15.04.2019		dblascos: complete algorithm change: now it keeps the sum and substracts the
--											last value and adds the new. It only takes a couple of clock cycles O(1)
--		v6.1	26.09.2019		as: correction to remove bug in 'hardware' version -> realmul := true
--		v6.2	05.11.2019		JS: bugfix: if filter params are changed after reset, length of array and divshift do not fit anymore
--								Added process checking if any param (length of array or division factor) changed, reset FSM accordingly
--
-- Description: 
--					completely new implementation based on fsm (easier to handle)
--					Entity works with real multiplier (using math blocks on IGLOO2) (realmul = TRUE) or with bitshift divider (realmul=FALSE)
--					According to operational mode parameters are interpreted differently:
--					filter_depth: length of array (needs to be 2^n in case of bitshift divider)
--					real_invdiv:  reciprocal of divisor in case of realmul=TRUE, number of rightshifts in case of realmul=FALSE 					
--
--
--!@brief Implements a moving average filter of depth d
--
--	Timing: Each read takes 2 clocks. 2 * 2^depth + 5: for 2^10 = 2053 clocks
--			@ 40MHz : 52us = 19kHz
--				
--
-- Targeted device: A3PE1500
-- Author: Jens Steckert
--
--------------------------------------------------------------------------------



entity meanram_fsm_fast is
generic(d_width : integer range 15 to 32 := 32;
		ram_depth : integer range 0 to 1024 := 1024;
		realmul : boolean := false
		);
port (
	clk				:in std_logic;
	sampleclock 	:in std_logic;
	rst		 		:in std_logic;
    Data_in			:in std_logic_vector(d_width -1 downto 0);
    filter_depth 	:in integer range 0 to ram_depth -1; 				-- length of filter array in # of samples
    real_invdiv		:in std_logic_vector(d_width -1 downto 0); -- reciprocal of integer number in s(7,24) in case of realmul, number of bitshifts otherwise
	Qout			:out std_logic_vector(d_width -1 downto 0);
	dready_out 		:out std_logic
	);
end meanram_fsm_fast;

architecture meanram_arch of meanram_fsm_fast is

--attribute syn_radhardlevel : string;
--attribute syn_radhardlevel of meanram_arch: architecture is "tmr";

----ram signals
constant rd: natural := log2c(ram_depth);

signal clk_i 		:std_logic;

---normal registers
signal circular_sum 	:signed(d_width-1 + rd downto 0);
signal preload_counter : integer range 0 to 1024;

signal samplepulse : std_logic;
signal oldsample :std_logic;

signal wpointer :integer range 0 to ram_depth-1;
--signal rpointer :integer range 0 to ram_depth-1;
signal mem_max : integer range 0 to ram_depth-1;

signal divshift : integer range 0 to log2c(ram_depth);
signal filter_depth_old : integer range 0 to ram_depth -1;
signal real_invdiv_old : std_logic_vector(d_width -1 downto 0);

signal rreg_tmr : std_logic_vector(d_width -1 downto 0);

--type ram_array is array(0 to ram_depth) of std_logic_vector(d_width -1 downto 0);
type ram_array is array(0 to ram_depth -1) of std_logic_vector(d_width -1 downto 0);
signal ram0 : ram_array;
signal ram1 : ram_array;
signal ram2 : ram_array;

signal sf_fxp		:sf_voltage;
signal mulresultfxp : sf_voltage;
--signal mulresult : std_logic_vector((d_width *2)-1 downto 0);
--dummy signal to show the fixed package the sizing of the origin and the target
signal rc : sf_voltage;
signal rs : sfixed(rc'left + rd downto rc'right);

attribute syn_ramstyle : string;
attribute syn_ramstyle of ram0 : signal is "no_rw_check";
attribute syn_ramstyle of ram1 : signal is "no_rw_check";
attribute syn_ramstyle of ram2 : signal is "no_rw_check";

attribute syn_preserve : boolean;
attribute syn_preserve of ram0: signal is true;
attribute syn_preserve of ram1: signal is true;
attribute syn_preserve of ram2: signal is true;

type state_type is (init, preload, idle, new_sample, calcmean, roundmul);

signal state : state_type;
signal fsm_reset : std_logic;


begin

clk_i <= clk;

divshift <= to_integer(unsigned(real_invdiv(log2c(ram_depth) downto 0)));

-- if relevant parameters change during running state of FSM, 
-- generate reset to re-init FSM and to take into account new parameters

filter_reset_generator: process(clk)
begin
	if rising_edge(clk) then
		if rst = '1' then
			--propagate reset
			fsm_reset <= '1';
			--always generate reset after reset ( 1 clock cycle)
			filter_depth_old <= 0;
			real_invdiv_old <= (others => '1');
			
		else
			filter_depth_old <= filter_depth;
			real_invdiv_old <= real_invdiv;
			if (filter_depth_old = filter_depth) AND (real_invdiv_old = real_invdiv) then
				--if no param has changed, keep FSM running
				fsm_reset <= '0';
			else
				--param changed value --> reset main FSM
				fsm_reset <= '1';
			end if;
		end if;
	end if;
end process;


spulse:process(clk_i)
begin
if rising_edge(clk_i) then 
	if rst = '1' then
		samplepulse <= '0';
		oldsample <= '0';
	else
		oldsample <= sampleclock;
		if oldsample < sampleclock then
			samplepulse <= '1';
		else
			samplepulse <= '0';
		end if;
	end if;
end if;
end process;


fsm: process(clk_i)
variable calc_mem_max : std_logic_vector(9 downto 0);
begin
if rising_edge(clk_i) then 
	if fsm_reset = '1' then
		wpointer <= 0;
		calc_mem_max := (others => '0');
		state <= init;
		Qout <= (others => '0');
		mem_max <= 0;
		dready_out <= '0';
		circular_sum <= (others => '0');
		preload_counter <= 0;
	else
		case state is
			when init => 
				
				mem_max <= filter_depth;
				state  <=  preload;
				--end if;
				
			when preload => 
				if samplepulse = '1' then -- Load the circular_buffer with all the values before starting to use it
					if preload_counter <= mem_max then
						ram0(preload_counter) <= Data_in;
						ram1(preload_counter) <= Data_in;
						ram2(preload_counter) <= Data_in;
						preload_counter <= preload_counter + 1;
						circular_sum <= signed(to_slv(resize(to_sfixed(std_logic_vector(circular_sum),rs) + to_sfixed(Data_in,rc),rs)));
						state <=  preload;
					else
						state  <=  idle;
					end if;
				else
				state <=  preload;	
				end if;
					
			when idle => 
				if samplepulse = '1' then
					-- TMR for RAM
					rreg_tmr <= (ram0(wpointer) AND ram1(wpointer)) OR (ram0(wpointer) AND ram2(wpointer)) OR (ram1(wpointer) AND ram2(wpointer));
					state <= new_sample;                  
				else
					state <= idle;
				end if;
				dready_out <= '0';
                -- Make sure that mem_max is calculated again to account for changes			
			when new_sample =>
				ram0(wpointer) <= Data_in;
				ram1(wpointer) <= Data_in;
				ram2(wpointer) <= Data_in;
                -- Since the buffer is circular, wpointer is updated to the first in sample
                -- in the next idle cycle, data is taken from that position and in the next new_sample, data is written to the same position before updating wcounter
                if wpointer = (mem_max) then 
                    wpointer <= 0;
                else
                    wpointer <= wpointer + 1;
                end if;
				--rpointer <= mem_max;
				circular_sum <= signed(to_slv(resize(to_sfixed(std_logic_vector(circular_sum),rs) - to_sfixed(rreg_tmr,rc) + to_sfixed(Data_in,rc),rs)));
				
				state <= calcmean;
				
			when calcmean =>
				if (realmul = false) then
					Qout <= std_logic_vector(shift_right(circular_sum, divshift)(d_width-1 downto 0));
					state <= idle;
					dready_out <= '1';
				else
					--let the fixed point package take care about all the sizing			
					--mulresult <= to_slv(resize(to_sfixed(std_logic_vector(circular_sum), rs) * to_sfixed(real_invdiv, rc), rc'high, rc'low));
					
					mulresultfxp <= resize(
							arg 			=> (to_sfixed(std_logic_vector(circular_sum), rs) * to_sfixed(real_invdiv, rc)), 
							size_res 		=> sf_fxp,
							overflow_style 	=> fixed_saturate,
							round_style		=> fixed_truncate 
							);
							
					state <= roundmul;
					dready_out <= '0';
				end if;
				
			when roundmul =>
				--Qout <= to_slv(resize(to_sfixed(std_logic_vector(circular_sum), rs) * to_sfixed(real_invdiv, rc), rc'high, rc'low));
				Qout <= to_slv(mulresultfxp);
				state <= idle;
				dready_out <= '1';
				
			when others =>
				state <= idle;
		end case;
	end if;
end if;
end process;
		



end;