--------------------------------------------------------------------------------
-- Company: <Name>
--
-- File: GeneralPurposeRegister.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- <Description here>
--
-- Targeted device: <Family::IGLOO2> <Die::M2GL060> <Package::484 FBGA>
-- Author: <Name>
--
--------------------------------------------------------------------------------

LIBRARY IEEE;

USE IEEE.std_logic_1164.ALL;

ENTITY GeneralPurposeRegister IS
	GENERIC(
		-- defines which bit is read only and this bit can be assigned 
		--  to any internal signal
		registerMask    : STD_LOGIC_VECTOR(31 downto 0) := x"00000000";
		-- defines the default vaue of the register after reset. It is 
		-- overided by the mask value
		registerDefault : STD_LOGIC_VECTOR(31 downto 0) := x"00000000"
	);
	PORT(
		DataIn  : IN  STD_LOGIC_VECTOR(31 downto 0);
		DataOUT : OUT STD_LOGIC_VECTOR(31 downto 0);
		regWR   : IN  STD_LOGIC;
		clk     : IN  STD_LOGIC;
		nRST    : IN  STD_LOGIC
	);
END GeneralPurposeRegister;

ARCHITECTURE behav of GeneralPurposeRegister is
	SIGNAL Reg : STD_LOGIC_VECTOR(31 downto 0);
BEGIN
	RegisterControll : PROCESS(clk, nRST)
	BEGIN
		IF clk'event AND clk = '1' THEN
			IF nRST = '0' THEN
				REG <= registerDefault;
			else
				IF regWR = '1' THEN
					REG <= DataIn;
				END IF;
			END IF;
		end if;
	END PROCESS;

	generateOutputVector : FOR i IN 31 DOWNTO 0 GENERATE
	BEGIN
		outputAssigmentReg : IF (registerMask(i) = '1') GENERATE
		BEGIN
			DataOut(i) <= Reg(i);
		END GENERATE;
		outputAssigmentDirect : IF (registerMask(i) = '0') GENERATE
		BEGIN
			DataOut(i) <= DataIn(i);
		END GENERATE;
	END GENERATE;
END behav;
