LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.std_logic_unsigned.ALL;
use ieee.numeric_std.all;

--------------------------------------------------------------------------------
-- Company: CERN
--
-- File: spi_slave.vhd
-- File history:
--      0.1: 21.03.2012: first attempt, untested
--		0.2: 21.06.2012: full duplex, untested
--      0.3: 25.06.2012: new design, samples on falling edge
--		0.4: 16.10.2012: clear all signals during reset
--		0.5: 10.01.2013: compacted code, added check for csn = 1 during transmission
--		0.6: 19.02.2014: added synchronizer for SPI inputs
--		0.7: 11.08.2016: changes in operation of rx_valid_pulse and tx_valid_pulse; synchro reset 
--		0.8: 21.06.2017: sdout is tristanted if CSN= 1 
--		0.9: 28.08.2017: quick solution for long transactions; chip select low; sclk combines functionality of both signals: cs and sclk 
--		1.0: 29.08.2017: new re-design fpt sclk idle low (CPOL=0) and first edge (CPHA=1)
--		1.1: 13.02.2018: added watchdog and register for holding the whole byte during adquisition
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- implements an SPI slave
--
-- ToDo:
--		test !
--
-- Targeted device: ProAsic3 A3P1500 PQ208
-- Author: Jens Steckert, Andrzej Skoczen, Dani Blasco
--
--------------------------------------------------------------------------------

entity spi_slave is
generic(trans_length : natural := 8);
port (clk, rst 		:in std_logic;
    --spi hw lines
    spi_csn			:in std_logic;
	spi_sclk		:in std_logic;
	spi_sdin		:in std_logic;
	spi_sdout		:out std_logic;
	--internal signals
	rdata			:out std_logic_vector(7 downto 0);
	wdata			:in std_logic_vector(7 downto 0);
	-- spi_active		:out std_logic;
	-- rx_valid		:out std_logic;
	-- tx_valid		:out std_logic;
	rx_valid_pulse	:out std_logic;
	tx_valid_pulse	:out std_logic
	--tx_req			:out std_logic
);
end spi_slave;


--! @brief implements an spi slave
--! @details 

architecture spi_slave_arch of spi_slave is

type state_type is (idle, tx_n, tx0, rx_n, rx0, nextbit, done, delay_lsb);
signal state : state_type;

--signal sclk_old : std_logic;
signal clock_pos_pulse :std_logic;
signal clock_neg_pulse :std_logic;
signal input_byte :std_logic_vector(7 downto 0);
--signal output_byte : std_logic_vector(7 downto 0);
signal txcnt : integer range 0 to 7;
signal rxcnt : integer range 0 to 7;

signal spi_sclk_sync		:	std_logic;
signal spi_csn_sync         :	std_logic;
signal spi_sdin_sync        :	std_logic;
signal spi_sclk_reg         :	std_logic;
signal spi_csn_reg          :	std_logic;
signal spi_sdin_reg         :	std_logic;
signal nedsclk : std_logic_vector(1 downto 0) := "11";
signal pedsclk : std_logic_vector(1 downto 0) := "00";
signal sdout, clock_pos_pulse_tx1 				: std_logic;
signal cnt_dly : std_logic_vector(3 downto 0);

CONSTANT WATCHDOGTIMEOUT : INTEGER := 1000;
SIGNAL cntWatchDog       : INTEGER range 0 to WATCHDOGTIMEOUT;

signal wdata_reg_hold : std_logic_vector(7 downto 0);

begin
	
synchro : process(clk)
Begin
	if rising_edge(clk) then
		spi_sclk_reg	<= spi_sclk;
		spi_csn_reg     <= spi_csn;
		spi_sdin_reg    <= spi_sdin;
		spi_sclk_sync	<= spi_sclk_reg;
		spi_csn_sync    <= spi_csn_reg;
		spi_sdin_sync   <= spi_sdin_reg;
	end if;
end process;

ned_sclk_gen: process(clk) begin
	if rising_edge(clk) then
		nedsclk <= nedsclk(0) & spi_sclk;
	end if;
end process;
clock_neg_pulse <= nedsclk(1) and not nedsclk(0);

ped_sclk_gen: process(clk) begin
	if rising_edge(clk) then
		pedsclk <= pedsclk(0) & spi_sclk;
	end if;
end process;
clock_pos_pulse <= not pedsclk(1) and pedsclk(0);

tx_valid_pulse <= clock_pos_pulse  when (state = idle) else '0';
--when (txcnt = trans_length-1) else '0';
rx_valid_pulse <= '1' when (state = done) else '0';

special_pulse_tx1: process(clk) begin
	if rising_edge(clk) then
		if rst = '1' then
			clock_pos_pulse_tx1 <= '0';
		else
			clock_pos_pulse_tx1 <= tx_valid_pulse;
		end if;
	end if;
end process;

spi_sdout <= sdout when spi_csn = '0' else 'Z';

fsm: process(clk, rst) begin
if rising_edge(clk) then
	cntWatchDog  <= cntWatchDog + 1;
	if rst = '1' OR (cntWatchDog > WATCHDOGTIMEOUT) then
		state <= idle;
		sdout <= '0';
		--output_byte <= x"00";
		input_byte <= x"00";
		rdata <= x"00";
		txcnt <= trans_length-1;
		rxcnt <= trans_length-1;
		cntWatchDog  <= 0;
	else
		case state is
			when idle =>
				if clock_pos_pulse = '1' AND spi_csn_sync = '0' then
					state <= tx_n;
					wdata_reg_hold <= wdata;
				else
					sdout <= '0';
					state <= idle;
					cntWatchDog  <= 0;
					txcnt <= trans_length-1;
					rxcnt <= trans_length-1;
				end if;
				
			when tx_n =>
				if (clock_pos_pulse_tx1 = '1' or clock_pos_pulse = '1') AND spi_csn_sync = '0' then --chip select has to be low
					txcnt <= txcnt - 1;
					sdout <= wdata_reg_hold(txcnt);
					state <= rx_n;
				else
					if spi_csn_sync = '1' then
						state <= idle;
					else
						state <= tx_n;
					end if;
				end if;
			
			when rx_n =>
				if clock_neg_pulse = '1' AND spi_csn_sync = '0' then --chip select has to be low
					rxcnt <= rxcnt - 1;
					input_byte(rxcnt) <= spi_sdin_sync;
					state <= nextbit;
				else
					if spi_csn_sync = '1' then
						state <= idle;
					else
						state <= rx_n;
					end if;
				end if;
				
			when nextbit =>
				if txcnt > 0 then
					state <= tx_n;
				else 
					state <= tx0;
				end if;
				
			when tx0 =>
				if clock_pos_pulse = '1' AND spi_csn_sync = '0' then --chip select has to be low
					sdout <= wdata_reg_hold(txcnt);
					--tx_valid <= '0';
					state <= rx0;
				else
					if spi_csn_sync = '1' then
						state <= idle;
					else
						state <= tx0;
					end if;
				end if;
			
			when rx0 =>
				if clock_neg_pulse = '1' AND spi_csn_sync = '0' then --chip select has to be low
					input_byte(rxcnt) <= spi_sdin_sync;
					state <= delay_lsb;
				else
					if spi_csn_sync = '1' then
						state <= idle;
					else
						state <= rx0;
					end if;
				end if;		
				
			when delay_lsb =>
				if spi_csn_sync = '0' then
					if cnt_dly > 0 then
						state <= delay_lsb;
					else
						state <= done;
					end if;
				else
					state <= idle;
				end if;
				
			when done =>
				rdata <= input_byte;
				txcnt <= trans_length-1;
				rxcnt <= trans_length-1;
				state <= idle;
				cntWatchDog <= 0;
			
		end case;
	end if;
end if;
end process;

delay_counter: process(clk) 
begin
	if rising_edge(clk) then
		if rst = '1' then
			cnt_dly <= "1010";
		else
			case state is 
				when delay_lsb => cnt_dly <= cnt_dly - '1';
				when others => cnt_dly <= "1010";
			end case;
		end if;
	end if;
end process;

end;