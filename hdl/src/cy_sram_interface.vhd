LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
use ieee.numeric_std.all;

--------------------------------------------------------------------------------
-- Company: CERN, AGH-UST
--
-- File: cy_sram_interface.vhd
-- File history:
--      0.1: 21.03.2012: first attempt, untested
--      0.2: 22.09.2015: upgraded, tested
--      0.3: 17.03.2017: corrections 
--    
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- interfaces to the Cypress SRAM 16Mbit, supports read and write
-- CY62147GE_45
--
-- Targeted device: ProAsic3 A3P1500 PQ208
-- Author: Jens Steckert
-- Rewrited, optimised: Karol Witkowski
-- Corrections: A.Skoczen, AGH-UST
-- 
--------------------------------------------------------------------------------

entity cy_sram_interface is
generic (
	dw 	: natural := 16; -- data width
	aw 	: natural := 17; -- address width
	dbg : boolean := false
);
port (
    clk, rst 		:in std_logic;
    --HW signals of sram
    ram_oe			:out std_logic;
	ram_bhe			:out std_logic;
	ram_ble			:out std_logic;
	ram_ce			:out std_logic;
	ram_we			:out std_logic;
	ram_adr			:out std_logic_vector(aw-1 downto 0);
	ram_io			:inout std_logic_vector(dw-1 downto 0);
	--data/controls
	adr_in			:in std_logic_vector(aw-1 downto 0);
	rdata			:out std_logic_vector(dw-1 downto 0);
	wdata			:in std_logic_vector(dw-1 downto 0);
	r_done			:out std_logic;
	w_done			:out std_logic;
	cmd				:in std_logic_vector(1 downto 0);
	debug			:out std_logic_vector(3 downto 0)
);
end cy_sram_interface;


--! @brief interfaces to the cyress sram chip, supports low level read and write commands
--! @details 

architecture cy_sram_interface_arch of cy_sram_interface is
-- constants
constant max_counter : integer := 2; -- 2 is the lowest number with working sram 
constant read_cmd : std_logic_vector(1 downto 0) := "01";
constant write_cmd : std_logic_vector(1 downto 0) := "10";

type state_type is (idle, wait_read, read_ram, read_finished, wait_write, write_finished);
signal state, next_state : state_type;

--signal ram_wdata	: std_logic_vector(dw-1 downto 0);

signal counter : integer range 0 to max_counter;
signal read_from_ram : std_logic;
signal fsm_debug : std_logic_vector(3 downto 0) := "0000";

begin

ram_io <=  (others =>'Z') when read_from_ram = '1' else wdata;
--ram_io <=  wdata when read_from_ram = '0' else (others =>'Z');

fsm: process (all)
begin  
	ram_ce <= '1'; 		--unselect chip
	ram_oe <= '1'; 		--disable ouput
	ram_we <= '1'; 		--disable write enable 
	ram_ble <= '1';		--disable lower byte 
	ram_bhe	<= '1';		--disable high byte
	read_from_ram <= '1';--set IO to high z --> read
	r_done <= '0';
	w_done <= '0';
	case state is
		when idle =>
			case cmd is
				when read_cmd =>				--read
					next_state <= wait_read; 
				when write_cmd =>				--write
	--				ram_wdata <= wdata; --register wdata
					next_state <= wait_write;
				when others =>
					next_state <= idle;
			end case;
		----Implement ready cyle no 2
		when wait_read =>		--25 ns later, set control signals
			ram_ce <= '0'; 		--select chip
			ram_oe <= '0'; 		--output enable
			ram_ble <= '0';		--activate lower byte 
			ram_bhe	<= '0';		--activate high byte
			if counter = max_counter-1 then 
				next_state <= read_ram;
			else 
				next_state <= wait_read;
			end if;
		when read_ram =>
			-- next_state <= read_finished;
		-- when read_finished =>
			next_state <= idle;
			r_done <= '1';
		---implement write cycle no 1
		when wait_write =>	--25 ns later, apply control signals
			ram_ce <= '0'; 		--select chip
			ram_we <= '0'; 		--write enable 
			ram_ble <= '0';		--activate lower byte 
			ram_bhe	<= '0';		--activate high byte
			if counter > 0 then 
				read_from_ram <= '0';
			end if;
			if counter = max_counter-1 then 
				next_state <= write_finished;
			else 
				next_state <= wait_write;
			end if;
		when write_finished =>
			read_from_ram <= '1';	 --keep io high z to avoid short
			next_state <= idle;
			w_done <= '1';
		when others =>
			next_state <= idle;
	end case;        
end process;

read_data_update: process(clk) begin
	if rising_edge(clk) then
		if (rst='1') then
			rdata <= (others => '0');
		else 
			if state = wait_read then
				rdata <= ram_io;  
			end if;
		end if;
	end if;
end process; 

address_update: process(clk) begin
	if rising_edge(clk) then
		if (rst='1') then
			ram_adr <= adr_in;
		else
			if state = idle and (next_state = wait_read or next_state = wait_write) then 
				ram_adr <= adr_in;
			end if;
		end if;
	end if;
end process;

count_for_wait: process(clk) begin
	if rising_edge(clk) then
		if (rst='1') then
			counter <= 0;
		else 
			if state = wait_read or state = wait_write then 
				counter <= counter +1; 
			else
				counter <= 0;
			end if;
		end if;
	end if;
end process;

state_reg: process(clk) begin
	if rising_edge(clk) then
		if (rst='1') then
			state <= idle;
		else 
			state <= next_state;
		end if;
	end if;
end process;	

debug <= fsm_debug when dbg else "0000";
debug_comb: if dbg generate  
	dbug_fsm: process (all) begin
		fsm_debug <= "1000";
		case state is
			when idle => fsm_debug <= "0001";
			when wait_read => fsm_debug <= "0010";
			when read_ram => fsm_debug <= "0100";
			when read_finished => fsm_debug <= "0101";
			when wait_write => fsm_debug <= "1001";
			when write_finished => fsm_debug <= "1011";
			when others => fsm_debug <= "0000";
		end case; 
	end process;
end generate debug_comb;
	
end;
