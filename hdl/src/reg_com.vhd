library ieee;
use ieee.std_logic_1164.all;
USE IEEE.numeric_std.all;

library UQDSLib;
use UQDSLib.UQDSLib.all;

---------------------------------------------------------------------------------------------------------
-- Company: CERN
--
-- File: reg_com.vhd
-- File history:
--      0.1: 1.4.2019: first attempt
--      
--  
--	<Revision number>: <Date>: <Comments>
--
-- Description: 
--
--!@brief Communication interface for reading and writing registers using either SPI or UART (with two different interfaces supported)
--	
--
--
-- Targeted device: IGLOO150
-- Author: Daniel Blasco
--
----------------------------------------------------------------------------------------------------------
entity reg_com is
	port(
		clk             : in  std_logic;
		rst             : in  std_logic;
		-- RS485 and FTDI
		uartComTX_FTDI  : out std_logic;
		uartComRX_FTDI  : in  std_logic;
		uartComTX_RS485 : out std_logic;
		uartComRX_RS485 : in  std_logic;
		--SPI
		spi_csn         : in  std_logic;
		spi_sclk        : in  std_logic;
		spi_sdin        : in  std_logic;
		spi_sdout       : out std_logic;
		-- Write register
		dataIn          : out std_logic_vector(31 downto 0);
		regWRsig        : out std_logic_vector(2**REGISTER_ADR_WIDTH - 1 downto 0);
		-- Read register
		regAddress      : out std_logic_vector(REGISTER_ADR_WIDTH - 1 downto 0);
		dataOut         : in  std_logic_vector(31 downto 0)
	);
end reg_com;

architecture reg_com of reg_com is

	constant WATCHDOG_MAX : integer := 1000000;

	signal uartComTX : std_logic;
	signal uartComRX : std_logic;

	signal uart_nWR : std_logic;
	signal uart_nRD : std_logic;

	signal uartTXRdy : std_logic;
	signal uartRXRdy : std_logic;
	signal SPIRXRdy  : std_logic;
	signal SPITXRdy  : std_logic;

	signal SPIuartDataTX : std_logic_vector(7 downto 0);
	signal uartDataRX    : std_logic_vector(7 downto 0);
	signal SPIDataRX     : std_logic_vector(7 downto 0);

	type states is (IDLE, RECEIVE_COMMAND, WAIT_ADDR, RECEIVE_ADDR, -- Common states
	                WAIT_REG_VALUE, RECEIVE_REG_VALUE, WAIT_CRC_WRITE, CHECK_CRC_WRITE, PUSH_REG, -- Write register states
	                WAIT_CRC_READ, CHECK_CRC_READ, PULL_REG, WAIT_SEND_REG_VALUE, WAIT_SEND_CRC, -- Read register states
	                UART_WAIT_CLK1, UART_WAIT_CLK2, UART_WAIT_CLK3, UART_WAIT_CLK4, -- UART needs extra clock when reading/writing
	                DONE);
	signal state : states;

	type protocols is (UART, SPI);
	signal protocol : protocols;
	type commands is (WRITE, READ);
	signal command : commands;

	signal byte_counter         : integer range 0 to 4;
	signal accumulated_checksum : std_logic_vector(7 downto 0);

	signal reg_address : std_logic_vector(7 downto 0);
	signal reg_value   : std_logic_vector(31 downto 0);

	signal watchdog_counter : integer range 0 to WATCHDOG_MAX;

	component top_UART is
		port(
			RESET_N           : in  std_logic;
			CLK               : in  std_logic;
			WEN               : in  std_logic;
			OEN               : in  std_logic;
			CSN               : in  std_logic;
			DATA_IN           : in  std_logic_vector(7 downto 0);
			RX                : in  std_logic;
			BAUD_VAL          : in  std_logic_vector(12 downto 0);
			BIT8              : in  std_logic; --   IF SET TO ONE 8 DATA BITS OTHERWISE 7 DATA BITS
			PARITY_EN         : in  std_logic; --   IF SET TO ONE PARITY IS ENABLED OTHERWISE DISABLED
			ODD_N_EVEN        : in  std_logic; --   IF SET TO ONE ODD PARITY OTHERWISE EVEN PARITY
			BAUD_VAL_FRACTION : in  std_logic_vector(2 downto 0); --USED TO ADD EXTRA PRECISION TO BAUD VALUE WHEN BAUD_VAL_FRCTN_EN = 1
			PARITY_ERR        : out std_logic; --   PARITY ERROR INDICATOR ON RECIEVED DATA
			OVERFLOW          : out std_logic; --   RECEIVER OVERFLOW
			TXRDY             : out std_logic; --   TRANSMIT READY FOR ANOTHER BYTE
			RXRDY             : out std_logic; --   RECEIVER HAS A BYTE READY
			DATA_OUT          : out std_logic_vector(7 downto 0);
			TX                : out std_logic;
			FRAMING_ERR       : out std_logic
		);
	end component;

begin

	FSM : process(clk)
		variable crc_aux : std_logic_vector(7 downto 0);
	begin
		if rising_edge(clk) then

			if rst = '1' or watchdog_counter = 0 then
				state                <= IDLE;
				uart_nWR             <= '1';
				uart_nRD             <= '1';
				SPIuartDataTX        <= (others => '0');
				byte_counter         <= 0;
				accumulated_checksum <= (others => '0');
				regWRsig             <= (others => '0');
				reg_value            <= (others => '0');
				watchdog_counter     <= WATCHDOG_MAX;
			else

				watchdog_counter <= watchdog_counter - 1; -- In case FSM is stuck anywhere except IDLE state, reset

				case state is

					when IDLE =>
						if uartRXRdy = '1' then
							protocol <= UART;
							uart_nRD <= '0';
							state    <= RECEIVE_COMMAND;
						elsif SPIRXRdy = '1' then
							protocol <= SPI;
							state    <= RECEIVE_COMMAND;
						else
							state <= IDLE;
						end if;
						watchdog_counter <= WATCHDOG_MAX;

					when RECEIVE_COMMAND => -- Check the first byte received (0x01 write, 0x02 read)
						if protocol = UART then
							uart_nRD             <= '1';
							accumulated_checksum <= accumulated_checksum xor uartDataRX;
							state                <= UART_WAIT_CLK1;
						else
							accumulated_checksum <= accumulated_checksum xor SPIDataRX;
							if SPIDataRX = x"01" then
								command <= WRITE;
								state   <= WAIT_ADDR;
							elsif SPIDataRX = x"02" then
								command <= READ;
								state   <= WAIT_ADDR;
							else
								state <= DONE; -- Command unknown
							end if;
						end if;

					when UART_WAIT_CLK1 =>
						if uartDataRX = x"01" then
							command <= WRITE;
							state   <= WAIT_ADDR;
						elsif uartDataRX = x"02" then
							command <= READ;
							state   <= WAIT_ADDR;
						else
							state <= DONE; -- Command unknown
						end if;

					when WAIT_ADDR =>   -- Wait for register address
						if protocol = UART and uartRXRdy = '1' then
							uart_nRD <= '0';
							state    <= RECEIVE_ADDR;
						elsif protocol = SPI and SPIRXRdy = '1' then
							state <= RECEIVE_ADDR;
						else
							state <= WAIT_ADDR;
						end if;

					when RECEIVE_ADDR => -- Get register address and split ways depending on write or read
						if protocol = UART then
							uart_nRD             <= '1';
							accumulated_checksum <= accumulated_checksum xor uartDataRX;
							reg_address          <= uartDataRX;
							state                <= UART_WAIT_CLK2;
						else
							accumulated_checksum <= accumulated_checksum xor SPIDataRX;
							reg_address          <= SPIDataRX;
							if command = WRITE then
								byte_counter <= 0;
								state        <= WAIT_REG_VALUE;
							else
								state <= WAIT_CRC_READ;
							end if;
						end if;

					when UART_WAIT_CLK2 =>
						if command = WRITE then
							byte_counter <= 0;
							state        <= WAIT_REG_VALUE;
						else
							state <= WAIT_CRC_READ;
						end if;

					------------------------------------ Writing register states ------------------------------------

					when WAIT_REG_VALUE => -- Wait for each of the 4 bytes of the register value
						if protocol = UART and uartRXRdy = '1' then
							byte_counter <= byte_counter + 1;
							uart_nRD     <= '0';
							state        <= RECEIVE_REG_VALUE;
						elsif protocol = SPI and SPIRXRdy = '1' then
							byte_counter <= byte_counter + 1;
							state        <= RECEIVE_REG_VALUE;
						else
							state <= WAIT_REG_VALUE;
						end if;

					when RECEIVE_REG_VALUE => -- Populate the 32bit register one byte at time
						if protocol = UART then
							uart_nRD                                                            <= '1';
							accumulated_checksum                                                <= accumulated_checksum xor uartDataRX;
							reg_value(8*(4 - byte_counter + 1) - 1 downto 8*(4 - byte_counter)) <= uartDataRX;
							state                                                               <= UART_WAIT_CLK3;
						else
							accumulated_checksum                                                <= accumulated_checksum xor SPIDataRX;
							reg_value(8*(4 - byte_counter + 1) - 1 downto 8*(4 - byte_counter)) <= SPIDataRX;
							if byte_counter = 4 then
								state <= WAIT_CRC_WRITE;
							else
								state <= WAIT_REG_VALUE;
							end if;
						end if;

					when UART_WAIT_CLK3 =>
						if byte_counter = 4 then
							state <= WAIT_CRC_WRITE;
						else
							state <= WAIT_REG_VALUE;
						end if;

					when WAIT_CRC_WRITE =>
						if protocol = UART and uartRXRdy = '1' then
							uart_nRD <= '0';
							state    <= CHECK_CRC_WRITE;
						elsif protocol = SPI and SPIRXRdy = '1' then
							state <= CHECK_CRC_WRITE;
						else
							state <= WAIT_CRC_WRITE;
						end if;

					when CHECK_CRC_WRITE =>
						if protocol = UART then
							uart_nRD <= '1';
							if accumulated_checksum = uartDataRX then
								state <= PUSH_REG;
							else
								state <= DONE; -- CRC was not right
							end if;
						else
							if accumulated_checksum = SPIDataRX then
								state <= PUSH_REG;
							else
								state <= DONE; -- CRC was not right
							end if;
						end if;

					when PUSH_REG =>
						regWRsig                                    <= (others => '0');
						regWRsig(to_integer(unsigned(reg_address))) <= '1';
						dataIn                                      <= reg_value;
						state                                       <= DONE;

					------------------------------------ Reading register states ------------------------------------

					when WAIT_CRC_READ =>
						if protocol = UART and uartRXRdy = '1' then
							uart_nRD <= '0';
							state    <= CHECK_CRC_READ;
						elsif protocol = SPI and SPIRXRdy = '1' then
							state <= CHECK_CRC_READ;
						else
							state <= WAIT_CRC_READ;
						end if;

					when CHECK_CRC_READ =>
						if protocol = UART then
							uart_nRD <= '1';
							if accumulated_checksum = uartDataRX then
								accumulated_checksum <= accumulated_checksum xor uartDataRX;
								regAddress           <= reg_address;
								state                <= PULL_REG;
							else
								state <= DONE; -- CRC was not right
							end if;
						else
							if accumulated_checksum = SPIDataRX then
								accumulated_checksum <= accumulated_checksum xor SPIDataRX;
								regAddress           <= reg_address;
								state                <= PULL_REG;
							else
								state <= DONE; -- CRC was not right
							end if;
						end if;

					when PULL_REG =>
						reg_value     <= dataOut;
						byte_counter  <= 1;
						state         <= WAIT_SEND_REG_VALUE;
						SPIuartDataTX <= reg_value(8*(4 - byte_counter + 1) - 1 downto 8*(4 - byte_counter));

					when WAIT_SEND_REG_VALUE => -- Send byte by byte the whole register value kept in reg_value
						SPIuartDataTX        <= reg_value(8*(4 - byte_counter + 1) - 1 downto 8*(4 - byte_counter));
						if protocol = UART and uartTXRdy = '1' then
							uart_nWR <= '0';
							state    <= UART_WAIT_CLK4;
						elsif protocol = SPI and SPITXRdy = '1' then
							if byte_counter = 4 then
								--SPIuartDataTX <= accumulated_checksum;
								-- Keep also the CRC
								crc_aux              := accumulated_checksum;
								crc_aux              := crc_aux xor reg_value(31 downto 24);
								crc_aux              := crc_aux xor reg_value(23 downto 16);
								crc_aux              := crc_aux xor reg_value(15 downto 8);
								crc_aux              := crc_aux xor reg_value(7 downto 0);
								accumulated_checksum <= crc_aux;
								state         <= WAIT_SEND_CRC;
							else
								state <= WAIT_SEND_REG_VALUE;
							end if;
							byte_counter <= byte_counter + 1;
						else
							uart_nWR <= '1';
							state    <= WAIT_SEND_REG_VALUE;
						end if;

					when UART_WAIT_CLK4 =>
						uart_nWR     <= '1';
						if byte_counter = 4 then
							--SPIuartDataTX <= accumulated_checksum;
							-- Keep also the CRC
							crc_aux              := accumulated_checksum;
							crc_aux              := crc_aux xor reg_value(31 downto 24);
							crc_aux              := crc_aux xor reg_value(23 downto 16);
							crc_aux              := crc_aux xor reg_value(15 downto 8);
							crc_aux              := crc_aux xor reg_value(7 downto 0);
							accumulated_checksum <= crc_aux;
							state         <= WAIT_SEND_CRC;
						else
							state <= WAIT_SEND_REG_VALUE;
						end if;
						byte_counter <= byte_counter + 1;

					when WAIT_SEND_CRC =>
						if protocol = UART and uartTXRdy = '1' then
							uart_nWR <= '0';
							state    <= DONE;
						elsif protocol = SPI and SPITXRdy = '1' then
							state <= DONE;
						else
							uart_nWR      <= '1';
							SPIuartDataTX <= accumulated_checksum;
							state         <= WAIT_SEND_CRC;
						end if;

					when DONE =>
						uart_nWR             <= '1';
						uart_nRD             <= '1';
						SPIuartDataTX        <= (others => '0');
						byte_counter         <= 0;
						accumulated_checksum <= (others => '0');
						regWRsig             <= (others => '0');
						reg_value            <= (others => '0');
						state                <= IDLE;

				end case;
			end if;
		end if;
	end process;

	uartCom : top_UART
		PORT MAP(
			RESET_N           => not rst,
			CLK               => clk,
			WEN               => uart_nWR, -- 0 when sending byte out
			OEN               => uart_nRD, -- 0 when reading available byte
			CSN               => '0',
			DATA_IN           => SPIuartDataTX,
			RX                => uartComRX,
			--BAUD_VAL            => '0' & x"001", --1.25Mbit   
			BAUD_VAL          => '0' & x"000", --2.5Mbit   
			--BAUD_VAL          => '0' & x"015", --115200bit
			--BAUD_VAL          => '0' & x"004", --500000bit
			BIT8              => '1',
			PARITY_EN         => '0',
			ODD_N_EVEN        => '0',
			BAUD_VAL_FRACTION => "000", --0 correction
			PARITY_ERR        => open,
			OVERFLOW          => open,
			TXRDY             => uartTXRdy, -- ready in 1 when byte can be sent out
			RXRDY             => uartRXRdy, -- ready in 1 when a byte is available to read
			DATA_OUT          => uartDataRX,
			TX                => uartComTX,
			FRAMING_ERR       => open
		);

	-- Can use FTDI or RS485 simultaneously
	uartComTX_FTDI  <= uartComTX;
	uartComTX_RS485 <= uartComTX;
	uartComRX       <= '0' when (uartComRX_FTDI = '0' or uartComRX_RS485 = '0') else '1';

	spiCom : spi_slave
		generic map(
			trans_length => 8
		)
		port map(
			clk            => clk,
			rst            => rst,
			--spi hw lines
			spi_csn        => spi_csn,
			spi_sclk       => spi_sclk,
			spi_sdin       => spi_sdin,
			spi_sdout      => spi_sdout,
			--internal signals
			rdata          => SPIDataRX,
			wdata          => SPIuartDataTX,
			rx_valid_pulse => SPIRXRdy, -- ready in 1 when a byte is available to read
			tx_valid_pulse => SPITXRdy  -- ready in 1 when byte can be sent out
		);

end;
