library ieee ;
--LIBRARY IEEE_PROPOSED;

use ieee.std_logic_1164.all;
USE IEEE.numeric_std.ALL;

--LIBRARY IEEE_PROPOSED;
USE IEEE.fixed_pkg.all;
use IEEE.fixed_float_types.all;


LIBRARY UQDSLib;
use UQDSLib.UQDSLib.ALL;
--library RADlib;
--use RADlib.all;

--A.Skoczen, AGH-UST
----------------------------------------------------------------------------------------------------------
entity corr_fixed is 
generic(d_width : integer range 15 to 32 := 20;
		o_width : integer range 15 to 32 := 16;
	    c_width : integer range 4 to 24 := 24;
		outreg : boolean := TRUE);

port (clk, rst : in std_logic;
	din	 : in std_logic_vector(d_width -1 downto 0);
	cfactor	: in std_logic_vector(c_width -1 downto 0);
	offset	:in std_logic_vector(o_width -1 downto 0);
	dout : out std_logic_vector(d_width -1 downto 0) );
end entity;

architecture rtl of corr_fixed is
component multS is
generic (W1, W2: natural; omode: std_logic);
port(clk, rst, start	:in std_logic;
	multiplier	:in std_logic_vector(W1-1 downto 0);
	multiplicand	:in std_logic_vector(W2-1 downto 0);
	done		:out std_logic;
	product		:out std_logic_vector(W1+W2-2 downto 0));
end component;

signal mul_result_raw : std_logic_vector(d_width + c_width -1 downto 0);
signal mul_rdy : std_logic;
signal gain : std_logic_vector(c_width downto 0);

--create fixed point signed vector with proper length
--multiplier: s(7,24) x s(2,14) = s(9,38)
signal mul_result_fx : sfixed(9 downto -38);
signal offset_fx : sf_voltage;
signal standard_fxp : sf_voltage;
signal fsp, fsn : std_logic_vector(d_width -1 downto 0);

begin

--convert cfactor from unsigned to signed
--cfactor is u(2,14)
gain <= '0' & cfactor;
fsp <= (fsp'left =>'0',others=>'1'); 
fsn <= (fsn'left =>'1',others =>'0');


mul_seq: multS 
	generic map(W1 => d_width, W2 => c_width+1, omode => '1')
	port map (clk => clk, rst => rst, 
			start => '1', 
			multiplier => din,
			multiplicand => gain,	
			product => mul_result_raw,
			done => mul_rdy);

--convert result to fixed s(9,38)
mul_result_fx <= to_sfixed(mul_result_raw, mul_result_fx);
--convert offset value to fixed (standard UQDS voltage)
offset_fx <= to_sfixed(offset, offset_fx);
 

--result_crop <= resize(	arg => mul_result_fx,		--number to be resized
--						size_res => result_crop, 	--resize to crop vector length
--						overflow_style => TRUE,		--saturate number
--						round_style => FALSE		--don't round, just cut
--						);

--for the case that the input signal to corrector is already saturated, just propagate input, otherwise propagate result
saturation_check: process(clk)
begin
	if rising_edge(clk) then
		if rst = '1' then
			dout <= (others => '0');
		else
			--check whether input signal is full-scale positive or negative
			if (din = fsn OR din = fsp) then
				--signal is already saturated, just propagate
				dout <= din;
			else
				--add offset and crop
				dout <= to_slv(resize(	arg => (mul_result_fx + offset_fx),
										size_res 		=> standard_fxp,
										overflow_style 	=> fixed_saturate,
										round_style		=> fixed_truncate  
								));
			end if;
		end if;
	end if;
end process;



end;