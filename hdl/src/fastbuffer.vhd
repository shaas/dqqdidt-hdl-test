LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
--------------------------------------------------------------------------------
-- Company: CERN
--
-- File: fastbuffer
-- File history:
--      v0.1    20/07/2009      first try, works
--		v0.2	07/08/2009		supports 3k memory
--      v0.3	28/08/2009		added debug and status flags TESTED
--		v0.4 	12/10/2009		added ONLY comments for better readability
--		v0.5	12/10/2009		changed ram geometry to 4096x12
--		v0.6	20/10/2009		ram utilisation can be configured in the constants 
--								file, sampling speed of buffer as well
--		v0.7	02/12/2009		added rounding of buffer values to improve precision
--		v0.8	27/04/2010		Supports "auto rearm" when buffer is completely read
--		v0.9	23/09/2010		changed buffer data type to signed(23 downto 0) 
--								changed ram to 3072x16
--		v1.0	30/09/2010		Changed flagging (switch off postsampling/running flag 
--								while waiting for readout
--		v1.1    12/07/2011		Start marker set to neg full scale, end marker set to pos full scale
--		v1.2	28/07/2011		Multiple start markers (to avoid missing markers)
--		v1.3	01/08/2011		Supports number of skipped samples in buffer control word
--		v1.31	11/10/2011		Skipsamples extended to 5 bit
--		v1.5	10/25/2011		24-bit implementation using 2048x24 memory
--		v1.6	06/03/2013		With parametrised by generic, still memory has to be adapted by hand
--		v1.7	
-- Description: 
--
--!@brief Implements a circular buffer which records the last n adc samples
-- can be read out with a special version of the logging command
--
-- Targeted device: A3PE1500
-- Author: Jens Steckert
--
--------------------------------------------------------------------------------

entity fastbuffer is
generic(d_width :integer range 3 to 32 := 16);
port (clk			:in std_logic;
	rst		 	:in std_logic;
	sampleclock :in std_logic;
	rclock		:in std_logic;
	Data_in		:in std_logic_vector(d_width -1 downto 0);
	spulse		:in std_logic;
	restart		:in std_logic;
	skipsamples :in std_logic_vector(4 downto 0);
	Data_out	:out std_logic_vector(d_width -1 downto 0);
	debug		:out std_logic_vector(1 downto 0));
end fastbuffer;

architecture fb_arch of fastbuffer is

attribute syn_radhardlevel : string;
attribute syn_radhardlevel of fb_arch: architecture is "none";
constant fb_init : std_logic_vector(d_width-1 downto 0) := '1' & std_logic_vector(to_unsigned(0,d_width -1));
constant fb_n: natural := 14;
constant fb_maxadr : unsigned(13 downto 0) := "11" & x"fFF"; --4095
constant fb_postsamplecounts: natural := 8192; 
constant fb_skipsamples : unsigned(4 downto 0) := "00000"; --skip no samples, fbuf = fs
----ram signals
signal wreg 	:std_logic_vector(d_width-1 downto 0);
signal rreg 	:std_logic_vector(d_width-1 downto 0);

signal wadr		:std_logic_vector(fb_n-1 downto 0);
signal radr 		:std_logic_vector(fb_n-1 downto 0);
signal wadrcount : unsigned(fb_n-1 downto 0);
signal radrcount : unsigned(fb_n-1 downto 0);
signal flag_running: std_logic;
--signal flag_triggered: std_logic;
--signal flag_postsampling: std_logic;
signal flag_waitreadout : std_logic;
signal clk_i 		:std_logic;
signal renable	:std_logic;
signal wenable	:std_logic;
signal rst_i		:std_logic;
signal buffer_flags : std_logic_vector(1 downto 0);
signal flag_rdata_valid : std_logic;
---normal registers
signal newval 	:std_logic_vector(d_width -1 downto 0);
signal maxadr : unsigned(fb_n-1 downto 0);
signal postsamplecounts : integer range 0 to 16384;
signal sp :std_logic;
signal spulse_buf : std_logic;
signal repulse_buf :std_logic;
signal rep : std_logic;
signal postsamplecounter : integer range 0 to 16384;
signal rp : std_logic;
signal rwi : std_logic;
--signal flagps : std_logic;
signal clkreg : std_logic;
signal clkreg_buf : std_logic;
signal rpulse_buf : std_logic;
signal endreadflag : std_logic;
signal startreadcount : integer range 0 to 7;
signal newsample : std_logic;
signal newsample_prim : std_logic;
signal skip : unsigned(4 downto 0);
signal fb_autorearm : std_logic;
signal autorearm_buf : std_logic;
signal endmarkercount : unsigned(1 downto 0);

type state_ram is (idle, prepwrite, writeram, prepread, readram, readram2);
type state_control is (sidle, setcount,waitsample, count, endcount, waitrestart);
signal scontrol : state_control;
signal stateram : state_ram;

type MEMORY_sizexw is array (0 to to_integer(fb_maxadr)) of std_logic_vector(d_width-1 downto 0); 
signal MEM: MEMORY_sizexw;
attribute syn_ramstyle : string;
attribute syn_ramstyle of MEM : signal is "no_rw_check";

begin
maxadr <= fb_maxadr;
postsamplecounts <= fb_postsamplecounts;
clk_i <= clk;
rst_i <= rst;
--debug(0) <= flag_running;
--debug(1) <= flag_waitreadout;
debug <= buffer_flags;
clkreg <= sampleclock;

--listens on the sample clock, creates a newsample pulse once a new value comes from the adc
wpulsedetect: process(clk_i)
begin
if (clk_i'event and clk_i='1') then 
	if rst = '1' then
		newsample_prim <= '0';
		newval <= (others => '0');
		clkreg_buf <= '0';
	else
		clkreg_buf <= clkreg;					--sample clock, detect edge
		if clkreg_buf < clkreg then				--if positive edge detected
			newsample_prim <= '1';				--flag a new sample
			newval <= std_logic_vector(Data_in);--get new sample from input and reduce width to 23 bit
		else
			newsample_prim <= '0'; 
		end if;
	end if;
end if;
end process;

--skips every fb_skipsample sample --> variable data rate to be defined in SymQ_constants.vhd
--New in version 1.3, skipsamples can be programmed !
newsampleskip : process(clk_i)
begin
if (clk_i'event and clk_i = '1') then 
	if rst = '1' then
		skip <= (others => '0');
		newsample <= '0';
	else
		if newsample_prim = '1' then --new sample coming from adc
			if skip = 0 then
				newsample <= '1';
				skip <= unsigned(skipsamples);
			else --skip is not 0, sample will be skipped, skip decremented
				skip <= skip - 1;
				newsample <= '0';
			end if;
		else
			newsample <= '0';
		end if;
	end if;
end if;
end process;

--read pulse
rpulsedetect: process(clk_i)
begin
if (clk_i'event and clk_i='1') then 
	if rst = '1' then
		rp <= '0';
		rpulse_buf <= '0';
	else
		rpulse_buf <= rclock;
		if rpulse_buf < rclock then
			rp <= '1';
		else
			rp <= '0';
		end if;
	end if;end if;
end process;

--trigger
spulsedetect: process(clk_i)
begin
if (clk_i'event and clk_i='1') then 
	if rst = '1' then
		sp <= '0';
		spulse_buf <= '0';
	else
		spulse_buf <= spulse;
		if spulse_buf < spulse then
			sp <= '1';
		else
			sp <= '0';
		end if;
	end if;end if;
end process;

--detect rearm pulse 
--handles the autorearm as well
repulsedetect: process(clk_i)
begin
if (clk_i'event and clk_i='1') then 
	if rst = '1' then
		rep <= '0';
		repulse_buf <= '0';
		autorearm_buf <= '0';
	else
		repulse_buf <= restart;
		autorearm_buf <= fb_autorearm;
		if repulse_buf < restart OR autorearm_buf < fb_autorearm then
			rep <= '1';
		else
			rep <= '0';
		end if;
	end if;end if;
end process;

--controls the buffer operation and handles the flagging
fsm_control: process(clk_i)
begin
if(clk_i'event and clk_i = '1' ) then 
	if rst = '1' then
		scontrol <= sidle;
--		flagps <= '0';
		rwi <= '0';
		postsamplecounter <= 0;
--		flag_triggered <= '0';
--		flag_postsampling <= '0';
		flag_waitreadout <= '0';
		flag_running <= '0';
		buffer_flags <= "00";
	else
		case scontrol is
			when sidle =>
				flag_waitreadout <= '0';
				flag_running <= '1';
				buffer_flags <= "00";
				--flag_postsampling <= '0';
				--flag_triggered <= '0';
				if sp = '1' then			--if trigger is detected
					scontrol <= setcount;	
				else	
					scontrol <= sidle;
				end if;
			
			when setcount =>				--sets the post trigger counter
				postsamplecounter <= fb_postsamplecounts;
				buffer_flags <= "01";
				scontrol <= count;
				--flag_triggered <= '1';
			
			when waitsample =>			--waits for sample
				if rp = '1' then
					scontrol <= count;
				else
					scontrol <= waitsample;
				end if;
--				flagps <= '1';
			when count =>  				--handles the post trigger counter each new sample decrements
				buffer_flags <= "01";
				if postsamplecounter > 0 then
					--flag_postsampling <= '1';
					if newsample = '1' then
						postsamplecounter <= postsamplecounter -1;
					else
						postsamplecounter <= postsamplecounter;
					end if;
					scontrol <= count;
				else
					--flag_postsampling <= '0';
					scontrol <= endcount;
				end if;
				
			when endcount =>		--stops the recording
				rwi <= '1'; --set read/write flag to 1 (read mode) 
--				flagps <= '0';
				--flag_postsampling <= '0';
				buffer_flags <= "10";
				scontrol <= waitrestart;
				
			when waitrestart =>	--waits for restart
				flag_waitreadout <= '1';
				flag_running <= '0';
				if flag_rdata_valid = '1' then
					buffer_flags <= "11";
				else
					buffer_flags <= "10";
				end if;
				--flag_postsampling <= '0';
				if rep = '1' then   	--if rearm pulse detected
					rwi <= '0';			--ram is set to write mode
					scontrol <= sidle;
				else
					scontrol <= waitrestart;
				end if;
			when others =>
				scontrol <= sidle;
		end case;
	end if;end if;
end process;

--Handles the basic memory routines (read from and writes to the RAM)
fsm_mem: process(clk_i)
begin
if (clk_i'event and clk_i = '1') then 
	if rst = '1' then
		stateram <= idle;
		radrcount <= (others => '0');
		wadrcount <= (others => '0');
		radr <= (others => '0');
		wadr <= (others => '0');
		renable <= '0';
		wenable <= '0';
		endreadflag <= '0';
		startreadcount <= 7;
		wreg <= fb_init;
		Data_out <= fb_init;
		fb_autorearm <= '0';
		endmarkercount <= (others => '0');
		flag_rdata_valid <= '0';
		
	else
		case stateram is
			when idle =>
				wenable <= '0';
				
				if rwi = '0' then	--rwi is 0 --> write mode
					if newsample = '1' then
						stateram <= writeram;
					else
						stateram <= idle;
					end if;
					
				else				--rwi is 1 --> read mode
				-----------------read mode----------------------
				fb_autorearm <= '0';
					if rp = '1' then
						stateram <= prepread;
					else
						stateram <= idle;
					end if;
				end if;
				
			when prepwrite =>
				if wadrcount < maxadr then
					wadrcount <= wadrcount + 1;
				else
					wadrcount <= (others => '0');
				end if;
				flag_rdata_valid <= '0';
				stateram <= idle;
			when writeram =>
				--calculate readpointer here (less confusing)
				if (wadrcount) < maxadr then 
						radrcount <= wadrcount +1;
				else --e,f
					radrcount <= (others => '0');
				end if;	
				--this will cause to send the startmarker to be sent n times after first read
				--startreadcount <= 4; --set startreadcount
				--new settings: no startmarker
				startreadcount <= 0;
				wadr <= std_logic_vector(wadrcount);
				wreg <= newval;
				--ramcell is written
				wenable <= '1';
				flag_rdata_valid <= '0';
				stateram <= prepwrite;
			when prepread =>
				if radrcount < maxadr then
					if (radrcount + 1) = wadrcount then --last sample
						radrcount <= radrcount;
						endreadflag <= '1';
					else
						radrcount <= radrcount + 1; --normal
						endreadflag <= '0';
					end if;
				else
					if wadrcount = x"000" then
						radrcount <= radrcount;
						endreadflag <= '1';
					else
						radrcount <= (others => '0'); --wrap-a-round
						endreadflag <= '0';
					end if;
					
				end if;
				stateram <= readram;
				
			when readram =>
				radr <= std_logic_vector(radrcount);
				renable <= '1';
				stateram <= readram2;
			when readram2 =>
				renable <= '0';
				--reading of buffer starts return neg full scale for first few samples
				if startreadcount > 0 then
					startreadcount <= startreadcount -1;
					--Data_out <= fb_init;
					Data_out <= (others => '0');
					--flag_rdata_valid <= '1';
				else
				--start marker sending is over, return buffer content
					if endreadflag = '0' then
						--12 bit code--Data_out <= rreg & "0000";  
						--blow up data by 4 bit to get 16 bits
						Data_out <= rreg;	--signed(rreg);
						flag_rdata_valid <= '1';
					else
						--Data_out <= std_logic_vector(unsigned(fb_init) - 1);
						Data_out <= (others => '1');
						flag_rdata_valid <= '0';
						--for automatic rearm at end of read (useful for non-latching soft-triggers)
						--returns 4x endmarker and rearms buffer afterwards
						if endmarkercount < "11" then
							endmarkercount <= endmarkercount + 1;
						else
							endmarkercount <= "00";
							fb_autorearm <= '1';
						end if;
					end if;
				end if;
				stateram <= idle;
			when others =>
				stateram <= idle;
				
		end case;
		
	end if;end if;
end process;
--memory (array) --> synplify will use block rams
memory: process(clk)
begin
	if rising_edge(clk) then
		if rst = '1' then
			rreg <= fb_init;
		else
			if wenable = '1' then
				MEM(to_integer(unsigned(wadr))) <= wreg;
			end if;
			if renable = '1' then
				rreg <= MEM(to_integer(unsigned(radr)));
			end if;
		end if;
	end if;
end process;

end;