library ieee;
use ieee.std_logic_1164.all;
USE IEEE.numeric_std.ALL;
--USE IEEE.fixed_pkg.all;

LIBRARY UQDSLib;
use UQDSLib.UQDSLib.ALL;

---------------------------------------------------------------------------------------------------------
-- Company: CERN
--
-- File: dynamic_threshold.vhd
-- File history:
--      0.1: 24.11.2018: first attempt
--      2.0: 29.1.2019: accepts any current (full comparator) and dyn discr added
--  
--	<Revision number>: <Date>: <Comments>
--
-- Description: 
--
--!@brief changes threshold taken from threshold table according to current measurement.
-- thresholds should be ordered from smaller currents to larger currents
--	
--
--
-- Targeted device: IGLOO150
-- Author: Daniel Blasco
--
----------------------------------------------------------------------------------------------------------
entity dynamic_threshold is
	generic(d_width      : integer range 15 to 32 := 32;
	        table_length : integer range 1 to 8   := 4
	       );
	port(
		clk, rst             : in  std_logic;
		threshold_tab        : in  regArrayCurDepSettings;
		discriminator_tab    : in  regArrayCurDepSettings;
		current_level_tab    : in  regArrayCurDepSettings;
		current              : in  std_logic_vector(d_width - 1 downto 0);
		force_last_threshold : in  std_logic;
		threshold            : out std_logic_vector(31 downto 0);
		discriminator        : out std_logic_vector(31 downto 0)
	);
end dynamic_threshold;

architecture dynamic_threshold_arch of dynamic_threshold is

	signal current_unipolar_debug : std_logic_vector(d_width - 1 downto 0);
	--signal compared_value_debug   : std_logic_vector(d_width - 1 downto 0);

begin

	process(clk)
		variable current_unipolar : std_logic_vector(d_width - 1 downto 0);
		--variable compared_value   : std_logic_vector(d_width - 1 downto 0);

	begin
		if rising_edge(clk) then

			if rst = '1' then
				current_unipolar := (others => '0');

			else
				-- Consider negative currents as positive
				if (current(current'left)) then
					current_unipolar := std_logic_vector(signed(not current) + 1);
				else
					current_unipolar := current;
				end if;

				threshold     <= threshold_tab(0);
				discriminator <= discriminator_tab(0);

				--				for i in 1 to table_length - 1 loop
				--					-- Convert the current level values to bits with 1's on the left of the power of 2 value (except for the left which is sign)
				--					-- Ex: current_level = 4  ---> compared_value = 01111111111111111111111111110000
				--					--compared_value := "0" & ((30 downto current_level(i)) => '1') & ((current_level(i) - 1 downto 0) => '0');
				--					compared_value(compared_value'left)                             := '0';
				--					compared_value(compared_value'left - 1 downto current_level(i)) := (others => '1');
				--					compared_value(current_level(i) - 1 downto 0)                   := (others => '0');
				--
				--					-- Check if current has any of the most significant bits in 1, so current is higher than the threshold
				--					if (compared_value and current_unipolar) /= x"00000000" then
				--						threshold <= threshold_tab(i); -- The last threshold which fulfills the condition will be the remaining one
				--					end if;
				--
				--				end loop;

				for i in 1 to table_length - 1 loop

					if (current_unipolar(current_unipolar'left - 1 downto 0) > current_level_tab(i)(current_level_tab(i)'left - 1 downto 0)) then -- Remove sign bit
						threshold     <= threshold_tab(i);
						discriminator <= discriminator_tab(i);
					end if;

				end loop;

				-- In case we want to force the last threshold
				if (force_last_threshold = '1') then
					threshold     <= threshold_tab(table_length - 1);
					discriminator <= discriminator_tab(table_length - 1);
				end if;

				current_unipolar_debug <= current_unipolar;
				--compared_value_debug   <= compared_value;

			end if;
		end if;

	end process;

end;
