LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;
--library RADlib;
--use RADlib.util.all;
use work.util.all;
--------------------------------------------------------------------------------
-- Company: CERN
--
-- File: meanram_fsm
-- File history:
--      v1.0    11/16/2011       Works well
--		v1.1	01/31/2012		regsum register too small for depths >5, fixed
--		v2.0	01/31/2012		read loop is accelerated by factor of two 
--		v4.0	08/29/2012		fully new implementation ram is instantiated by synplify
--      v4.1    14/02/2018      dblascos: changed d_width range up to 32 bits
-- Description: 
--					completely new implementation based on fsm (easier to handle)
--
--!@brief Implements a moving average filter of depth d
--
--	Timing: Each read takes 2 clocks. 2 * 2^depth + 5: for 2^10 = 2053 clocks
--			@ 40MHz : 52us = 19kHz
--				
--
-- Targeted device: A3PE1500
-- Author: Jens Steckert
--
--------------------------------------------------------------------------------



entity meanram_fsm is
generic(d_width : integer range 15 to 32 := 16;
		ram_depth : integer range 0 to 1024 := 128;
		realmul : boolean := false
		);
port (
	clk			:in std_logic;
	sampleclock :in std_logic;
	rst		 	:in std_logic;
    depth_bits  :in integer range 0 to 12;
    Data_in		:in std_logic_vector(d_width -1 downto 0);
	real_div	:in integer range 0 to 12;
	real_invdiv	:in std_logic_vector(d_width -1 downto 0);
	Qout		:out std_logic_vector(d_width -1 downto 0);
	dready_out : out std_logic
	);
end meanram_fsm;

architecture meanram_arch of meanram_fsm is

--attribute syn_radhardlevel : string;
--attribute syn_radhardlevel of meanram_arch: architecture is "tmr";

----ram signals
constant rd: natural := log2c(ram_depth);

signal clk_i 		:std_logic;
-- signal renable	:std_logic;
-- signal wenable	:std_logic;
signal rst_i		:std_logic;

---normal registers

signal regsum 	:signed(d_width-1 + rd downto 0);

signal samplepulse : std_logic;
signal oldsample :std_logic;

signal wpointer :integer range 0 to ram_depth-1;
signal rpointer :integer range 0 to ram_depth-1;
signal mem_max : integer range 0 to ram_depth-1;
signal calc_mem_max : std_logic_vector(12 downto 0);
signal divshift : integer range 0 to 12;
signal rreg0	: std_logic_vector(d_width -1 downto 0);
signal rreg1	: std_logic_vector(d_width -1 downto 0);
signal rreg2	: std_logic_vector(d_width -1 downto 0);
signal rreg_tmr : std_logic_vector(d_width -1 downto 0);

--type ram_array is array(0 to ram_depth) of std_logic_vector(d_width -1 downto 0);
type ram_array is array(0 to ram_depth -1) of std_logic_vector(d_width -1 downto 0);
signal ram0 : ram_array;
signal ram1 : ram_array;
signal ram2 : ram_array;

attribute syn_ramstyle : string;
-- attribute syn_ramstyle of ram0 : signal is "block_ram";
-- attribute syn_ramstyle of ram1 : signal is "block_ram";
-- attribute syn_ramstyle of ram2 : signal is "block_ram";
attribute syn_ramstyle of ram0 : signal is "no_rw_check";
attribute syn_ramstyle of ram1 : signal is "no_rw_check";
attribute syn_ramstyle of ram2 : signal is "no_rw_check";

attribute syn_preserve : boolean;
attribute syn_preserve of ram0: signal is true;
attribute syn_preserve of ram1: signal is true;
attribute syn_preserve of ram2: signal is true;



type state_type is ( idle, new_sample, check_wadr, wait_read, read_ram, sum_up, 
						calcmean, dec_adr, rr, calc_tmr,  setup);

signal state : state_type;

begin

clk_i <= clk;
rst_i <= rst;

divshift <= depth_bits;

--renable <= '1';
--wenable <= '1';
spulse:process(clk_i)
begin
if rising_edge(clk_i) then 
	if rst = '1' then
		samplepulse <= '0';
		oldsample <= '0';
	else
		oldsample <= sampleclock;
		if oldsample < sampleclock then
			samplepulse <= '1';
		else
			samplepulse <= '0';
		end if;
	end if;
end if;
end process;


fsm: process(clk_i)
begin
if rising_edge(clk_i) then 
	if rst = '1' then
		wpointer <= 0;
		rpointer <= 0;
		--wenable <= '0';
		regsum <= (others => '0');
		calc_mem_max <= (others => '0');
		state <= idle;
		Qout <= (others => '0');
		mem_max <= 0;
		dready_out <= '0';
	else
		case state is
			when idle =>
				if samplepulse = '1' then
					state <= setup;
				else
					state <= idle;
				end if;
				calc_mem_max <= (others => '0');
				dready_out <= '0';
				
			when setup =>
				calc_mem_max(depth_bits) <= '1'; ---for 7 --> 128
				--calc_mem_max <= "001000000";
				state <= new_sample;
				
			when new_sample =>
				--set adresses and sample
				ram0(wpointer) <= Data_in;
				ram1(wpointer) <= Data_in;
				ram2(wpointer) <= Data_in;
				--calculate maximum RAM address
				mem_max <= to_integer(unsigned(calc_mem_max)-1 );
				--mem_max <= 127;
				state <= check_wadr;
			when check_wadr =>
				--write it into ram
				if wpointer >= (mem_max) then
					wpointer <= 0;
				else
					wpointer <= wpointer + 1;
				end if;
				rpointer <= mem_max;
				regsum <= (others => '0');
				state <= rr;
			
			when rr =>
				rreg0 <= ram0(rpointer);
				rreg1 <= ram1(rpointer);
				rreg2 <= ram2(rpointer);
				--state <= sum_up;
				state <= calc_tmr;
			when calc_tmr =>
				rreg_tmr <= (rreg0 AND rreg1) OR (rreg0 AND rreg2) OR (rreg1 AND rreg2);
				state <= sum_up;
			when sum_up =>
				--now take value from read register and add it to the sum !
				--majority voting between the three ram blocks
				--regsum <= regsum + signed((rreg0 AND rreg1) OR (rreg0 AND rreg2) OR (rreg1 AND rreg2));
				regsum <= regsum + signed(rreg_tmr);
				--decrease address here, saves one clock cycle
				if rpointer > 0 then
					rpointer <= rpointer -1;
					state <= rr;
				else
					state <= calcmean;
				end if;	
			when calcmean =>
				--when all is summed up, calculate mean !
				Qout <= std_logic_vector(shift_right(regsum, divshift)(d_width-1 downto 0));
				dready_out <= '1';
				state <= idle;
			when others =>
				state <= idle;
		end case;
	end if;
end if;
end process;
		



end;