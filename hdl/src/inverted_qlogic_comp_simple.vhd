LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
use ieee.numeric_std.all;
LIBRARY UQDSLib;
use UQDSLib.UQDSLib.ALL;

---------------------------------------------------------------------------------------------------------
--! Company: CERN
--
--! @File: qlogic.vhd
-- File history:
--		unknown
--		v0.1	03/10/2017	second version for DLQ
--		v0.2	05/18/2017  now supports 16-bit threshold 
--!@brief 				Performs the DLQ quench detection logic 
-- Description: 	
--			If input signal exceeds threshold, set quench signal high and latch
--
--
-- Targeted device: Actel ProAsic3 A3PE1500 PQ208
--! @Author: Jens Steckert (CERN), Andrzej Skoczen (AGH-UST)
--
----------------------------------------------------------------------------------------------------------
entity inverted_qlogic_comp_simple is
generic(d_width : integer range 15 to 33 := 32);
port (
    clk : 		in std_logic;
    rst : 		in std_logic;
    U_in : 		in std_logic_vector(d_width-1 downto 0);
	T_in : 		in std_logic_vector(d_width -1 downto 0);
	disc : 	in std_logic_vector(15 downto 0);
	L_out : 	out std_logic_vector(3 downto 0)
);
end entity inverted_qlogic_comp_simple;

architecture logic_arch of inverted_qlogic_comp_simple is
--component discriminator is
--generic(freq_factor : unsigned(14 downto 0);
--	disc_time_width : natural
--);
--port (clk, rst, enable, lsig : in std_logic;
--    maxval : in unsigned(7 downto 0);
--    gate : out std_logic);
--end component;


signal discenable: std_logic;
signal quench : std_logic;
signal gquench :std_logic;
signal syncquench		:std_logic;
signal latchquench		:std_logic;
signal gater		:std_logic;
signal disc_i		:unsigned(15 downto 0);
signal ures_int		: unsigned(d_width-1 downto 0);
signal result		: signed(d_width downto 0);

constant res_ext : natural := result'length - T_in'length;

begin
	
discenable <= '0' when disc = x"0000" else '1';
disc_i <=unsigned(disc(15 downto 0));




--do all necessary stuff in one process, easier to follow
quenchcalc: process(clk)
begin
if (rising_edge(clk)) then
	if rst = '1' then
		ures_int <= (others => '0');  	--1 clock
		result <= (others => '0');		--2 clocks
		quench <= '0';					--3 clocks
		syncquench <= '0';				--4
		gquench <= '0';					--5
		latchquench <= '0';				--6
		
	else
		--clip to 16 bit and take absolute value
		if U_in (d_width -1) = '0' then --positive
			
			ures_int <= '0' & unsigned(U_in((d_width- 2) downto 0)); --take value without sign
		else --negative
		
			ures_int <= '0' & unsigned(not U_in((d_width- 2) downto 0)) +1; --take 16msb without sign and invert
		end if;
		
		--calc difference between ures and threshold (zero pad difference in length between result & threshold)
		--result <= signed( signed("0" & ures_int) + signed( ((result'length - T_in'length) => '0') & T_in));
		result <= signed( (res_ext => '0') & ures_int) - signed( (res_ext => '0') & unsigned(T_in));
		
		--quench detection
		if result < 0 then
			quench <= '1';
			
		else
			
			quench <= '0';
		end if;
		
		--introduce one clock delay for discriminator
		syncquench <= quench; 
		
		
		--if discriminator is run out, propagate quench
		gquench <= syncquench AND gater;
		
		if gquench = '1' then
			--conseccount <= conseccount_buf2;
			latchquench <= '1';
		--else
		--	latchquench <= latchquench;
		end if;
		
	end if;
end if;
end process;

discrim: discriminator
	generic map(
		freq_factor => "000"& x"FA0", -- 4.000 ticks @ 40MHz = 100us
		disc_time_width => 16
	)
	port map(   
		clk=> clk, 
		rst => rst, 
		enable => discenable,
		lsig => quench, 
		maxval => disc_i, 
		gate => gater
	);

--output <= tflags & latchquench ;
L_out <= "00" & syncquench & latchquench ;
end; 
