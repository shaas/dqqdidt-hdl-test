library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
USE IEEE.fixed_pkg.all;
LIBRARY UQDSLib;
use UQDSLib.UQDSLib.ALL;
use UQDSLib.config_block_lib.all;
---------------------------------------------------------------------------------------------------------
-- Company: CERN
--
-- File: detection_block_comp_L_didt.vhd
-- File history:
--      0.1: 23.08.2018: first attempt, 
--      
--  
--	<Revision number>: <Date>: <Comments>
--
-- Description: 
--
--!@brief 	implements a quench detection block with inductive compensation on base of 
--			L * didt algorithm
--	
--			Contains 2x channel generic which are controlled via chgen record types
--			
--			Udiff ----------------------------------------> + -----> chgenUres ---> qlogic --> interlocks
--															^
--															|
--															|
--														chgenUind
--															^
--															|
--															|
--			I_mag ------> differentiator ----> |mul_L_didt -/
--			Inductance ----------------------> |
--
--
-- Targeted device: Microsemi ProAsic3, IGLOO2 (doesn't matter, generic VHDL)
-- Author: Jens Steckert
--
----------------------------------------------------------------------------------------------------------
entity detection_block_comp_L_didt is
generic(d_width : integer range 15 to 32 := 32;
		d_width_L : integer range 7 to 32 := 16;
		differentiator_arr_depth : integer range 7 to 65536 
		);
	port(
		clk 			: in std_logic;
		rst 			: in std_logic;
		dready			: in std_logic;
		chgenUind_params: in channel_generic_settings;
		chgenUres_params: in channel_generic_settings;
		
		diff_dt			: in std_logic_vector(d_width_L downto 0);
		diff_nms		: in std_logic_vector(7 downto 0);
		Udiff 			: in std_logic_vector(d_width -1 downto 0);	--voltage across magnet
		I_mag 			: in std_logic_vector(d_width -1 downto 0);
		Inductance		: in std_logic_vector(d_width_L-1 downto 0);
		threshold		: in std_logic_vector(d_width -1 downto 0);
		discrimination	: in std_logic_vector(15 downto 0);
		Uind			: out std_logic_vector(d_width -1 downto 0);
		Uind_diag		: out std_logic_vector(d_width -1 downto 0);
		Ures			: out std_logic_vector(d_width -1 downto 0);
		Ures_diag		: out std_logic_vector(d_width -1 downto 0);
		Logic_out		: out std_logic_vector(3 downto 0) 
	);
end entity detection_block_comp_L_didt;

architecture RTL of detection_block_comp_L_didt is
	
	
	signal didt					: std_logic_vector(d_width -1 downto 0);
	signal diff_complete		: std_logic;
	signal sfUres_raw			: sf_voltage;
	signal Ures_raw				: std_logic_vector(d_width -1 downto 0);
	signal Uind_i				: std_logic_vector(d_width -1 downto 0);
	signal Uind_filt			: std_logic_vector(d_width -1 downto 0);
	signal sfUind				: sf_voltage;
	signal sfUdiff				: sf_voltage;
	
	
begin

differentiator: differentiator_synram
	generic map(
		arr_depth => differentiator_arr_depth,
		d_width   => d_width
	)
	port map(
		clk                => clk,
		rst                => rst,
		din                => I_mag,
		dready             => dready,
		dt                 => diff_dt,
		normalizing_shifts => diff_nms,
		dout               => didt,
		done               => diff_complete
	);

mulLdidt: mul_L_didt
	generic map(
		d_width   => d_width,
		d_width_L => d_width_L
	)
	port map(
		clk        => clk,
		rst        => rst,
		didt       => didt,
		L          => Inductance,
		start_mult => diff_complete,
		dout       => Uind_i
	);
	
filterblock_Uind: channel_generic
	generic map(
		d_width        => data_path_width,
		b_width        => 32,
		mavg_ram_depth => 256,
		cor_c_width    => cor_c_width
	)
	port map(
		clk             => clk,
		rst             => rst,
		sampleclock     => dready,			--let's see what to do here
		Data_in         => Uind_i,
		autozero_start  => '0',					--do not support autozero here
		mux_data        => chgenUind_params.mux_data,
		mux_daq         => chgenUind_params.mux_daq,
		mavg_depth		 => chgenUind_params.flt_mov_avg_depth_bits,
		median_depth    => chgenUind_params.flt_median_depth,
		median_middle   => chgenUind_params.flt_median_middle,
		cor_gain        => chgenUind_params.cor_gain,
		cor_offset      => chgenUind_params.cor_offset,
		cor_offset_auto => open,				--do not support autozero
		Dmux_data_out   => Uind_filt,
		Bmux_data_out   => Uind_diag,
		d_ready_out     => open,
		debug           => open
	);		

--Add Udiff and Ures

--put to port
Uind <= Uind_filt;

--convert Uind to sfixed
sfUind <= to_sfixed(Uind_filt, sfUind);

--convert Udiff to sfixed
sfUdiff <= to_sfixed(Udiff, sfUdiff);

--add and coerce to range --> SIMULATE
sfUres_raw <= resize(sfUind + sfUdiff, sfUres_raw);

--std logic vector representation 
Ures_raw <= std_logic_vector(sfUres_raw);



filterblock_ures: channel_generic
	generic map(
		d_width        => data_path_width,
		b_width        => 32,
		mavg_ram_depth => 256,
		cor_c_width    => cor_c_width
	)
	port map(
		clk             => clk,
		rst             => rst,
		sampleclock     => dready,			--let's see what to do here
		Data_in         => Ures_raw,
		autozero_start  => '0',					--do not support autozero here
		mux_data        => chgenUres_params.mux_data,
		mux_daq         => chgenUres_params.mux_daq,
		mavg_depth		 => chgenUres_params.flt_mov_avg_depth_bits,
		median_depth    => chgenUres_params.flt_median_depth,
		median_middle   => chgenUres_params.flt_median_middle,
		cor_gain        => chgenUres_params.cor_gain,
		cor_offset      => chgenUres_params.cor_offset,
		cor_offset_auto => open,				--do not support autozero
		Dmux_data_out   => Ures,
		Bmux_data_out   => Ures_diag,
		d_ready_out     => open,
		debug           => open
	);
	

QDLogic: qlogic_comp_simple
	generic map(
		d_width => d_width
	)
	port map(
		clk   => clk,
		rst   => rst,
		U_in  => Ures,
		T_in  => threshold,
		disc  => discrimination,
		L_out => Logic_out
	);

end architecture RTL;