--------------------------------------------------------------------------------
--! Company: CERN
--!
--! @File: TopProto.vhd
-- File history:
--      v0.1	12/04/2017	first version for uQDS 2.0
--		v0.2	18/01/2018	basic register map and flash interface started
--		v0.3	23/01/2018	flash interface including autoconfigure works
--		v0.4	13/02/2018	added SPI in register config. added RAM modules (with test processes)
--      
--! @brief basic command interface register 
-- Description: 
--				
--
-- Targeted device: M2GL150
--! @Author: Jens Steckert, Dani Blasco, Jelena Spasic
--
--------------------------------------------------------------------------------
library IEEE;
use ieee.numeric_std.all;
use IEEE.std_logic_1164.all;
use IEEE.MATH_REAL.ALL;

LIBRARY UQDSLib;
use UQDSLib.UQDSLib.ALL;
use UQDSLib.buildstamp.all;
use UQDSLib.reg_map_pkg.all;
use UQDSLib.config_block_lib.all;

entity Top is

	port(
		CLK            : IN    std_logic; --main clock
		PRST           : IN    std_logic; -- power on reset
		RESET          : IN    std_logic; -- Reset from crate
		
		DEBUG_LED      : OUT   std_logic_vector(7 downto 0);
		DEBUG_PORT     : OUT   std_logic_vector(17 downto 0);
		
		-- Dedicated interlocks
		--INTERLOCK      : OUT   std_logic_vector(1 downto 0);
		FPGA_HEATER_TRIGGER : OUT std_logic;
		FPGA_POWER_TRIGGER	: OUT std_logic;
		
		-- Aux port
		AUXPORT_DIO    : INOUT std_logic_vector(17 downto 0);
		
		-- Sync lines
		SYNC_IN        : IN    std_logic_vector(1 downto 0);
		SYNC_OUT       : OUT   std_logic_vector(1 downto 0);
		SYNC_EXT_IN    : IN    std_logic_vector(1 downto 0);
		
		--Connections to the FLASH_1 flash memory
		FLASH_HOLD_A  : OUT   std_logic;
		FLASH_WP_A    : OUT   std_logic;
		FLASH_SO_A     : IN    std_logic;
		FLASH_SI_A     : OUT   std_logic;
		FLASH_CLK_A    : OUT   std_logic;
		FLASH_CS_A    : OUT   std_logic;
		
		-- ADC connections
		ADC_CNV_FPGA  : OUT   std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
		ADC_BUSY_FPGA : IN    std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
		ADC_SCK_FPGA  : OUT   std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
		ADC_SDO_FPGA  : IN    std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
		--PORT_PSPI_MOSI : OUT   std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
		--PORT_PSPI_CS0  : OUT   std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
		--PORT_PSPI_CS1  : OUT   std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
		--PORT_PSPI_SCK  : OUT   std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
		PORT_AUX_LINE0 : OUT   std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
		PORT_AUX_LINE1 : OUT   std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
		
		-- Connected to FTDI module A serving 245FIFO mode
		--FTDI_A_CNT     : INOUT std_logic_vector(6 downto 0);
		--FTDI_A_D       : OUT   std_logic_vector(7 downto 0);
		
		-- Connected to FTDI module B (FT232 pin 13 & 14) serving as RX and TX in RS232 mode
		FTDI_B_D       : INOUT std_logic_vector(1 downto 0);
		-- RS485
		--RS485_A_RI     : OUT   std_logic;
		--RS485_A_RO     : IN    std_logic;
		
		-- SPI connections
		SPI_MISO_FPGA 	: out std_logic;
		SPI_MOSI_FPGA 	: in std_logic;
		SPI_nCS_FPGA 	: in std_logic;
		SPI_SCK_FPGA 	: in std_logic;
		BUFFER_CH1_nOE	: out std_logic
	);
end Top;

architecture rtl of Top is

	-- Triplicate registers (RAM should be triplicated manually)
	--	attribute syn_radhardlevel : string;
	--	attribute syn_radhardlevel of rtl : architecture is rad_control;

	-- Global I/O registers enabled
	attribute syn_useioff : boolean;
	attribute syn_useioff of rtl : architecture is true;

	--=========================================================================

	--========== Alias for generic pins used for specific tasks ===============
	
	-- Connected to FTDI module B (FT232 pin 13 & 14) serving as RX and TX in RS232 mode
	alias UART_FTDIB_RX : std_logic is FTDI_B_D(0);
	alias UART_FTDIB_TX : std_logic is FTDI_B_D(1);


	--=========================================================================

	--========================= Signal declaration ============================

	-- Miscellaneous
	signal blink_led_counter : unsigned(23 downto 0);
	signal autozero_start    : std_logic_vector(31 downto 0);

	-- Reset signals
	signal rst       : std_logic;
	--signal nrst      : std_logic;
	signal RST_FF1   : std_logic;       -- Reset synchronizer
	signal RST_FF2   : std_logic;
	signal RST_FF3   : std_logic;
	signal com_reset : STD_LOGIC := '0';

	-- Register control signals
	signal comRegAddress       : std_logic_vector(REGISTER_ADR_WIDTH - 1 downto 0);
	signal comRegAddress_Flash : std_logic_vector(REGISTER_ADR_WIDTH - 1 downto 0);
	signal comRegAddress_Mux   : std_logic_vector(REGISTER_ADR_WIDTH - 1 downto 0);

	signal comRegDataIn       : std_logic_vector(REGISTER_SIZE - 1 downto 0);
	signal comRegDataIn_Flash : std_logic_vector(REGISTER_SIZE - 1 downto 0);
	signal comRegDataOut      : std_logic_vector(REGISTER_SIZE - 1 downto 0);

	signal comRegMk : regArray;

	signal comWEN       : std_logic_vector(2**REGISTER_ADR_WIDTH - 1 downto 0);
	signal comWEN_Flash : std_logic_vector(2**REGISTER_ADR_WIDTH - 1 downto 0);
	signal comWEN_Mux   : std_logic_vector(2**REGISTER_ADR_WIDTH - 1 downto 0);

	-- Logic signals
	signal system_status       : std_logic_vector(REGISTER_SIZE - 1 downto 0);
	signal TriggerLines        : std_logic_vector(TRIGGER_LINES_MAX - 1 downto 0);
	signal TriggerPM           : std_logic;
	signal sync_signals        : std_logic_vector(5 downto 0);
	signal sync_out_pulse_init : std_logic;

	-- Flash control signals
	signal Flash_AT25_inputs : AT_flash_outputs;
	signal Flash_AT25_output : std_logic;
	signal mux_Flash         : std_logic;

	signal cmd_ConfigFlash  : std_logic_vector(3 downto 0);
	signal cmd_FlashBlock   : std_logic_vector(3 downto 0);
	signal cmd_comRegFreeze : std_logic;

	-- Channel pipeline signals
	signal GlobalHeartBeatReg     : std_logic_vector(31 downto 0);
	signal adc_clock_counter      : integer range 0 to 65535;
	signal adc_start_conv         : std_logic := '0';
	signal adc_clock_sampl_period : integer range 0 to 65535;
	type dataOutArray is array (CHANNELS_IN_MAX - 1 downto 0) of std_logic_vector(19 downto 0);
	signal adc_data               : dataOutArray;
	signal adc_data_ready         : std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
	signal scaled_data            : channelsArray(CHANNELS_IN_MAX - 1 downto 0);
	signal scaled_data_rdy        : std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
	signal adc_health             : std_logic_vector(REGISTER_SIZE - 1 downto 0);
	signal adc_err_constant		: std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
	signal adc_err_stuck		: std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
	-- PGA and scaler control signals
	type PGAConfigArray is array (CHANNELS_IN_MAX - 1 downto 0) of std_logic_vector(3 downto 0);
	type ScalerArray is array (CHANNELS_IN_MAX - 1 downto 0) of std_logic_vector(19 downto 0);
	signal adc_pga_ch                 : PGAConfigArray;
	signal channel_transducer_factor  : PGAConfigArray;
	signal channel_resistor_divider   : PGAConfigArray;
	signal channel_direct_scaler_mode : std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
	signal channel_direct_scaler      : ScalerArray;
	signal scaler_overrange			  : std_logic_vector(REGISTER_SIZE- 1 downto 0);
	signal scaler_input_overrange		: std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
	signal scaler_output_overrange		: std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
	signal channel_mask		   : std_logic_vector(REGISTER_SIZE- 1 downto 0);
	signal ref_relay                  : std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
	signal sr_cs                      : std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
	signal sr_data_in                 : std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
	signal sr_clk                     : std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
	signal sclk                       : std_logic;
	signal pga_counter                : unsigned(7 downto 0);
	signal slow_clock                 : std_logic;
	
	--crc checker signals
	signal waitcount : integer range 0 to 4000000;
	type st_crc_type is (IDLE, STARTUP, ACTIVE);
	signal st_crc	: st_crc_type;
	signal crc_fail_cnt : unsigned(29 downto 0);
	signal config_fail : std_logic;
	signal crc_chk_en	: std_logic;
	
	signal cntval	: unsigned(15 downto 0);
	
	-- Buffer control signals
	signal cmd_buffer_trigger : std_logic;
	signal nextByteBuffer     : std_logic;
	signal decimated_enabled  : std_logic;
	signal buffer_cmd         : std_logic_vector(7 downto 0) := (others => '0');
	signal buffer_status      : std_logic_vector(7 downto 0);
	signal trigger_postmortem : std_logic := '0';
	signal reg_refresh_buffer : integer range 0 to 255;
	signal BufferMUXData      : outputChannelsArray;
	signal BufferMUXDataM     : ChannelsArray(31 downto 0);
	signal BufferMUXDataRdy   : std_logic;
	signal BufferNoDecimate   : std_logic_vector(31 downto 0);
	signal ChannelBufferM     : ChannelsArray(31 downto 0);

	-- Communication control signals
	signal usb_split_data     : data8Array(CHANNELS_OUT_FR * 4 - 1 downto 0);
	signal FReadoutMUXData    : outputChannelsArray;
	signal FReadoutMUXDataRdy : std_logic;


	signal config_block_num_and_board_ID : std_logic_vector(31 downto 0);

	--=========================================================================


begin
	config_block_num_and_board_ID(23 downto 0) <= x"0CAFFE";

	--===================== Custom configuration block ========================

	config_block_inst : config_block
		port map(
			clk                      => CLK,
			rst                      => rst,
			ChannelsScaledData       => scaled_data,
			ChannelsScaledDataRdy    => scaled_data_rdy,
			ADC_Health               => adc_health,
			ADC_overRange			 => scaler_overrange,
			Channel_Mask		 	 => channel_mask,
			ConfigBlockReg           => ConfigCustomRegister,
			AutozeroStart            => autozero_start,
			ReadoutConfig            => readout_config,
			AdcStartConv             => adc_start_conv,
			PMBufferMask             => pm_buffer_mask,
			SyncSignals              => sync_signals,
			BufferMUXData            => BufferMUXData,
			BufferMUXDataRdy         => BufferMUXDataRdy,
			BufferNoDecimateChannels => BufferNoDecimate,
			RegMUXData               => ChannelData,
			FReadoutMUXData          => FReadoutMUXData,
			FReadoutMUXDataRdy       => FReadoutMUXDataRdy,
			TriggerLines             => TriggerLines,
			TriggerPM                => TriggerPM,
			ConfigBlockNum(7 downto 0)           => config_block_num_and_board_ID(31 downto 24)
		);

	--=========================================================================

	--============ Pipeline (ADC --> ADC_config --> Scaler) ===============
	adc_health <= adc_err_constant & adc_err_stuck;

	
	
	pipeline : for i in 0 to CHANNELS_IN_MAX - 1 generate
		channel_adc : adc_LTC2378_20
			port map(
				clk        => CLK,
				reset      => rst,
				start_conv => adc_start_conv,
				data_rdy   => adc_data_ready(i),
				data_out   => adc_data(i),
				adc_constant => adc_err_constant(i),
				adc_stuck  => adc_err_stuck(i),
				sdi        => ADC_SDO_FPGA(i),
				sck        => ADC_SCK_FPGA(i),
				busy       => ADC_BUSY_FPGA(i),
				conv       => ADC_CNV_FPGA(i)
			);

		pga_ADC : channel_controller
			port map(
				clk      => clk,
				slowclock => sclk,
				reset    => rst,
				data_in  => "0000" & ref_relay(i) & adc_pga_ch(i)(2 downto 0),
				enable   => '1',
				data_out => sr_data_in(i),
				clk_out  => sr_clk(i),
				strobe   => sr_cs(i)
			);

		PORT_AUX_LINE0(i) <= '0';
		PORT_AUX_LINE1(i) <= '0';

		--adc_pga_ch(i) <= ChannelConfig(i)(27 downto 24);
		--ref_relay(i)  <= ChannelConfig(i)(28);

		scaler_overrange <= scaler_input_overrange & scaler_output_overrange;
		
		channel_scaler_20_to_32 : scaler
			generic map(
				d_width_in     => 20,
				d_width_out    => 32,
				d_width_factor => 21)
			port map(
				clk               => CLK,
				rst               => rst,
				str               => adc_data_ready(i),
				gainsel           => adc_pga_ch(i),
				transducer_factor => channel_transducer_factor(i),
				resistor_divider  => channel_resistor_divider(i)(0 downto 0),
				scaler_value      => channel_direct_scaler(i),
				direct_mode       => channel_direct_scaler_mode(i),
				din               => adc_data(i),
				dout              => scaled_data(i),
				input_overrange   => scaler_input_overrange(i),	
				output_overrange  => scaler_output_overrange(i),
				ready             => scaled_data_rdy(i)
			);

		channel_transducer_factor(i)   <= ChannelConfig(i)(3 downto 0);
		channel_resistor_divider(i)    <= "000" & ChannelConfig(i)(4);
		channel_direct_scaler(i)       <= ChannelConfig(i)(19 downto 0);
		channel_direct_scaler_mode(i)  <= ChannelConfig(i)(20);

	end generate;

	ADC_start_conv_counter : process(CLK)
	begin
		if (rising_edge(CLK)) then
			if (rst = '1') then
				adc_clock_counter <= 1;
				adc_start_conv    <= '0';
			else
				if (adc_clock_counter >= adc_clock_sampl_period) then
					adc_start_conv    <= '1';
					adc_clock_counter <= 1;
				else
					adc_start_conv    <= '0';
					adc_clock_counter <= adc_clock_counter + 1;
				end if;
			end if;
		end if;
	end process;

	adc_clock_sampl_period <= to_integer(unsigned(GlobalHeartBeatReg(15 downto 0)));

	cnt_pga : process(CLK)
	begin
		if rising_edge(CLK) then
			if rst = '1' then
				pga_counter <= x"00";
				slow_clock  <= '0';
			else
				if pga_counter /= x"07" then
					pga_counter <= pga_counter + 1;
				else
					pga_counter <= x"00";
					slow_clock  <= not slow_clock;
				end if;
			end if;
		end if;
	end process;

	sclk <= slow_clock;

	--======================== Communication modules ==========================

	-- UART (FTDI and RS485) and SPI for register communication module
	ComunicationInterface : reg_com
		port map(
			clk             => CLK,
			rst             => rst,
			uartComTX_FTDI  => UART_FTDIB_TX,
			uartComRX_FTDI  => UART_FTDIB_RX,
			uartComTX_RS485 => open, -- RS485_A_RI,
			uartComRX_RS485 => open, -- RS485_A_RO,
			spi_csn         => SPI_nCS_FPGA,
			spi_sclk        => SPI_SCK_FPGA,
			spi_sdin        => SPI_MOSI_FPGA,
			spi_sdout       => SPI_MISO_FPGA,
			dataIn          => comRegDataIn,
			regWRsig        => comWEN,
			regAddress      => comRegAddress,
			dataOut         => comRegDataOut --output of selected register going back into communication module
		);
		
		BUFFER_CH1_nOE <= '1'; -- TBD

	--=========================================================================

	--========================= Flash configuration ===========================

	ConfigurationFlash : FlashConfig
		PORT MAP(
			clk               => CLK,
			rst               => rst,
			cmd               => cmd_ConfigFlash,
			flash_block       => cmd_FlashBlock,
			flash_data_w      => comRegDataOut, --output of the active register going into the flash module
			flash_data_r      => comRegDataIn_Flash,
			reg_WEN           => comWEN_Flash,
			mux_flash         => mux_Flash,
			reg_adr           => comRegAddress_Flash,
			Flash_AT25_SO     => Flash_AT25_output,
			Flash_AT25_inputs => Flash_AT25_inputs,
			debug             => open
		);

	--Connections to the on-board configuration flash memory
	FLASH_HOLD_A      <= Flash_AT25_inputs.hold;
	FLASH_WP_A        <= Flash_AT25_inputs.wp;
	FLASH_SI_A        <= Flash_AT25_inputs.si;
	FLASH_CLK_A       <= Flash_AT25_inputs.sck;
	FLASH_CS_A        <= Flash_AT25_inputs.cs;
	Flash_AT25_output <= FLASH_SO_A;

	--=========================================================================

	--================ Communication registers and commands ===================

	--Generate communication registers
	--comRegRO(x): ('1' or '0') specifies whether this register is read only or writable
	--comRegIn(x): (32-bit register value) input value of the instantiated register
	--comRegRD(x): (32-bit register value) reset -default value of the register
	--<signal> <= comRegOut(x) output of register x
	--writing to registers is controlled via comWEN signals if set to '1' register will take
	--input value as new register content
	genComRegs : for i in (2**REGISTER_ADR_WIDTH - 1) downto 0 generate
		comRegs : comReg
			generic map(dwidth => 32)
			port map(clk          => CLK,
			         rst          => rst,
			         resetDefault => comRegRD(i),
			         registerMask => comRegMk(i),
			         din          => comRegIn(i),
			         wEN          => comWEN_Mux(i),
			         ReadOnly     => comRegRO(i),
			         freeze       => cmd_comRegFreeze,
			         dout         => comRegOut(i)
			        );
	end generate;

	-- Automatically script generated register signals
	-- JS: changed to named association to not be dependent on sequence of signal 
	RegisterMap(
					comRegDataIn_MUX 			=> comRegDataIn_MUX, --Input array
		         	comRegOut 					=> comRegOut,
		         	comRegIn 					=> comRegIn,
		         	comRegRD 					=> comRegRD,
		         	comRegMk 					=> comRegMk,
		         	comRegRO 					=> comRegRO,
		         	reg_CRC 					=> reg_CRC,									--in
		         	config_block_num_and_board_ID 	=> config_block_num_and_board_ID,		--in
		         	lock_CRC 					=> lock_CRC,
		         	crc_check_en 				=> crc_check_en,
		         	trigger_status 				=> trigger_status,
		         	crc_active_and_fail_cnt 	=> crc_active_and_fail_cnt, --swapped on purpose to verify named association
		         	system_status 				=> system_status,
		         	
		         	readout_config 				=> readout_config,
		         	pm_buffer_mask 				=> pm_buffer_mask,
		         	GlobalHeartBeatReg 			=> GlobalHeartBeatReg,
				 	adc_health 					=> adc_health,
		         	scaler_overrange 			=> scaler_overrange,
		         	channel_mask 				=> channel_mask,
		         	ChannelConfig 				=> ChannelConfig,
		         	ps5_volt 					=> ps5_volt,
		         	ps15_volt 					=> ps15_volt,
		         	ChannelData 				=> ChannelData,
		         	ChannelBuffer 				=> ChannelBuffer,
		         	ConfigCustomRegister 		=> ConfigCustomRegister
		         );

	--Flash mux 
	--Multiplexes input values and addresses going to the comRegs
	--Source can be either the command module or the FlashConfig module

	--Multiplex the register address
	comRegAddress_Mux <= comRegAddress when (mux_Flash = '0') else comRegAddress_Flash;
	--Multiplex the data vector to be written into the register
	comRegDataIn_MUX  <= comRegDataIn when (mux_Flash = '0') else comRegDataIn_Flash;
	--Multiplex the comReg write enable signals
	comWEN_Mux        <= comWEN when (mux_Flash = '0') else comWEN_Flash;
	--register output mux back to the communication module
	comRegDataOut     <= comRegOut(to_integer(unsigned(comRegAddress_Mux)));

	--Direct command interpreter
	--Writes to the last address of the valid register space is interpreted as a "command"
	--
	--commands available:   
	--	Config Flash module 		x20 xx xx bc 	where "c" is the command to the flash config module
	--												where "b" is the flash block argument 
	--	Control register Freeze		x03 xx xx xf 	where f is the command to freeze '1' or unfreeze '0' 
	--registers do not refresh when freeze is active

	comInt : process(CLK) is
	begin
		if rising_edge(CLK) then
			if rst = '1' then
				cmd_ConfigFlash  <= (others => '0');
				cmd_FlashBlock   <= (others => '0');
				cmd_comRegFreeze   <= '0';
				cmd_buffer_trigger <= '0';
				com_reset        <= '0';
				--cmd_quench_trigger <= '0';
				autozero_start   <= (others => '0');
				--test_sram_cmd    <= '0';
				buffer_cmd(3)      <= '0';
				buffer_cmd(2)      <= '0';
			else
				if comWEN(0) = '1' then

					case comRegDataIn(31 downto 24) is

						when x"70" =>   -- Autozero
							autozero_start <= comRegDataIn(31 downto 0);
						when x"20" =>   -- Flash
							cmd_ConfigFlash <= comRegDataIn(3 downto 0);
							cmd_FlashBlock  <= comRegDataIn(7 downto 4);
						when x"03" =>   -- Freeze registers
							cmd_comRegFreeze <= comRegDataIn(0);
						when x"13" =>   -- PM buffer trigger
							cmd_buffer_trigger <= '1';
						when x"32" =>   -- Rearm PM buffer
							buffer_cmd(3) <= '1';
						when x"33" =>   -- Reset PM buffer for reading again
							buffer_cmd(2) <= '1';
						--when x"40" =>   -- RAM test
						--	test_sram_cmd <= '1';
						when x"3F" =>   -- Reset command
							com_reset <= '1';
						when others => null;
					end case;
				else
					--Set all one-cycle commands outputs back to zero 
					cmd_ConfigFlash    <= (others => '0');
					cmd_comRegFreeze   <= '0';
					cmd_buffer_trigger <= '0';
					buffer_cmd(3)      <= '0';
					buffer_cmd(2)      <= '0';
					--test_sram_cmd      <= '0';
					com_reset          <= '0';
					autozero_start     <= (others => '0');					
				end if;
			end if;
		end if;
	end process comInt;

	--=========================================================================

	--=================== RAM and buffer functionalities ======================

	-- Generates the signal to read from buffer when the last active channel is set
	pm_buffer_readout : process(CLK)
		variable pulse_gen : std_logic;
	begin
		if rising_edge(CLK) then
			if rst = '1' then
				nextByteBuffer <= '0';
			else
				if (decimated_enabled = '1') then
					buffer_cmd(0) <= nextByteBuffer;
					buffer_cmd(1) <= '0';
				else
					buffer_cmd(1) <= nextByteBuffer;
					buffer_cmd(0) <= '0';
				end if;

				nextByteBuffer <= '1' when (to_integer(unsigned(comRegAddress_Mux)) = reg_refresh_buffer and pulse_gen = '1') else '0';
				pulse_gen      := '1' when (to_integer(unsigned(comRegAddress_Mux)) /= reg_refresh_buffer) else '0'; -- to generate only one pulse per read of the last channel register
			end if;
		end if;
	end process;

	decimated_enabled  <= readout_config(4);
	reg_refresh_buffer <= to_integer(unsigned(readout_config(31 downto 24)));

	--=========================================================================

	--================== Connections to on-board resources ====================

	--reset switch is low active...
	rst  <= (not RST_FF3) or com_reset OR (not PRST);
	--keep nrst for compatibility with ucom_v2_0
	--nrst <= RST_FF3 and (not com_reset) and PRST;

	-- Reset synchronizer for entities with asynchronous reset
	syncro_reset : process(CLK)
	begin
		if rising_edge(CLK) then
			RST_FF1 <= PRST;
			RST_FF2 <= RST_FF1;
			RST_FF3 <= RST_FF2;
		end if;
	end process;

	-- Leds
	DEBUG_LED(0) <= rst;
	DEBUG_LED(1) <= RST_FF3;
	DEBUG_LED(2) <= not blink_led_counter(22);
	DEBUG_LED(3) <= blink_led_counter(22);
	DEBUG_LED(4) <= '1';
	DEBUG_LED(5) <= '0';
	DEBUG_LED(6) <= '1';
	DEBUG_LED(7) <= '0';

	-- Blink Led Counter 							
	BlinkLed : process(CLK)
	begin
		if rising_edge(CLK) then
			if rst = '1' then
				blink_led_counter <= (others => '1');
			else
				if blink_led_counter > 0 then
					blink_led_counter <= blink_led_counter - 1;
				else
					blink_led_counter <= (others => '1');
				end if;
			end if;
		end if;
	end process;

	-- Debug signals
	--DEBUG_PORT(0) <= cmd_ConfigFlash(0);
	--DEBUG_PORT(1) <= cmd_ConfigFlash(1);
	--DEBUG_PORT(2) <= FLASH_1_nCS;
	--DEBUG_PORT(3) <= FLASH_1_CLK;
	--DEBUG_PORT(4) <= FLASH_1_SI;
	--DEBUG_PORT(5) <= FLASH_1_SO;
	--DEBUG_PORT(6) <= comWEN_Mux(1);
	--DEBUG_PORT(7) <= comWEN_Mux(2);

	--=========================================================================

	--======================= Register CRC calculation ========================

	CRC_calc : process(CLK)
		variable CRC_temp             : std_logic_vector(31 downto 0);
		variable comRegOut_write_only : std_logic_vector(31 downto 0);
	begin
		if rising_edge(CLK) then
			if (rst) then
				CRC_temp := (others => '0');
			else
				for i in 6 to 255 loop
					comRegOut_write_only := comRegOut(i) when comRegRO(i) = '0' else (others => '0');
					CRC_temp             := CRC_temp xor comRegOut_write_only;
				end loop;
				reg_CRC <= CRC_temp;
				CRC_temp := (others => '0');
			end if;
		end if;
	end process;
	
	crc_chk_en <= crc_check_en(0);
	crc_active_and_fail_cnt(30) <= config_fail;
	crc_active_and_fail_cnt(29 downto 0) <= std_logic_vector(crc_fail_cnt);
	
	
	REG_checker: process(CLK)
	begin
		if rising_edge(CLK) then
			if (rst) then
				waitcount <=  4000000; --100ms
				config_fail <= '0';
				crc_fail_cnt <= (others => '0');
				st_crc <= STARTUP;
			else
				case st_crc is
					when STARTUP =>
						if waitcount > 0 then
							waitcount <= waitcount -1;
							st_crc <= STARTUP;
						else
							st_crc <= ACTIVE;
						end if;
						config_fail <= '0';
					
					when ACTIVE =>
						if crc_chk_en = '0' then
							config_fail <= '0';
							st_crc <= ACTIVE;
						else
							if reg_CRC = lock_CRC then
							--reg map is consistent
								config_fail <= '0';
							else
								config_fail <= '1';
								crc_fail_cnt <= crc_fail_cnt +1;
							end if;
							st_crc <= ACTIVE;
						end if;
						
					when others =>
						st_crc <= ACTIVE;
				end case;
			end if;
		end if;
	end process;

	
	--=========================================================================
	--reg_CRC <= x"AFFEE5E7";
end;