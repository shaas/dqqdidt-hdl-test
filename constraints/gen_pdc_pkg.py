# -*- coding: utf-8 -*-
"""
Created on ?????????
Description: Package to generate PDC file from Excell sheet 
    
@author: Jelena Spasic

Modified: function with two arguments; A.Skoczen (AGH-UST); 23.08.2019
Modified: geneartion of NDC file for I/O Register Combining in ECF mode; A.Skoczen (AGH-UST); 23.08.2019
    
Arguments:
    libero_ver: 11 for CCF
                12 for ECF (NDC)
    in_file: full path to Execel file
    
"""

import xlrd
import os

def gen_pdc(libero_ver, in_file, top_name):
    if libero_ver == 11:
        print('Conservative Constraint Flow')
    elif libero_ver == 12:
        print('Enhancede Constraint Flow')
    else:
        print('Libero version not known')
        return False
    
    if os.path.exists(in_file):
        pdc_input = xlrd.open_workbook(in_file)
        pdc_path = os.path.split(in_file)[0]
        pdc_file = os.path.join(pdc_path, top_name + '_io.pdc')
        pdc_output = open(pdc_file, 'w')
        print('Generation:\t', in_file, '=>', pdc_file)
        if libero_ver == 12:
            ndc_file = os.path.join(pdc_path, top_name + '_io.ndc')
            ndc_output = open(ndc_file, 'w')
            print('\t\tand', in_file, '=>', ndc_file)
        
        cnt_io_rc = 0
        cnt_io = 0
        
        pdc_output.write('set_iobank Bank1 -vcci 3.3 -fixed yes \n')
        pdc_output.write('set_iobank Bank2 -vcci 3.3 -fixed yes \n')
        pdc_output.write('set_iobank Bank4 -vcci 3.3 -fixed no \n')
        pdc_output.write('set_iobank Bank5 -vcci 3.3 -fixed yes \n')
        pdc_output.write('set_iobank Bank6 -vcci 3.3 -fixed yes \n')
        pdc_output.write('set_iobank Bank9 -vcci 3.3 -fixed yes \n')
        pdc_output.write('\n\n\n')
        
        pin_data = pdc_input.sheet_by_index(0)
        
		# Extract all the reg signals except CRC and build_rev
        signal_name = [pin_data.cell_value(r, 0) for r in range(1, pin_data.nrows)]
        pin_name = [pin_data.cell_value(r, 1) for r in range(1, pin_data.nrows)]
        pin_used = [pin_data.cell_value(r, 2) for r in range(1, pin_data.nrows)]
        IO_standard = [pin_data.cell_value(r, 3) for r in range(1, pin_data.nrows)]
        pin_direction = [pin_data.cell_value(r, 4) for r in range(1, pin_data.nrows)]
        schmitt_trigger = [pin_data.cell_value(r, 5) for r in range(1, pin_data.nrows)]
        IO_register = [pin_data.cell_value(r, 7) for r in range(1, pin_data.nrows)]
        pull_up_down = [pin_data.cell_value(r, 8) for r in range(1, pin_data.nrows)]
        
        for i in range(len(pin_used)):
            if pin_used[i] == 'enabled':
                cnt_io = cnt_io + 1
                if signal_name[i].find('[') != -1:
                    name = signal_name[i].partition('[')[0] + '\\' + signal_name[i].partition('[')[1] + signal_name[i].partition('[')[2].partition(']')[0] + '\\' + ']'
                else:
                    name = signal_name[i]
                pdc_output.write('set_io\t' + name + '\t-pinname ' + pin_name[i] + '\t-fixed yes')
                
                if IO_standard[i] != '':
                    pdc_output.write('\t-iostd ' + IO_standard[i])
                
                if pin_direction[i] != '':
                    direction = pin_direction[i]
                else:
                    direction = 'INOUT'
                pdc_output.write('\t-direction ' + direction)
                
                if schmitt_trigger[i].upper() == 'YES':
                    pdc_output.write('\t-SCHMITT_TRIGGER On')
                if libero_ver == 11:
                    if IO_register[i].upper() == 'YES':
                        pdc_output.write('\t-register yes')
                        if pin_direction[i].upper() == 'INPUT':
                            pdc_output.write('\t-in_reg yes\t-out_reg no\t-en_reg no')
                        if pin_direction[i].upper() == 'OUTPUT':
                            pdc_output.write('\t-in_reg no\t-out_reg yes\t-en_reg no')
                        if pin_direction[i].upper() == 'INOUT':
                            pdc_output.write('\t-in_reg no\t-out_reg yes\t-en_reg no')
                        #pdc_output.write('\t-en_reg yes')
                    elif IO_register[i].upper() == 'NO':
                        pdc_output.write('\t-register no')
                elif libero_ver == 12:
                    #print('Libero 12 - not implemented still')

                    if IO_register[i].upper() == 'YES':
                        cnt_io_rc = cnt_io_rc + 1
                        ndc_output.write('set_ioff ' + name)
                        if pin_direction[i].upper() == 'INPUT':
                            ndc_output.write('\t-in_reg yes\t-out_reg no\t-en_reg no\n')
                        if pin_direction[i].upper() == 'OUTPUT':
                            ndc_output.write('\t-in_reg no\t-out_reg yes\t-en_reg no\n')
                        if pin_direction[i].upper() == 'INOUT':
                            ndc_output.write('\t-in_reg no\t-out_reg yes\t-en_reg no\n')
                else:
                    print('Libero version not known')
                    return False
        
                if pull_up_down[i] != '':
                    pdc_output.write('\t-res_pull ' + pull_up_down[i])
                    
                pdc_output.write('\n')
                
        pdc_output.close()
        if libero_ver == 12:
            ndc_output.close()
        print('Number of I/O constrained:', cnt_io)
        print('Number of register for combining into I/O:', cnt_io_rc)
        return True
    else:
        print('Input file: ', in_file, ' does not exist')
        return False
    