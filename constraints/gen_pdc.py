# -*- coding: utf-8 -*-
"""
Created on Fri Aug 23 18:11:53 2019
Description: Generation of PDC and NDC files from Excell sheet using package gen_pdc_pkg
    
@author: anskocze A.Skoczen (AGH-UST)
"""
import sys
sys.path.append('.\\')
import gen_pdc_pkg as pdc

libero_ver = 12
# Pin list for normal DQQDIDT Firmware
#in_file = '.\\DQQDIDT_pin_list_v1-0.xlsx'
# Pin list for UQDS Firmware
in_file = '.\\DQQDIDT_pin_list_v1-1.xlsx'

top_name = 'top'
if pdc.gen_pdc(libero_ver, in_file, top_name):
    print('Done')
else:
    print('Failed')
    