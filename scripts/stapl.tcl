#necessary configuration:
#project name should be the same as work_lib_name for synplify
set proj_name rad_test_dsp
#top_module_name
set top_name top
#libero_proj_dir
set dir actel

set prj_file [format "./%s/%s.prjx" $dir $proj_name]
open_project -file $prj_file

export_bitstream_file \
         -file_name $top_name \
         -format {STP} \
         -limit_SVF_file_size 0 \
         -limit_SVF_file_by_max_filesize_or_vectors {SIZE} \
         -svf_max_filesize {1024} \
         -svf_max_vectors {1000} \
         -master_file 0 \
         -master_file_components {  } \
         -encrypted_uek1_file 0 \
         -encrypted_uek1_file_components { } \
         -encrypted_uek2_file 0 \
         -encrypted_uek2_file_components { } \
         -trusted_facility_file 1 \
         -trusted_facility_file_components {FABRIC} \
         -add_golden_image 0 \
         -golden_image_address {} \
         -golden_image_design_version {} \
         -add_update_image 0 \
         -update_image_address {} \
         -update_image_design_version {} \
         -serialization_stapl_type {SINGLE} \
         -serialization_target_solution {FLASHPRO_3_4_5} 
set file_stp [format "./%s/designer/%s/export/%s.stp" $dir $top_name $top_name]
if {[file exists $file_stp]} {
	file copy -force $file_stp ./bitstream/
	puts "as: STAPL programming file copied"
} else {
puts "as: STAPL programming file dose not exist"
}

save_project