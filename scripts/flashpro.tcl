# Microsemi Tcl Script
# flashpro
#script for Libero with IGLOO2
#A.Skoczen - September 2015
#A.Skoczen - February 2016 - added config file for variables and path
set fp [open "./config.cfg" r]

while { [gets $fp data] >= 0 } {
   #puts $data
   set tab [split $data =]
   set pattern [lindex $tab 0]
   switch $pattern {
	proj_name {set proj_name [lindex $tab 1]
				puts "Project: $proj_name"}
	top_name {set top_name [lindex $tab 1]
				puts "Top module: $top_name"}
	lib_name {set lib_name [lindex $tab 1]
				puts "Library: $lib_name"}
	tech {set tech [lindex $tab 1]
				puts "FPGA: $tech"}
	}
}
close $fp

#libero_proj_dir
set dir flashpro

##############
#foreach dirf [glob -nocomplain -directory $dir *] {
#	puts "as: $dir to delete"
#	file delete $dirf
#}

set projdir [format "./%s/%s" $dir $proj_name]
file delete -force $projdir

set bitstr [format "./bitstream/%s.stp" $proj_name]

if { [file exists $bitstr] } {
	puts "STAPL programming file $bitstr found"
} else { 
	puts "STAPL programming file $bitstr dose not exist" 
	exit
}

set proj_file [format "%s/%s.pro" $projdir $proj_name] 

if { [file exists $proj_file] } {
	open_project -project $proj_file -connect_programmers 1 
} else {
	new_project -name $top_name -location $projdir -mode {single} -connect_programmers 1 
}

set_programming_file -file $bitstr 
set_programming_action -action {PROGRAM} 
run_selected_actions 
close_project
