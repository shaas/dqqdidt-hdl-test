#script for Libero with IGLOO2
#A.Skoczen - 12.2014
#A.Skoczen - revision 3.2015
#A.Skoczen - revision 8.2015
#A.Skoczen - revision 9.2015
#Requirements:
# synthesised design EDIF - file ./synplify/rev_1/'proj_name'.edn
# timing and physical constraints file - file ./constraints/'top_name'.fdc and ./constraints/'top_name'.io.pdc
#
#Results:
# results directory - ./'libero_proj_dir'
# backannotation VHD and SDF - files in ./backanno
# bitstream PDB - file in ./bitstream
#
#A.Skoczen - February 2016 - added config file for variables and path
#
###################################################################################
# use arguments
if { $argc == 0 } {
	puts "Path to directory containg EDN file as argument for $argv0 TCL script is required"
	exit
} else {
	set synthres [format "%s" [lindex $argv 0]]
}
###################################################################################
# read project config from file
set fp [open "./config.cfg" r]

while { [gets $fp data] >= 0 } {
   #puts $data
   set tab [split $data =]
   set pattern [lindex $tab 0]
   switch $pattern {
	proj_name {set proj_name [lindex $tab 1]
				puts "Project: $proj_name"}
	top_name {set top_name [lindex $tab 1]
				puts "Top module: $top_name"}
	lib_name {set lib_name [lindex $tab 1]
				puts "Library: $lib_name"}
	tech {set tech [lindex $tab 1]
				puts "FPGA: $tech"}
	}
}
close $fp

###################################################################################
# libero_proj_dir, dir for annotated netlist
set dir actel
set bann ./backanno

###################################################################################
# HDL language can be VHDL or verilog
set lang VHDL
if { $lang == "VHDL"} {
	set ext vhd
} else {
	set ext v
}
set aslogf [open aslogfile.log a]
puts "as: Project $proj_name with top unit $top_name in language $lang"
puts $aslogf "as: Project $proj_name with top unit $top_name in language $lang"

#####################################################################
# clean up and set stuff...

file delete -force ./$dir
puts "as: Old directory ./$dir deleted"
puts $aslogf "as: Old directory ./$dir deleted"

if { [file exists ./bitstream_backup] } {
	file delete -force ./bitstream_backup
	puts "as: Old directory ./bitstream_backup deleted"
	puts $aslogf "as: Old directory ./bitstream_backup deleted"
}
set bitstr [format "./bitstream/%s.stp" $top_name]
if { [file exists $bitstr] } {
	file rename ./bitstream ./bitstream_backup
	file mkdir ./bitstream 
	puts "as: Old directoriy ./bitstream backuped"
	puts $aslogf "as: Old directoriy ./bitstream backuped"
}
foreach baf [glob -nocomplain -directory $bann *] {
	puts $aslogf "as: $baf to delete"
	file delete $baf
}
puts "as: Directory $bann emptied"
puts $aslogf "as: Directory $bann emptied"

###################################################################
# switch between igloo and smart fusion

switch $tech {
	IG2_150 {
		puts "as: IGLOO2"
		new_project -location ./$dir -name $proj_name -hdl $lang \
		-family {IGLOO2} \
		-die {M2GL150} \
		-package {1152 FC} \
		-speed {-1} \
		-die_voltage {1.2} \
		-adv_options {DSW_VCCA_VOLTAGE_RAMP_RATE:100_MS} \
		-adv_options {IO_DEFT_STD:LVCMOS 2.5V} \
		-adv_options {RESTRICTPROBEPINS:0} \
		-adv_options {TEMPR:COM} \
		-adv_options {VCCI_1.2_VOLTR:COM} \
		-adv_options {VCCI_1.5_VOLTR:COM} \
		-adv_options {VCCI_1.8_VOLTR:COM} \
		-adv_options {VCCI_2.5_VOLTR:COM} \
		-adv_options {VCCI_3.3_VOLTR:COM} \
		-adv_options {VOLTR:COM}
	}
	IG2_060 {
		puts "as: IGLOO2"
		new_project -location ./$dir -name $proj_name -hdl $lang \
		-family {IGLOO2} -die {M2GL060} -package {484 FBGA} -speed {-1} \
		-die_voltage {1.2} \
		-adv_options {DSW_VCCA_VOLTAGE_RAMP_RATE:100_MS} \
		-adv_options {IO_DEFT_STD:LVCMOS 3.3V} \
		-adv_options {RESTRICTPROBEPINS:0} \
		-adv_options {TEMPR:COM} \
		-adv_options {VCCI_1.2_VOLTR:COM} \
		-adv_options {VCCI_1.5_VOLTR:COM} \
		-adv_options {VCCI_1.8_VOLTR:COM} \
		-adv_options {VCCI_2.5_VOLTR:COM} \
		-adv_options {VCCI_3.3_VOLTR:COM} \
		-adv_options {VOLTR:COM}
	}
	SF2 {
		puts "as: SmartFusion2"
		new_project -location ./$dir -name $proj_name -hdl $lang \
		-family {SmartFusion2} \
		-die {M2S010} \
		-package {484 FBGA} \
		-speed {-1} \
		-die_voltage {1.2} \
		-adv_options {DSW_VCCA_VOLTAGE_RAMP_RATE:100_MS} \
		-adv_options {IO_DEFT_STD:LVCMOS 2.5V} \
		-adv_options {RESTRICTPROBEPINS:1} \
		-adv_options {TEMPR:COM} \
		-adv_options {VCCI_1.2_VOLTR:COM} \
		-adv_options {VCCI_1.5_VOLTR:COM} \
		-adv_options {VCCI_1.8_VOLTR:COM} \
		-adv_options {VCCI_2.5_VOLTR:COM} \
		-adv_options {VCCI_3.3_VOLTR:COM} \
		-adv_options {VOLTR:COM}
	}
	default {
		puts "as: Lack of technology settings"
		exit
	}
}

puts "as: Project $proj_name created"
puts $aslogf "as: Project $proj_name created"

set edif [format "%s/%s.edn" $synthres $proj_name]
puts "as: $edif ..."
puts $aslogf "as: $edif ..."

###########################################################################
#import netlist from synplify

import_files -edif $edif
puts "as: EDIF for project $proj_name imported"
puts $aslogf "as: EDIF for project $proj_name imported"

#set lib_name [format "Z%s" $proj_name]
#set lib_name [format "%s" $lib_name]
set mod [format "%s::%s" $top_name $lib_name]




############################################################################
#import physical design constraints (pdc for pins and sdc for timing)

#build file path 
set sdc [format "./constraints/%s_sdc.sdc" $proj_name]
set iopdc [format "./constraints/%s_io.pdc" $top_name]

puts "as: $sdc $iopdc ..."
puts $aslogf "as: $sdc $iopdc ..."

import_files -sdc $sdc
puts "as: SDC for project $proj_name imported"

import_files -io_pdc $iopdc
puts "as: IO_PDC for project $proj_name imported"


puts $aslogf "as: IO_PDC for project $proj_name imported"

############################################################################
# the imported design constraints need to be "organized" this refers already
# to the ones in the /actel directory


set file_sdc [format "./actel/constraint/%s_sdc.sdc" $proj_name]
set file_iopdc [format "./actel/constraint/io/%s_io.pdc" $top_name]

puts "as: $file_sdc $file_iopdc"
puts $aslogf "as: $file_sdc $file_iopdc"

organize_tool_files -tool {COMPILE} \
	-file $file_iopdc \
	-file $file_sdc \
	-module $mod -input_type {constraint}

puts "as: organize_constraints and _tool_files for COMPILE - ok\n"
puts $aslogf "as: organize_constraints and _tool_files for COMPILE - ok\n"

##################################################################################
# start the compile

configure_tool -name {COMPILE} \
-params {MERGE_SDC:0} \
-params {PDC_IMPORT_HARDERROR:0} \
-params {DISPLAY_FANOUT_LIMIT:10} \
-params {BLOCK_PLACEMENT_CONFLICTS:ERROR}\
-params {BLOCK_ROUTING_CONFLICTS:ERROR} \
-params {BLOCK_MODE:0} \
-params {ENABLE_DESIGN_SEPARATION:0}
run_tool -name {COMPILE}
puts "\nas: COMPILE_2 ok\n"
puts $aslogf "\nas: COMPILE_2 ok\n"

#####################################################################################
# place & route

configure_tool \
-name {PLACEROUTE} \
-params {EFFORT_LEVEL:false} \
-params {INCRPLACEANDROUTE:false} \
-params {PDPR:false} -params {TDPR:true}
run_tool -name {PLACEROUTE}
puts "as: PLACEROUTE ok"
puts $aslogf "as: PLACEROUTE ok"
##################################################################################
# verify timing

run_tool -name {VERIFYTIMING}
puts "as: VERIFYTIMING ok"
puts $aslogf "as: VERIFYTIMING ok"

##################################################################################
## export sdf
#configure_tool -name {EXPORTSDF} -params {DELAY_TYPE:falseE} -params {EXPORT_HDL_TYPE:VHDL}
#puts "as: configure_tool for EXPORTSDF ok"
#puts $aslogf "as: configure_tool for EXPORTSDF ok"
#
#run_tool -name {EXPORTSDF}
#puts "as: EXPORTSDF ok"
#puts $aslogf "as: EXPORTSDF ok"
#
#set file_ba_vhd [format "./%s/designer/%s/%s_ba.%s" $dir $top_name $top_name $ext]
#if {[file exists $file_ba_vhd]} {
#	file copy -force $file_ba_vhd $bann
#	puts  "as: $ext copied"
#	puts $aslogf "as: $ext copied"
#} else {
#puts "as: nestlist $lang does not exist"
#puts $aslogf "as: nestlist $lang does not exist"
#}
#set file_ba_sdf [format "./%s/designer/%s/%s_ba.sdf" $dir $top_name $top_name]
#if {[file exists $file_ba_sdf]} {
#	file copy -force $file_ba_sdf $bann
#	puts "as: SDF copied"
#	puts $aslogf "as: SDF copied"
#} else {
#puts "as: SDF file dose not exist"
#puts $aslogf "as: SDF file dose not exist"
#}


###########################################################################################
# generate & export programming file and clean
update_and_run_tool -name {GENERATEPROGRAMMINGFILE} 
clean_tool -name {GENERATEPROGRAMMINGFILE} 

save_project

set dir_stp [format "./%s/designer/%s/export" $dir $top_name]
set file_stp [format "%s/%s.stp" $dir_stp $top_name ]
export_bitstream_file \
         -file_name {Top} \
         -export_dir $dir_stp \
         -format {STP} \
         -master_file 0 \
         -master_file_components {} \
         -encrypted_uek1_file 0 \
         -encrypted_uek1_file_components {} \
         -encrypted_uek2_file 0 \
         -encrypted_uek2_file_components {} \
         -trusted_facility_file 1 \
         -trusted_facility_file_components {FABRIC} \
         -add_golden_image 0 \
         -golden_image_address {} \
         -golden_image_design_version {} \
         -add_update_image 0 \
         -update_image_address {} \
         -update_image_design_version {} \
         -serialization_stapl_type {SINGLE} \
         -serialization_target_solution {FLASHPRO_3_4_5} 

if {[file exists $file_stp]} {
	puts "as: STAPL exported to $dir_stp"
	puts $aslogf "as: STAPL programming exported to $dir_stp"		 
}
		 
if {[file exists $file_stp]} {
	set file_stp_dest [format "./bitstream/%s.stp" $proj_name]
	file copy -force $file_stp $file_stp_dest
	puts "as: STAPL programming file copied to ./bitstream/"
	puts $aslogf "as: STAPL programming file copied to ./bitstream/"
} else {
	puts "as: STAPL programming file dose not exist"
	puts $aslogf "as: STAPL programming file dose not exist"
}

#if FlashPro device is connected you can uncomment this line - not tested
#configure_tool -name {PROGRAMDEVICE} -params {prog_action:} -params {prog_procedures:}
#run_tool -name {PROGRAMDEVICE}
puts $aslogf "as: Implementation of project $proj_name done"
puts $aslogf "as:-----------------------------------------------------------------------------------------"
close $aslogf

