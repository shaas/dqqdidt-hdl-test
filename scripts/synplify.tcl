#script for Synplify with IGLOO2
#A.Skoczen - 12.2014
#A.Skoczen - revision on 3.2015
#
#Requirements:
# VHDL RTL files - directory ./rtl
# timing constraints file - file ./constraints/'top_module_name'.fdc 
# results directory - empty directory ./synplify
# 
#A.Skoczen - February 2016 - added config file for variables and path
#A.Skoczen - July 2019 - modification for Synplify 2019.03
#
#
#Results:
# synthesised design - file ./synplify/synthesis/'proj_name'.edn
# logfile - file ./synplify/xx.log
#
set fp [open "./config.cfg" r]
set impl_name synthesis

while { [gets $fp data] >= 0 } {
   #puts $data
   set tab [split $data =]
   set pattern [lindex $tab 0]
   switch $pattern {
	proj_name {set proj_name [lindex $tab 1]
				puts "Project: $proj_name"}
	top_name {set top_name [lindex $tab 1]
				puts "Top module: $top_name"}
	lib_name {set lib_name [lindex $tab 1]
				puts "Library: $lib_name"}
	source_dir {set src_dir [lindex $tab 1]
				puts "Source directory: $src_dir"}
	tech {set tech [lindex $tab 1]
				puts "FPGA: $tech"}
	}
}
close $fp

set work $lib_name
#set uart_lib UQDSlib
###############################

project -new ./synplify/$proj_name.prj
project -save $proj_name.prj

puts "$work source files (packages):"
set rtllist [glob ./$src_dir/lib/*.vhd]
set main_pckg [format ./%s/lib/%s.vhd $src_dir $lib_name]
foreach f $rtllist {
	if { $f != $main_pckg } {
		puts $f
		add_file -vhdl -lib $work $f
	}
}

puts "$work source files (main package):"
puts $main_pckg
add_file -vhdl -lib $work $main_pckg

puts "$work source files (units):"
set rtllist [glob ./$src_dir/src/*.vhd]
#puts $rtllist
foreach f $rtllist {
puts $f
add_file -vhdl -lib $work $f
}

puts "$work source files (top):"
set top_unit [format ./%s/%s.vhd $src_dir $top_name]
puts $top_unit
add_file -vhdl -lib $work $top_unit

#Timing constraint SDC
set file_sdc [format ./constraints/%s.sdc $top_name]
if { [file exists $file_sdc] } {
	add_file -constraint $file_sdc
	puts "as: Timing constraint file: $file_sdc loaded"
} else {
	puts "as: Lack of timing constraint file: $file_sdc"
	exit
}

impl -add $impl_name -type fpga 

set_option -top_module [format %s.%s $work $top_name]
set res_file [format ./%s/%s.edn $impl_name $proj_name]
puts "as: Synthesis result file: $res_file" 
#set_option -result_file $res_file

set_option -fix_gated_and_generated_clocks 1

switch $tech {
	IG2_060 {
		puts "as: IGLOO2 060"
		set_option -technology IGLOO2
		set_option -part M2GL060
		set_option -grade -1
		set_option -package FG484
	}
	IG2_150 {
		puts "as: IGLOO2 150"
		set_option -technology IGLOO2
		set_option -part M2GL150
		set_option -grade -1
		set_option -package FC1152
	}
	SF2 {
		puts "as: SmartFusion2"
		set_option -technology SmartFusion2
		set_option -part M2S010
		set_option -grade -1
		set_option -package FBGA484
	}
	default {
		puts "as: Lack of technology settings"
		exit
	}
}

#set_option -frequency 50.0
#set_option -vlog_std v2001
set_option -vhdl2008 1
#set_option -write_verilog 1
set_option -write_vhdl 1
set_option -report_path 400
set_option -retiming 1
set_option -enable_nfilter 1
#--------------------------------------------

#---------------------------------------------
project -result_file $res_file
set check_res_file [get_option -result_file]
puts "as: Synthesis result file (check): $check_res_file"

project -save  $proj_name.prj
#start synthesis
project -run 
#synthesis

project -save $proj_name.prj

